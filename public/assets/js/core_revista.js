/////////////////////////////////////
function disableF5(e) {
    if ((e.which || e.keyCode) == 116) e.preventDefault();
    if (e.keyCode == 82 && e.ctrlKey) e.preventDefault();
    if (e.keyCode == 37 && e.altKey) e.preventDefault();
    if (e.keyCode == 39 && e.altKey) e.preventDefault();
};
$(document).on("keydown", disableF5);

$(document).ready(function () {



    $(':input').focus(function () {
        $(this).css("box-shadow", "0 0 0 500px rgba(0, 187, 236, 0.4) inset");
    }); // Tono azul en los input en focus
    $(':input').blur(function () {
        $(this).css("box-shadow", "none");
    });  // se restaura el tono cuando no se enfoque


    $('.mayusculas').keyup(function () {

        cadena = $(this).val().toString().toUpperCase();
        cadena = cadena.match(/[0-9a-zA-ZñÑ\s.,-]+/);
        $(this).val(cadena);


    });
    $('.placa_text').keyup(function () {

        cadena = $(this).val().toString().toUpperCase();
        cadena = cadena.match(/[0-9a-zA-ZñÑ]+/);
        $(this).val(cadena);


    });
    $('.alfanumerico').keyup(function () {

        cadena = $(this).val().toString();
        cadena = cadena.match(/[0-9a-zA-ZñÑ@\s.,-]+/);
        $(this).val(cadena);


    });
    $('.folio_text').keyup(function () {

        cadena = $(this).val().toString();
        cadena = cadena.match(/[0-9a-zA-ZñÑ]+/);
        $(this).val(cadena);


    });
    $('.numeros').keyup(function () {


        cadena = $(this).val().toString();
        cadena = cadena.match(/[0-9]+/);
        $(this).val(cadena);

    });


    function fecha_actual() {
        moment.locale('es');
        var fecha = moment().format('L') + " " + moment().format('LTS');
        $('#hora').text(fecha);
    }

    setInterval(fecha_actual, 1000);

});