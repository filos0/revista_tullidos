/////////////////////////////////////
function disableF5(e) {
    if ((e.which || e.keyCode) == 116) e.preventDefault();
    if (e.keyCode == 82 && e.ctrlKey) e.preventDefault();
    if (e.keyCode == 37 && e.altKey) e.preventDefault();
    if (e.keyCode == 39 && e.altKey) e.preventDefault();
};
$(document).on("keydown", disableF5);

$(document).ready(function () {

    $(document).on('focus', ':input', function () {
        $(this).attr('autocomplete', 'off');
    });
    if (top.location.pathname === '/home' || top.location.pathname === '/') {
        /*
        Descomentar para actualizar los servicios.

        setInterval(render_servicios, 20000);
        setInterval(render_home, 3000);
        */
    }

    var Max = moment().format('YYYY-MM-DD');
    var Min = moment().format('1900-01-01');

    $('input[type ="date"]').each(function () {
        $(this).prop("max", Max).prop("min", Min);
    });

    $(':input').addClass('text-uppercase'); //Mayusculas en todos los input

    $(':input').focus(function () {
        $(this).css("box-shadow", "0 0 0 500px rgba(0, 187, 236, 0.4) inset");
    }); // Tono azul en los input en focus
    $(':input').blur(function () {
        $(this).css("box-shadow", "none");
    });  // se restaura el tono cuando no se enfoque


    $('input[type = date]').keyup('change', function () {

        var fecha = $(this).val();

        if ($(this).hasClass("revista")) {
            var res_fecha = validar_Fechas_Revista(fecha);

            if (res_fecha == true) {
                $(this).css("background-color", "#FFFFFF");// normal
            }

            else if (res_fecha == 'anterior') {
                $(this).val('');
                $(this).focus();
                $(this).css("background-color", "#ff0005");// normal
                swal({
                    title: "",
                    text: "No puedes seleccionar una fecha anterior de hoy y de hoy.",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false
                }).then(function () {
                    $(this).focus();
                });

            }
            else {

                $(this).css("background-color", "#FFFFFF"); //rojo

            }
        }
        else {


            var res_fecha = validar_Fechas(fecha);

            if (res_fecha == true) {
                $(this).css("background-color", "#FFFFFF");// normal
            }

            else if (res_fecha == 'futuro') {
                $(this).val('');
                $(this).focus();
                $(this).css("background-color", "#FFFFFF");// normal
                swal({
                    title: "",
                    text: "No puedes seleccionar una fecha posterior de hoy.",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false
                }).then(function () {
                    $(this).focus();
                });

            }
            else {

                $(this).css("background-color", "#FF0000"); //rojo

            }
        }

    });  // Valida Fechas


    $('.solo_numero').keyup(function () {
        var valActual = $(this).val();
        var nuevoValor = soloNumeros(valActual);
        $(this).val(nuevoValor);
    });

    $('.alfa_Numerico').keyup(function () {
        var valActual = $(this).val();
        var nuevoValor = alfaNumerico(valActual);
        $(this).val(nuevoValor);
    });
    $('.alfaNumerico_espacio').keyup(function () {
        var valActual = $(this).val();
        var nuevoValor = alfaNumerico_espacio(valActual);
        $(this).val(nuevoValor);
    });
    $('.solo_letra').keyup(function () {
        var valActual = $(this).val();
        var nuevoValor = soloLetras(valActual);
        $(this).val(nuevoValor);
    });
    $('.soloLetras_espacio').keyup(function () {
        var valActual = $(this).val();
        var nuevoValor = soloLetras_espacio(valActual);
        $(this).val(nuevoValor);
    });


    $('.confirmar_salir').click(function () {
        swal({
            title: "¿Esta seguro que quiere salir?",
            text: "Se perdera todo el avance",
            type: "error",
            showCancelButton: true,
            cancelButtonText: "Cancelar",
            confirmButtonColor: "#dd0e00",
            confirmButtonText: "Si, Salir!",
            allowEscapeKey: false,
            allowOutsideClick: false
        }).then(function () {
            window.location.href = APP_URL;
        });

    });


    $("[id = persona_moral]").focus(function () {
        $("[name  = focus_propietarioI]").css("box-shadow", "0 0 0 20px rgba(0, 187, 236, 0.4) inset");
    });
    $("[id = persona_fisica]").focus(function () {
        $("[name  = focus_propietario]").css("box-shadow", "0 0 0 20px rgba(0, 187, 236, 0.4) inset");
    });

    $("[id = persona_extranjera]").focus(function () {
        $("[name  = focus_propietarioII]").css("box-shadow", "0 0 0 20px rgba(0, 187, 236, 0.4) inset");
    });


    $("[id = persona_extranjera]").blur(function () {
        $("[name  = focus_propietarioII]").css("box-shadow", "none");
    });

    $("[id = persona_moral]").blur(function () {
        $("[name  = focus_propietarioI]").css("box-shadow", "none");
    });
    $("[id = persona_fisica]").blur(function () {
        $("[name  = focus_propietario]").css("box-shadow", "none");

    });
    $("[id = persona_nacional]").focus(function () {
        $("[name  = focus_nacional]").css("box-shadow", "0 0 0 20px rgba(0, 187, 236, 0.4) inset");
    });
    $("[id = persona_nacional]").blur(function () {
        $("[name  = focus_nacional]").css("box-shadow", "none");
    });
    $("[id = persona_extranjero]").focus(function () {
        $("[name  = focus_extranjero]").css("box-shadow", "0 0 0 20px rgba(0, 187, 236, 0.4) inset");
    });
    $("[id = persona_extranjero]").blur(function () {
        $("[name  = focus_extranjero]").css("box-shadow", "none");
    });
    $("[id = impresion_correcta]").focus(function () {
        $("[name  = focus_correcto]").css("box-shadow", "0 0 0 20px rgba(0, 187, 236, 0.4) inset");
    });
    $("[id = impresion_correcta]").blur(function () {
        $("[name  = focus_correcto]").css("box-shadow", "none");
    });
    $("[id = impresion_incorrecta]").focus(function () {
        $("[name  = focus_incorrecto]").css("box-shadow", "0 0 0 20px rgba(0, 187, 236, 0.4) inset");
    });
    $("[id = impresion_incorrecta]").blur(function () {
        $("[name  = focus_incorrecto]").css("box-shadow", "none");
    });

    $("[id = no_presento_tc]").focus(function () {
        $("[name  = focus_no_presento_tc]").css("box-shadow", "0 0 0 20px rgba(0, 187, 236, 0.4) inset");
    });
    $("[id = no_presento_tc]").blur(function () {
        $("[name  = focus_no_presento_tc]").css("box-shadow", "none");
    });
    $("[id = presento_tc]").focus(function () {
        $("[name  = focus_presento_tc]").css("box-shadow", "0 0 0 20px rgba(0, 187, 236, 0.4) inset");
    });
    $("[id = presento_tc]").blur(function () {
        $("[name  = focus_presento_tc]").css("box-shadow", "none");
    });


    $("[id = num_placa_0]").focus(function () {
        $("[name  = focus_tc_0]").css("box-shadow", "0 0 0 20px rgba(0, 187, 236, 0.4) inset");
    });
    $("[id = num_placa_0]").blur(function () {
        $("[name  = focus_tc_0]").css("box-shadow", "none");
    });
    $("[id = num_placa_1]").focus(function () {
        $("[name  = focus_tc_1]").css("box-shadow", "0 0 0 20px rgba(0, 187, 236, 0.4) inset");
    });
    $("[id = num_placa_1]").blur(function () {
        $("[name  = focus_tc_1]").css("box-shadow", "none");

    });
    $("[id = num_placa_2]").focus(function () {
        $("[name  = focus_tc_2]").css("box-shadow", "0 0 0 20px rgba(0, 187, 236, 0.4) inset");
    });
    $("[id = num_placa_2]").blur(function () {
        $("[name  = focus_tc_2]").css("box-shadow", "none");
    });


//////////////////////////////////////   1er  PANTALLA VEHICULO ALTA NUEVO     ////////////////////////////////////////////////////////////

    $("[name = clave_vehicular]").on('change', function () {
        $('[name = marca]').val('');
        $('[name = linea]').val('');
        $('[name = version]').val('');
        var clave = $("[name = clave_vehicular]").val();

        var expreg = new RegExp('[0-9A-Z]{7}');
        if (clave != clave_lc) {

            swal({
                title: "",
                text: "La clave vehicular no coincide con la proporcionada en la linea de captura",
                confirmButtonText: "Aceptar",
                allowEscapeKey: false,
                allowOutsideClick: false
            }).then(function () {
                $("[name = clave_vehicular]").focus();
            });

            $('[name = marca]').val('');
            $('[name = linea]').val('');
            $('[name = version]').val('');


        }
        else {
            if (!expreg.test(clave)) {

                swal({
                    title: "",
                    text: "El formato de la clave vehícular es incorrecto.",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false
                }).then(function () {
                    $("[name = clave_vehicular]").focus();
                });

                $('[name = marca]').val('');
                $('[name = linea]').val('');
                $('[name = version]').val('');


            }
            else {
                $("[name = clave_vehicular]").css("border-color", "rgb(221, 221, 221)");
                $.ajax({
                    url: serie_vehicular_db,
                    type: "post",
                    data: {"clave_vehicular": clave},
                    success: function (data) {
                        // console.log(data);
                        var marca = data['marca'];
                        var linea = data['linea'];
                        var version = data ['version'];


                        if (data == 0) {
                            swal({
                                title: "",
                                text: "No se encontró registro de la Clave Vehicular " + clave + ".",
                                confirmButtonText: "Aceptar",
                                allowEscapeKey: false,
                                allowOutsideClick: false
                            }).then(function () {
                                $("[name = clave_vehicular]").focus();
                            });


                        }

                        else {

                            $('[name = marca]').val(data.marca);
                            $('[name = linea]').val(data.linea);
                            $('[name = version]').val(data.version);
                            $('[name = clase_tipo_vehiculo_id]').focus();
                        }


                    },
                    statusCode: {
                        500: function () {
                            swal({
                                title: "",
                                text: "El servidor de FINANZAS no responde. Contacta a Sistemas",
                                confirmButtonText: "Aceptar",
                                allowEscapeKey: false,
                                allowOutsideClick: false
                            }).then(function () {
                                $("[name = clave_vehicular]").focus();
                            });
                        }
                    }

                });


            }
        }


    }); ///Checa  BD y luego WebService de Finanzas
    $('[name = clase_vehiculo]').on('change', function () {

        var clase_id = $('[name = clase_vehiculo ]').val();
        $.ajax({
            url: tipovehiculo,
            type: "post",
            data: {"clase": clase_id},
            success: function (data) {
                $('[name = clase_tipo_vehiculo_id]').empty();
                $('[name = clase_tipo_vehiculo_id]').append('<option ></option>');
                $.each(data, function (index, value) {
                    $('[name = clase_tipo_vehiculo_id]').append('<option value="' + value.id_cat_clase_tipo_vehiculo + '">' + value.tipo + '</option>');
                });
            }

        });

    });
    $('[name = combustible]').on('change', function () {
        var id = $('[name = combustible ]').val();
        if (id == 5) { //Electrico
            $('[name = bat ]').val('').prop("disabled", false).prop("required", true).css("border-color", "red");
            $('[name = lit ]').val('').prop("disabled", true).prop("required", false).css("border-color", "rgb(221, 221, 221)");
            $('[name = cilindros ]').val('').prop("disabled", true).prop("required", false).css("border-color", "rgb(221, 221, 221)");
            $(this).css("border-color", "rgb(221, 221, 221)");

        }
        else if (id == 7) { //Manual
            $('[name = bat ]').val('').prop("disabled", true);
            $('[name = lit ]').val('').prop("disabled", true);
            $('[name = cilindros ]').val('').prop("disabled", true);
            $(this).css("border-color", "rgb(221, 221, 221)");
        }
        else if (id == 8) { //No usa
            $('[name = bat ]').val('').prop("disabled", true);
            $('[name = lit ]').val('').prop("disabled", true);
            $('[name = cilindros ]').val('').prop("disabled", true);
            $(this).css("border-color", "rgb(221, 221, 221)");
        }
        else if (id == 9) { //hibrido
            $('[name = bat ]').val('').prop("disabled", false).prop("required", true).css("border-color", "red");
            $('[name = lit ]').val('').prop("disabled", false).prop("required", true).css("border-color", "red");
            $('[name = cilindros ]').val('').prop("disabled", false).prop("required", true).css("border-color", "red");
            $(this).css("border-color", "rgb(221, 221, 221)");
        }
        else {
            $('[name = bat ]').val('').prop("disabled", true);
            $('[name = lit ]').val('').prop("disabled", false);
            $('[name = cilindros ]').val('').prop("disabled", false);
            $(this).css("border-color", "rgb(221, 221, 221)");
        }

    });
    $("[name = repuve]").on('change', function () {
        var repuve = $(this).val();
        if (repuve != '') {
            if (repuve.length < 8) {
                swal({
                    title: "",
                    text: "El número de REPUVE no puede tener menos de 8 digitos.",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false

                }).then(function () {
                    $("[name = repuve]").val('').focus();
                });

            }
            else if (repuve < 0) {
                return false
            }
        }
    });
    $("[name = pasajeros]").keyup(function () {
        var pasajeros = $(this).val();
        if (pasajeros != '') {
            if (pasajeros > 8) {
                swal({
                    title: "",
                    text: "No puede transportar más de 8 pasajeros.",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false

                }).then(function () {
                    $("[name = pasajeros]").val('').focus();
                });

            }
            if (pasajeros < 1) {
                swal({
                    title: "",
                    text: "No puede transportar menos de 1 pasajero.",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false

                }).then(function () {
                    $("[name = pasajeros]").val('').focus();
                });
            }
        }
    });
    $("[name = cilindros]").keyup(function () {
        var cilindros = $(this).val();
        if (cilindros != '') {
            if (cilindros > 12) {
                swal({
                    title: "",
                    text: "No puede tener más de 12 cilindros.",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false

                }).then(function () {
                    $("[name = cilindros]").val('').focus();
                });

            }
            if (cilindros < 1) {
                swal({
                    title: "",
                    text: "No puede tener menos de 1 cilindro.",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false

                }).then(function () {
                    $("[name = cilindros]").val('').focus();
                });
            }
        }
    });
    $("[name = puertas]").keyup(function () {
        var puertas = $(this).val();

        if (puertas != '') {
            if (puertas > 8) {
                swal({
                    title: "",
                    text: "No puede tener más de 8 puertas.",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false

                }).then(function () {
                    $("[name = puertas]").val('').focus();
                });
            }
            if (puertas < 1) {
                swal({
                    title: "",
                    text: "No puede tener 0 puertas.",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false

                }).then(function () {
                    $("[name = puertas]").val('').focus();
                });
            }
        }

    });
    $("[name = lit]").keyup(function () {
        var lit = $(this).val();

        if (lit != '') {
            if (lit > 60) {
                swal({
                    title: "",
                    text: "No puede tener más de 60 lit.",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false

                }).then(function () {
                    $("[name = lit]").val('').focus();
                });

            }
            if (lit < 1) {
                swal({
                    title: "",
                    text: "No puede tener menos de 1 lit.",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false

                }).then(function () {
                    $("[name = lit]").val('').focus();
                });
            }
        }

    });
    $("[name = bat]").keyup(function () {
        var bat = $(this).val();

        if (bat != '') {
            if (bat > 100) {
                swal({
                    title: "",
                    text: "No puede tener más de 100 kWh.",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false

                }).then(function () {
                    $("[name = bat]").val('').focus();
                });
            }
            if (bat < 1) {
                swal({
                    title: "",
                    text: "No puede tener menos de 10 kWh.",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false

                }).then(function () {
                    $("[name = bat]").val('').focus();
                });
            }
        }

    });

////////////////////////////////////////////     3ra  PANTALLA PROPIETARIO ALTA NUEVO  ///////////////////////////////////////////////////////////
    $(' [id = propietario_curprfc ]').on('change', function () {
        var clave = $("[id = propietario_curprfc]").val();
        var clave_original = clave;


        if (clave.length == 0) {

        }

        else if (clave.length > 11) {

            clave = clave.toUpperCase();
            var expreg;

            if ($('[id=persona_fisica]').is(':checked')) {

                expreg = new RegExp('^[A-Z]{4}[0-9]{6}[HM]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[A-Z]{3}[0-9]{2}|[A-Z]{4}[0-9]{6}[A-Z0-9]{3}$');

            }
            else if ($('[id=persona_moral]').is(':checked')) {

                expreg = new RegExp('^[A-Z]{3}[0-9]{6}[A-Z0-9]{3}$');
            }
            else {
                expreg = new RegExp('[A-Z0-9]+');
            }


            if (!expreg.test(clave)) {
                swal({
                    title: "",
                    type: "error",
                    text: "CURP o RFC es incorrecto.",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false
                }).then(function () {
                    if ($('[id=persona_fisica]').is(':checked')) {
                        $('[id=persona_fisica]').prop("checked", true).change();
                    }
                    if ($('[id=persona_moral]').is(':checked')) {
                        $('[id=persona_moral]').prop("checked", true).change();
                    }
                    if ($('[id=persona_extranjera]').is(':checked')) {
                        $('[id=persona_extranjera]').prop("checked", true).change();
                    }
                    $("[id = propietario_curprfc]").val('').focus();

                });

            }
            else {

                var tipo = $("[name = tipo_propietario]").val();
                clave = clave.substr(0, 9);
                $.ajax({
                    url: buscar_porqueria,
                    type: "post",
                    data: {
                        "porqueria": clave,
                        "tipo_propietario": tipo
                    },
                    success: function (data) {
                        //    console.log(data);
                        if (data.length == 0) {

                            swal({
                                title: "Se registrara un nuevo propietario",
                                text: "No hay Propietarios relacionados con este CURP o RFC",
                                confirmButtonText: "Aceptar",
                                allowEscapeKey: false,
                                allowOutsideClick: false
                            }).then(function () {

                                if ($('[id=persona_fisica]').is(':checked')) {
                                    $('[id=persona_fisica]').prop("checked", true).change();
                                }
                                if ($('[id=persona_moral]').is(':checked')) {
                                    $('[id=persona_moral]').prop("checked", true).change();
                                }
                                if ($('[id=persona_extranjera]').is(':checked')) {
                                    $('[id=persona_extranjera]').prop("checked", true).change();
                                }


                                $("[id = propietario_curprfc]").val(clave_original).focus();
                            });


                        }
                        else {

                            var html = "<br><br>" +
                                "<table id='tabla_propietario' class='table table-responsive'>" +
                                "<tr class='sin_hover'>" +
                                "<th class='text-center' style='rgba(255,0,0,0.7);'><b>Clave</b></th>" +
                                "<th class='text-center' style='rgba(255,0,0,0.7);'><b>Nombre</b></th>" +
                                "<th class='text-center' style='rgba(255,0,0,0.7);'><b>Fecha Nacimiento</b></th>" +
                                "<th class='text-center' style='rgba(255,0,0,0.7);'><b>Sexo</b></th>" +

                                "</tr>" +
                                "<tbody>";

                            $.each(data, function (i, propietario) {
                                var sexo;
                                if (propietario.sexo == 'M') {
                                    sexo = 'Masculino';
                                }
                                else if (propietario.sexo == 'F') {
                                    sexo = 'Femenino';
                                }
                                else {
                                    sexo = '-';
                                }
                                html = html + ( '<tr onclick="aceptar_propietario(\'' + propietario.clave_unica + '\')" >' +
                                    '<td >' + propietario.clave_unica + '</td>' +
                                    '<td >' + propietario.primer_apellido + ' ' + propietario.segundo_apellido + ' ' + propietario.nombre_razon_social + '</td>' +
                                    '<td >' + propietario.fecha_nacimiento + '</td>' +
                                    '<td >' + sexo + '</td>' +

                                    '</tr>');

                            });

                            html = html + ("</tbody></table><br>" );


                            swal({
                                title: "Selecciona el propietario que corresponde al ciudadano ",
                                html: html,
                                width: '800px',
                                type: "",
                                animation: false,
                                showCancelButton: false,
                                confirmButtonColor: "#FF0000",
                                confirmButtonText: "El usuario no se encuentra en la lista",
                                allowOutsideClick: false,
                                allowEscapeKey: false
                            }).then(function () {
                                if ($('[id=persona_fisica]').is(':checked')) {
                                    $('[id=persona_fisica]').prop("checked", true).change();
                                }
                                if ($('[id=persona_moral]').is(':checked')) {
                                    $('[id=persona_moral]').prop("checked", true).change();
                                }
                                if ($('[id=persona_extranjera]').is(':checked')) {
                                    $('[id=persona_extranjera]').prop("checked", true).change();
                                }
                                $("[id = propietario_curprfc]").val(clave_original).focus();
                                //  $("[id = propietario_curprfc]").focus();
                            });


                        }
                    }

                });

            }


        }
        else {
            swal({
                title: "",
                text: "El RFC y el CURP debe tener la homoclave",
                confirmButtonText: "Aceptar",
                allowEscapeKey: false,
                allowOutsideClick: false
            }).then(function () {
                $("[id = propietario_curprfc]").val("").focus();
            });
        }

    });

    $('[id=persona_fisica]').change(function () {
        if (this.checked) {
            $('[id = tipo_persona_requerido]').css("background-color", "#ffffff");
            $("[name = tipo_propietario]").val('1');
            $("[name = folio_doc_solicitante]").val('').prop("readonly", true);
            $("[name = doc_solicitante]").val('').prop("disabled", true);
            $("[name = curp_solicitante]").val('').focus();
            $("[name = propietario_id]").remove();
            $('[name = fecha_nacimiento]').val('').prop("disabled", false).prop("readonly", false).css("border-color", "red");

            $('[name = nombre]').val('').prop("disabled", false).css("border-color", "red");
            $('[name = apaterno]').val('').prop("disabled", false).css("border-color", "red");
            $('[name = amaterno]').val('').prop("disabled", false).css("border-color", "red");

            $('[name = sexo]').empty().append('<option></option><option value="F">Femenino</option><option value="M">Masculino</option>');
            $('[name = sexo]').val('').prop("disabled", false).css("border-color", "red");
            $('[name = identificacion]').val('').prop("disabled", false).css("border-color", "red");
            $('[name = nacionalidad]').val('').prop("disabled", true).css("border-color", "rgb(221, 221, 221)").attr("required", false).hide();
            $('[id = nacionalidad]').hide();
            $('[name = telefono]').val('');
            $('[name = cp]').val('').prop("disabled", false).css("border-color", "red");
            $('[name = delegacion]').val('');
            $('[name = colonia]').val('').empty();
            $('[name = calle]').val('').prop("disabled", false).css("border-color", "red");
            $('[name = num_exterior]').val('').prop("disabled", false).css("border-color", "red");
            $('[name = num_interior]').val('').prop("disabled", false).css("border-color", "rgb(221, 221, 221)");
            $('[name = telefono]').val('').prop("disabled", false).css("border-color", "rgb(221, 221, 221)");

            $('[name = identificacion]').empty().append('<option></option>' +
                '<option value="1">Credencial para Votar</option>' +
                '<option value="2">Cartilla de Servicio Militar Nacional</option>' +
                '<option value="3">Pasaporte</option>' +
                '<option value="4">Cedula Profesional</option>' +
                '<option value="5">Licencia para Conducir</option>' +
                '<option value="8">Visa</option>').css("border-color", "red");

            $('[id = curp_rfc]').empty();
            $('[id = curp_rfc]').append("CURP o RFC").css("border-color", "red");
            $('[id = nombre_razon]').empty();
            $('[id = nombre_razon]').append("Nombre(s)").css("border-color", "red");
            $('[id = propietario_curprfc]').val('');
            $('[id = propietario_curprfc]').attr('maxlength', '18').css("border-color", "red");
            $('[id = persona_fisica]').focus();
        }

    });

    $('[id=persona_moral]').change(function () {
        if (this.checked) {
            $('[id = tipo_persona_requerido]').css("background-color", "#ffffff");
            $("[name = tipo_propietario]").val('2');
            $("[name = folio_doc_solicitante]").val('').prop("readonly", true);
            $("[name = doc_solicitante]").val('').prop("disabled", true);
            $("[name = curp_solicitante]").val('').focus();
            $("[name = propietario_id]").remove();
            $('[name = fecha_nacimiento]').val('').prop("disabled", true).css("border-color", "rgb(221, 221, 221)").attr("required", false);

            $('[name = apaterno]').prop("disabled", true).val('').css("border-color", "rgb(221, 221, 221)").attr("required", false);
            $('[name = amaterno]').prop("disabled", true).val('').css("border-color", "rgb(221, 221, 221)").attr("required", false);
            $('[name = sexo]').val('').prop("disabled", true).css("border-color", "rgb(221, 221, 221)").attr("required", false);
            $('[name = identificacion]').val('').prop("disabled", false).css("border-color", "red").attr("required", true);
            $('[name = nacionalidad]').prop("disabled", false).css("border-color", "red").attr("required", true).show();
            $('[id = nacionalidad]').show();
            $('[name = nombre]').val('').prop("disabled", false).css("border-color", "red").attr("required", true);
            $('[name = telefono]').val('').css("border-color", "rgb(221, 221, 221)").attr("required", false);

            $('[name = identificacion]').empty().append('<option></option>' +
                '<option value="12">Acta Constitutiva</option>' +
                '<option value="13">Poder Notarial</option>').prop("disabled", false).css("border-color", "red").attr("required", true);

            $('[name = cp]').val('').prop("disabled", false).prop("disabled", false).css("border-color", "red").attr("required", true);
            $('[name = delegacion]').val('');
            $('[name = colonia]').val('').empty();
            $('[name = calle]').val('').prop("disabled", false).css("border-color", "red").attr("required", true);
            $('[name = num_exterior]').val('').prop("disabled", false).css("border-color", "red").attr("required", true);
            $('[name = num_interior]').val('').prop("disabled", false);
            $('[name = telefono]').val('').prop("disabled", false);

            $('[id = curp_rfc]').empty().append("RFC");
            $('[id = nombre_razon]').empty().append("Razón Social");
            $('[id = propietario_curprfc]').val('').attr('maxlength', '13').change().css("border-color", "red").attr("required", true);
            $('[id = persona_moral]').focus();

        }

    });

    $('[id=persona_extranjera]').change(function () {
        if (this.checked) {
            $('[id = tipo_persona_requerido]').css("background-color", "#ffffff");
            $("[name = tipo_propietario]").val('3');
            $("[name = folio_doc_solicitante]").val('').prop("readonly", true);
            $("[name = doc_solicitante]").val('').prop("disabled", true);
            $("[name = curp_solicitante]").val('').focus();

            $("[name = propietario_id]").remove();
            $('[name = fecha_nacimiento]').val('').prop("readonly", false).prop("disabled", false).css("border-color", "red").attr("required", false);
            $('[name = nombre]').val('').prop("disabled", false).css("border-color", "red").attr("required", false);
            $('[name = apaterno]').val('').prop("disabled", false).css("border-color", "red").attr("required", false);
            $('[name = amaterno]').val('').prop("disabled", false).css("border-color", "red").attr("required", false);
            $('[name = sexo]').val('').prop("disabled", false).css("border-color", "red").attr("required", false);
            $('[name = identificacion]').val('').prop("disabled", false).css("border-color", "red").attr("required", false);
            $('[name = nacionalidad]').val('').prop("disabled", false).show().css("border-color", "red").attr("required", false);
            $('[id = nacionalidad]').show();
            $('[name = telefono]').val('').prop("disabled", false);

            $('[name = identificacion]').empty().append('<option></option>' +
                '<option value="10">FM-2</option>' +
                '<option value="11">FM-3</option>').css("border-color", "red").attr("required", false);
            $('[name = cp]').val('').prop("disabled", false).css("border-color", "red").attr("required", true);
            $('[name = delegacion]').val('');
            $('[name = colonia]').val('').empty();
            $('[name = calle]').val('').prop("disabled", false).css("border-color", "red").attr("required", true);
            $('[name = num_exterior]').val('').prop("disabled", false).css("border-color", "red").attr("required", true);
            $('[name = num_interior]').val('').prop("disabled", false);
            $('[name = telefono]').val('').prop("disabled", false);

            $('[id = curp_rfc]').empty();
            $('[id = curp_rfc]').append("Folio de FM2 - FM3");
            $('[id = nombre_razon]').empty();
            $('[id = nombre_razon]').append("Nombre(s)");
            $('[id = propietario_curprfc]').val('');
            $('[id = propietario_curprfc]').css("border-color", "red").attr("required", true).attr('maxlength', '12').change();
            $('[id = persona_extranjera]').focus();


        }

    });

    $("[name = cp]").on('change', function () {
        $('[name = colonia]').empty();
        $('[name = delegacion]').empty();
        var serie = $("[name = cp]").val();
        var expreg = new RegExp('[0-9]{5}');

        if (!expreg.test(serie)) {

            $("[name = delegacion]").prop("readonly", true);
            $("[name = colonia]").prop("disabled", true);
            $("[name = delegacion]").val('');
            $("[name = colonia]").val('');

            swal({
                title: "",
                text: "El Código Postal es incorrecto.",
                confirmButtonText: "Aceptar",
                allowEscapeKey: false,
                allowOutsideClick: false
            }).then(function () {
                $("[name = cp]").val('').focus();
            });

        }
        else {
            $.ajax({
                url: cp,
                type: "post",
                data: {"cp": serie},
                success: function (data) {
                    if (data.length == 0) {

                        swal({
                            title: "",
                            text: "No hay Colonias ni Delegaciones en esté Código Postal",
                            confirmButtonText: "Aceptar",
                            allowEscapeKey: false,
                            allowOutsideClick: false
                        }).then(function () {
                            $("[name = cp]").val('').focus();
                        });


                    }
                    else {
                        $.each(data, function (key, value) {
                            $('[name = delegacion]').val(value.delegacion);
                            $('[name = delegacion]').prop("readonly", true);
                            $('[name = colonia]').prop("disabled", false);
                            $('[name = colonia]').append('<option class="form-control" value="' + value.id_colonia + '">' + value.colonia + '</option>');
                            $('[name = colonia]').val(value.colonia);
                        });
                        $('[name = delegacion]').css("border-color", "rgb(221, 221, 221)").prop("required", false);
                        $('[name = cp]').css("border-color", "rgb(221, 221, 221)").prop("required", true);
                        $('[name = colonia]').css("border-color", "red").prop("required", true).focus();
                        //if (top.location.pathname === '/Alta/Nuevo/' || top.location.pathname === '/Alta/Usado/') {
                        //  $('[name = colonia]').append('<option value="0000" class="form-control" style="background-color: #ff190c">Agregar Colonia</option>');

                        //  }
                    }
                }

            });


        }

    });

////////////////////////////////     2da  PANTALLA FACTURACION ALTA NUEVO  ////////////////////////////////////////////////////
    $('#procedencia').on('change', function () {

        if ($("#procedencia option:selected ").val() == 1) {
            //nacional
            $('[name = pais]').val('').attr("required", false).prop("disabled", true).css("border-color", "rgb(221, 221, 221)");
            ;
            $('[name = fecha_leg]').val('').attr("required", false).prop("disabled", true).css("border-color", "rgb(221, 221, 221)");
            ;
            $('[name = folio_leg]').val('').attr("required", false).prop("disabled", true).css("border-color", "rgb(221, 221, 221)");
            ;
            $('[id = ocular_pais_fecha_leg]').prop("hidden", true);

        }
        else {

            $('[name = fecha_leg]').val('').prop("disabled", false).attr("required", true).css("border-color", "red");
            ;
            $('[name = folio_leg]').val('').prop("disabled", false).attr("required", true).css("border-color", "red");
            ;
            $('[name = pais]').val('').prop("disabled", false).attr("required", true).css("border-color", "red");
            ;
            $('[id = ocular_pais_fecha_leg]').prop("hidden", false);


        }
    });

    $("input[id='noasegurado']").change(function () {
        if (this.checked) {
            $('[name = poliza]').prop("disabled", true);
            $('[name = aseguradora]').prop("disabled", true);

        }

    });

    $("input[id='asegurado']").change(function () {
        if (this.checked) {
            $('[name = poliza]').prop("disabled", false);
            $('[name = aseguradora]').prop("disabled", false);
        }

    });


    $("[name = valor_factura]").keyup(function () {
        var valActual = $(this).val();
        var nuevoValor = conComas(valActual);
        $(this).val(nuevoValor);
    });

    $("input #swal2-input").keyup(function () {
        var valActual = $(this).val();
        var nuevoValor = valActual.toUpperCase();
        $(this).val(nuevoValor);
    });
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $('[name = sexo]').on('change', function () {


        $(this).css("border-color", "rgb(221, 221, 221)").prop("required", true);

    });
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $("[name = curp_solicitante ]").on('change', function () {

        var curp = $(this).val();

        if (curp != null) {

            curp = curp.toUpperCase();

            var expreg = new RegExp('^[A-Z]{4}[0-9]{6}[HM]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[A-Z]{3}[0-9]{2}|[A-Z]{4}[0-9]{6}[A-Z0-9]{3}$');

            if (!expreg.test(curp)) {
                swal({
                    title: "",
                    text: "CURP o RFC es incorrecto.",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false
                }).then(function () {


                    $("[name = folio_doc_solicitante]").prop("readonly", true);
                    $("[name = doc_solicitante]").prop("disabled", true);
                    $("[name = curp_solicitante]").val('').focus();

                });

            }
            else {


                $("[name = folio_doc_solicitante]").prop("readonly", false);
                $("[name = doc_solicitante]").prop("disabled", false).focus();

            }
        }
    });
///////////////////////////////////////////////////////////////////////////////////////////////////////////
    $(' [name = folio_doc_solicitante ]').on('keyup', function () {
        var folio = $(this).val();

        if (folio != '') {
            if (folio.length > 7) {
                $('[name = datos]').prop("disabled", false);

            }
            else {
                $('[name = datos]').prop("disabled", true);
            }

        }

    });

});

$(' [name = folio_doc_solicitante ]').on('change', function () {
    var folio = $(this).val();

    if (folio.length > 8) {
        $('[name = datos]').prop("disabled", false);

    }

    else {

        swal({
            title: "",
            text: "El folio debe tener más de 8 caracteres.",
            confirmButtonText: "Aceptar",
            allowEscapeKey: false,
            allowOutsideClick: false
        }).then(function () {
            $('[name = folio_doc_solicitante ]').focus();
        });


    }


});

function conComas(valor) {

    var nums = new Array();
    var simb = ","; //Éste es el separador
    valor = valor.toString();
    valor = valor.replace(/\D/g, "");   //Ésta expresión regular solo permitira ingresar números
    nums = valor.split(""); //Se vacia el valor en un arreglo
    var long = nums.length - 1; // Se saca la longitud del arreglo
    var patron = 3; //Indica cada cuanto se ponen las comas
    var prox = 2; // Indica en que lugar se debe insertar la siguiente coma
    var res = "";

    while (long > prox) {
        nums.splice((long - prox), 0, simb); //Se agrega la coma
        prox += patron; //Se incrementa la posición próxima para colocar la coma
    }

    for (var i = 0; i <= nums.length - 1; i++) {
        res += nums[i]; //Se crea la nueva cadena para devolver el valor formateado
    }

    return res;
}

function soloLetras_espacio(nombre) {


    nombre = nombre.toString();
    nombre = nombre.match(/[.,a-zA-ZáÁéÉíÍóÓúÚñÑ|\s]+/);


    return nombre;
}

function soloLetras(nombre) {


    nombre = nombre.toString();
    nombre = nombre.match(/[,.a-zA-ZáÁéÉíÍóÓúÚñÑ]+/);


    return nombre;
}

function soloNumeros(numero) {

    console.log = function () {
    };
    numero = numero.toString();
    numero = numero.match(/[0-9]+/);

    return numero;


}

function alfaNumerico(cadena) {

    console.log = function () {
    };
    cadena = cadena.toString();
    cadena = cadena.match(/[0-9a-zA-ZáÁéÉíÍóÓúÚñÑ]+/);

    return cadena;


}

function alfaNumerico_espacio(cadena) {

    console.log = function () {
    };
    cadena = cadena.toString();
    cadena = cadena.match(/[.,0-9a-zA-ZáÁéÉíÍóÓúÚñÑ|\s]+/);

    return cadena;


}

function validar_Fechas(fecha) {

    var rxDatePattern = /^([1-2][0-9]{3})(\-)(\d{2})(\-)(\d{2})$/;

    var fecha_comp = fecha.match(rxDatePattern);

    if (fecha_comp == null)
        return false;

    var dtArray = fecha.split('-');

    Mes = dtArray[1];
    Dia = dtArray[2];
    Ano = dtArray[0];


    var fecha_hoy = moment();
    var fecha_asig = moment(fecha);


    if (Ano < 1900)
        return false;
    if (Mes < 1 || Mes > 12)
        return false;
    else if (Dia < 1 || Dia > 31)
        return false;
    else if ((Mes == 4 || Mes == 6 || Mes == 9 || Mes == 11) && Dia == 31)
        return false;
    else if (Mes == 2) {
        var isleap = (Ano % 4 == 0 && (Ano % 100 != 0 || Ano % 400 == 0));
        if (Dia > 29 || (Dia == 29 && !isleap))
            return false;
    }

    if (fecha_asig.diff(fecha_hoy) >= 0) {
        return 'futuro';
    }

    return true;


}

function validar_Fechas_Revista(fecha) {

    var rxDatePattern = /^([1-2][0-9]{3})(\-)(\d{2})(\-)(\d{2})$/;

    var fecha_comp = fecha.match(rxDatePattern);

    if (fecha_comp == null)
        return false;

    var dtArray = fecha.split('-');

    Mes = dtArray[1];
    Dia = dtArray[2];
    Ano = dtArray[0];


    var fecha_hoy = moment();
    var fecha_asig = moment(fecha);


    if (Ano < 1900)
        return false;
    if (Mes < 1 || Mes > 12)
        return false;
    else if (Dia < 1 || Dia > 31)
        return false;
    else if ((Mes == 4 || Mes == 6 || Mes == 9 || Mes == 11) && Dia == 31)
        return false;
    else if (Mes == 2) {
        var isleap = (Ano % 4 == 0 && (Ano % 100 != 0 || Ano % 400 == 0));
        if (Dia > 29 || (Dia == 29 && !isleap))
            return false;
    }

    if (fecha_asig.diff(fecha_hoy) >= 0) {
        return 'futuro';
    }
    else {
        return 'anterior';
    }


}


function aceptar_propietario(propietario) {


    $.ajax({
        url: buscar_prop,
        type: "get",
        data: {"clave": propietario},
        success: function (data) {
            if (data.propietario.clave_unica.length != 12 && data.propietario.clave_unica.length != 13 && data.propietario.clave_unica.length != 18) {
                swal({
                    title: 'Se seleccionó un propietario con RFC o CURP erroneo. <br>Por favor corregirlo',
                    input: 'text',
                    showCancelButton: true,
                    inputValidator: function (value) {
                        return new Promise(function (resolve, reject) {

                            if (data.propietario.tipo_propietario_id == 1) {

                                expreg = new RegExp('^[A-Z]{4}[0-9]{6}[HM]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[A-Z]{3}[0-9]{2}|[A-Z]{4}[0-9]{6}[A-Z0-9]{3}$');
                            }
                            else if (data.propietario.tipo_propietario_id == 2) {

                                expreg = new RegExp('^[A-Z]{3}[0-9]{6}[A-Z0-9]{3}$');
                            }

                            if (expreg.test(value)) {
                                $.ajax({
                                    url: buscar_prop,
                                    type: "get",
                                    data: {"clave": value},
                                    success: function (data) {
                                        if (data == 0) {
                                            resolve()
                                        }
                                        else {
                                            reject('CURP o RFC registrado actualmente')
                                        }
                                    }
                                });


                            }

                            else {
                                reject('CURP o RFC Incorrecto')
                            }
                        })
                    }
                }).then(function (result) {

                    $.ajax({
                        url: actualizar_porqueria,
                        type: "POST",
                        data: {
                            "clave_vieja": data.propietario.clave_unica,
                            "clave_nueva": result
                        },
                        success: function (data) {
                            console.log(data);
                            if (data == 1) {

                                $.ajax({
                                    url: buscar_prop,
                                    type: "get",
                                    data: {"clave": result},
                                    success: function (data) {
                                        aceptar_propietario(data.propietario.clave_unica);
                                    }
                                });

                            }
                        }
                    });


                }, function (dismiss) {
                    // dismiss can be 'cancel', 'overlay',
                    // 'close', and 'timer'
                    if (dismiss === 'cancel') {

                        $("[id = propietario_curprfc ]").val('').focus();
                        //$("[id = propietario_curprfc ]").val(data.propietario.clave_unica).change();
                    }
                });
            }
            else {

                var html;
                if (data.colonia.id_colonia == 1) {
                    html = '<b>ATENCIÓN</b><br>' +
                        '<b>Hay datos que necesitan ser actualizados </b><br>' +
                        '<b>Los campos a actualizar apareceran con borde color rojo</b><br><br>' +
                        '<b>Nombre:  </b>' + data.propietario.primer_apellido + ' ' + data.propietario.segundo_apellido + ' ' + data.propietario.nombre_razon_social + '<br>' +
                        '<b>Fecha de Nacimiento:  </b>' + data.propietario.fecha_nacimiento + '<br>' +
                        '<b>Pais:  </b>' + data.pais.pais + '<br><br>';
                }
                else {
                    html = '<b>Nombre:  </b>' + data.propietario.primer_apellido + ' ' + data.propietario.segundo_apellido + ' ' + data.propietario.nombre_razon_social + '<br>' +
                        '<b>Fecha de Nacimiento:  </b>' + data.propietario.fecha_nacimiento + '<br>' +
                        '<b>Pais:  </b>' + data.pais.pais + '<br><br>' +
                        '<b>Domicilio:  </b>' + data.propietario.calle + '<b> Num.Ext  </b>' + data.propietario.numero_exterior + '<b>  Num.Int  </b>' + data.propietario.numero_interior + '<br>' +
                        '<b>Colonia:  </b>' + data.colonia.colonia + '<br>' +
                        '<b>Delegación:  </b>' + data.colonia.delegacion + '<br>' +
                        '<b>Código Postal:  </b>' + data.colonia.cp + '<br>';
                }

                swal({
                    title: "¿Seguro de utilizar la siguiente información?",
                    html: html,
                    type: "",
                    showCancelButton: true,
                    confirmButtonColor: "#FF0000",
                    confirmButtonText: "Si, Usar esa información.",
                    cancelButtonText: 'No, Utilizar otro.',
                    allowOutsideClick: false,
                    allowEscapeKey: false
                }).then(function () {


                        if (data.propietario.tipo_propietario_id == 1) {
                            $(" [id = persona_fisica]").prop('checked', true).click().change();

                        }
                        else if (data.propietario.tipo_propietario_id == 2) {

                            $(" [id = persona_moral]").prop('checked', true).click().change();
                        }
                        else {
                            $(" [id = persona_extranjera]").prop('checked', true).click().change();
                        }


                        $('[name = identificacion] option:selected').text(data.identificacion.tipo_identificacion).val(data.identificacion.id_tipo_identificacion).change();
                        $('[name = identificacion] ').css("border-color", "rgb(221, 221, 221)");

                        $("[name = nombre]").val(data.propietario.nombre_razon_social).css("border-color", "rgb(221, 221, 221)");
                        $("[name = apaterno]").val(data.propietario.primer_apellido).css("border-color", "rgb(221, 221, 221)");
                        $("[name = amaterno]").val(data.propietario.segundo_apellido).css("border-color", "rgb(221, 221, 221)");


                        if (data.colonia.cp == "00000") {
                            $("[name = cp]").val('').css("border-color", "red").prop("required", true);
                            $("[name = delegacion]").val('').css("border-color", "rgb(221, 221, 221)");
                            $("[name = colonia]").empty().css("border-color", "rgb(221, 221, 221)").prop("disabled", false);
                        }
                        else {
                            $("[name = cp]").val(data.colonia.cp).css("border-color", "rgb(221, 221, 221)").prop("required", true);
                            $("[name = delegacion]").val(data.colonia.delegacion).css("border-color", "rgb(221, 221, 221)");
                            $("[name = colonia]").empty().append('<option value="' + data.colonia.id_colonia + '">' + data.colonia.colonia + '</option>').css("border-color", "rgb(221, 221, 221)").prop("disabled", false);
                        }

                        expreg = new RegExp('[A-Z0-9]+');
                        expreg_fecha = new RegExp('[0-9]{4}(-)[0-9]{2}(-)[0-9]{2}');
                        expreg_telefono = new RegExp('[0-9]{8,}');


                        if (data.propietario.tipo_propietario_id == 1) {
                            if (!expreg_fecha.test(data.propietario.fecha_nacimiento)) {
                                $("[name = fecha_nacimiento]").val('').css("border-color", "red").prop("required", true);
                            }
                            else {
                                var vf;
                                vf = validar_Fechas(data.propietario.fecha_nacimiento);
                                if (vf == "futuro") {
                                    $("[name = fecha_nacimiento]").val("").css("border-color", "red").prop("required", true);
                                }
                                else {
                                    $("[name = fecha_nacimiento]").val(data.propietario.fecha_nacimiento).css("border-color", "rgb(221, 221, 221)").prop("required", true);
                                }

                            }
                            if (data.propietario.sexo == 'M') {

                                $('[name = sexo]').empty().append('<option value="M">Masculino</option>').css("border-color", "rgb(221, 221, 221)").prop("required", true);
                            }
                            else if (data.propietario.sexo == 'F') {
                                $('[name = sexo]').empty().append('<option value="F">Femenino</option>').css("border-color", "rgb(221, 221, 221)").prop("required", true);
                            }
                            else {
                                $('[name = sexo]').empty().append('<option></option>');
                                $('[name = sexo]').append('<option value="M">Masculino</option>');
                                $('[name = sexo]').append('<option value="F">Femenino</option>').css("border-color", "red").prop("required", true);
                            }
                        }
                        if (data.propietario.tipo_propietario_id == 2) {

                            $("[name = fecha_nacimiento]").val("").css("border-color", "rgb(221, 221, 221)").prop("required", false);

                            $('[name = sexo]').empty().css("border-color", "rgb(221, 221, 221)").prop("required", false);

                        }


                        if (!expreg.test(data.propietario.calle)) {
                            $("[name = calle]").val('').css("border-color", "red").prop("required", true);
                        }
                        else {
                            $("[name = calle]").val(data.propietario.calle).css("border-color", "rgb(221, 221, 221)").prop("required", true);
                        }

                        if (!expreg.test(data.propietario.numero_exterior)) {
                            $("[name = num_exterior]").val('').css("border-color", "red").prop("required", true);
                        }
                        else {
                            $("[name = num_exterior]").val(data.propietario.numero_exterior).css("border-color", "rgb(221, 221, 221)").prop("required", true);
                        }
                        if (!expreg.test(data.propietario.numero_interior)) {

                            $("[name = num_interior]").val('').css("border-color", "rgb(221, 221, 221)");
                        }
                        else {
                            $("[name = num_interior]").val(data.propietario.numero_interior).css("border-color", "rgb(221, 221, 221)");
                        }

                        if (!expreg_telefono.test(data.propietario.telefono || data.propietario == 'SN')) {

                            $("[name = telefono]").val('').css("border-color", "rgb(221, 221, 221)");
                        }

                        else {
                            $("[name = telefono]").val(data.propietario.telefono).css("border-color", "rgb(221, 221, 221)");
                        }


                        $("[name = nacionalidad]").append('<option selected value="' + data.pais.id_pais + '">' + data.pais.nacionalidad + '</option>').css("border-color", "rgb(221, 221, 221)");
                        $('<input>').attr('type', 'hidden').attr('name', 'propietario_id').attr('value', data.propietario.id_propietario).appendTo('form');
                        $("[id = propietario_curprfc ]").val(data.propietario.clave_unica).css("border-color", "rgb(221, 221, 221)");
                        $("[id = propietario_curprfc ]").focus();

                    },
                    function (dismiss) {
                        // dismiss can be 'cancel', 'overlay',
                        // 'close', and 'timer'
                        if (dismiss === 'cancel') {

                            $("[id = propietario_curprfc ]").focus().val('');
                            $("[id = propietario_curprfc ]").val(propietario).change();
                        }
                    });
            }


        }
    });
}

function preventBack() {
    window.history.forward();

}

setTimeout("preventBack()", 0);
window.onunload = function () {
    null
};

function render_home() {
    $.ajax({
        url: renderhome,
        type: "POST",
        data: {},
        success: function (data) {
            //console.log(data);
            $('#render_home').empty().append(data);

        }

    });
    return null;
}

function render_servicios() {
    $.ajax({
        url: renderServicios,
        type: "POST",
        data: {},
        success: function (data) {
            // console.log(data);
            $('.render_servicio').empty().append(data);
        }

    });
    return null;
}


function prop_solic() {

    if ($('[id = propietario_curprfc]').val() == '') {
        $('[name = curp_solicitante]').val('');
        swal({
            title: "",
            text: "Se necesita que el propietario registre el CURP o RFC.",
            confirmButtonText: "Aceptar",
            allowEscapeKey: false,
            allowOutsideClick: false
        });
    }
    else {

        // var sel_doc = $('[name = identificacion] option:selected');
        $('[name = nombre_solicitante]').val($('[name = nombre]').val()).change();
        $('[name = primer_apellido_solicitante]').val($('[name = apaterno]').val()).change();
        $('[name = segundo_apellido_solicitante]').val($('[name = amaterno]').val()).change();
        $('[name = doc_solicitante] option:selected').text($('[name = identificacion] option:selected').text()).val($('[name = identificacion] option:selected').val()).change();

        $('[name = curp_solicitante]').val($('[id = propietario_curprfc]').val()).change();

    }

}

function numero_mes(numero) {
    if (numero == 01) return 'Enero';
    else if (numero == 02) return 'Febrero';
    else if (numero == 03) return 'Marzo';
    else if (numero == 04) return 'Abril';
    else if (numero == 05) return 'Mayo';
    else if (numero == 06) return 'Junio';
    else if (numero == 07) return 'Julio';
    else if (numero == 08) return 'Agosto';
    else if (numero == 09) return 'Septiembre';
    else if (numero == 10) return 'Octubre';
    else if (numero == 11) return 'Noviembre';
    else return 'Diciembre';

}

function traducir_dia(nombre_inglish) {
    if (nombre_inglish == "Mon")return "Lunes";
    else if (nombre_inglish == "Tue")return "Martes";
    else if (nombre_inglish == "Wed")return "Miércoles";
    else if (nombre_inglish == "Thu")return "Jueves";
    else return "Viernes";
}
