<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();


Route::get('/pdf/holograma', 'PDFController@pdf_prueba_holograma');
Route::get('/pdf/constancia', 'PDFController@pdf_prueba_constancia');

Route::get('/', 'HomeController@index');
//Route::get('/home', 'HomeController@index')->name('home');



Route::get('/Tramite', function (){return view("Admin.Tramite");});
Route::post('/Tramite','RevistaController@tramite');
Route::post('/Tramite/Cancelar','RevistaController@tramite_cancelar');



Route::get('/Usuarios', 'UsuariosController@index');
Route::post('/Usuario/baja', 'UsuariosController@baja_usuario');
Route::post('/Usuario/alta', 'UsuariosController@alta_usuario');
Route::post('/Usuario/restablecer_contraseña', 'UsuariosController@restablecer_contrasena');



Route::get('/Material', 'MaterialController@index');
Route::post('/Material/Agregar', 'MaterialController@agregar_material');

Route::post('/API/Validar_Folio_Holograma/', 'APIController@validar_folio_holograma');
Route::post('/API/Holograma/', 'APIController@holograma');
Route::post('/API/Buscar/Material', 'APIController@material_modulo');
Route::post('/API/Impresion/', 'APIController@impresion');

Route::get('/Revista', 'RevistaController@index');
Route::post('/Revista/Placa', 'RevistaController@placa');
Route::post('/Revista/Datos', 'RevistaController@datos');
Route::post('/Revista/Documentacion', 'RevistaController@documentacion');
Route::post('/Revista/Finalizar', 'RevistaController@finalizar');



Route::get('/Reportes', 'ReportesController@index');
Route::posT('/Reportes', 'ReportesController@generar_reportes');
Route::get('/Reportes/Hoy', 'ReportesController@hoy');


Route::post('/Cambiar/Constraseña','Auth\LoginController@Cambiar_pass');

                                /*   NO LIBERADO */
Route::get('/Impresión/Holograma', function (){return view("Impresion");});
Route::post('/Impresión/Holograma', 'ReimpresionController@buscar');


