<?php
/**
 * Created by PhpStorm.
 * User: progr
 * Date: 20/10/2016
 * Time: 17:32
 */

namespace App\Repositories;


class Pre_Asignacion_Repositorio extends Repositorio
{
    function model()
    {
        return 'App\Models\Oklahoma\Pre_asignacion';
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }


}