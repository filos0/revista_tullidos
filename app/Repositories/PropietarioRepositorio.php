<?php


namespace App\Repositories;

use App\Repositories\Repositorio;



class  PropietarioRepositorio extends Repositorio
{
    function model()
    {
        return 'App\Models\Oklahoma\Propietario';
    }


    function existe($clave_curp = '', $clave_rfc ='', $clave_fm = ''){

        $e = $this->model->where('clave_unica', '=', $clave_curp)->orWhere('clave_unica', '=', $clave_rfc)->
        orWhere('clave_unica', '=', $clave_fm)->get()->first();

        if (is_null($e)){

            return false;

        }

        else{

            return $e->id_propietario;

        }
    }

    function buscar_propietario_ajax($clave){

        $e = $this->model->where('clave_unica', '=', $clave)->get()->first();

        if (is_null($e)){

            return false;

        }

        else{

            return $e;

        }
    }

    function actualizar_clave_unica($clave_nueva,$clave_vieja){

        $e = $this->model->where('clave_unica', $clave_nueva)
                    ->update(['clave_unica' => $clave_vieja]);


            return $e;

    }

    function buscar_propietario_porqueria($porqueria,$tipo_persona){

        $propietarios = $this->model->where('clave_unica', 'LIKE', "%$porqueria%")->where('tipo_propietario_id', '=', $tipo_persona)->get();

        if (is_null($propietarios)){

            return false;

        }

        else{

            return $propietarios;

        }
    }


}