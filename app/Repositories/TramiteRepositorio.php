<?php


namespace App\Repositories;


use Illuminate\Container\Container as App;
use Illuminate\Support\Facades\Auth;


class TramiteRepositorio extends Repositorio
{
    function model()
    {
        return 'App\Models\Oklahoma\Tramite';
    }

    public function buscar_lp($linea_pago)
    {
        if ($this->model->where('linea_captura', '=', $linea_pago)->get()->count() == 0) {
            return true;
        }
        return false;
    }

    public function ultimotramitevehiculo($vehiculo_id)
    {

        return $this->model->where('vehiculo_id', '=', $vehiculo_id)->orderBy('fecha_tramite', 'dsc' )
        ->first();
    }

    public function ultimotramitevehiculo_dif_baja($vehiculo_id)
    {

        return $this->model->where('vehiculo_id', '=', $vehiculo_id)
            ->whereNotIn('costo_id', [30,31,36,37,38,39,/*Chatarrizacion*/])->get()->last();
    }

    public function tramiteplacaactiva($vehiculo_id)
    {

        return $this->model->where('vehiculo_id', '=', $vehiculo_id)
            ->whereNotIn('costo_id', [30,31,36,37,38,39,/*Chatarrizacion*/])
            ->get()->last();
    }

    public function ultimasPlaca($vehiculo_id)
    {

        return $this->model->where('vehiculo_id', '<=', $vehiculo_id)->whereIn('costo_id', [1,4,32,33,40,42])
            ->orderBy('fecha_tramite', 'dsc')->first();
    }

    public function validar_constancia_mp($constancia)
    {

        $constancias =  $this->model->where('constancia', '=', $constancia)->where('tipo_constancia_id', '=', 3)->first();
        return $constancias ;
    }
    public function validar_constancia_chatarrizacion($constancia)
    {

        $constancias =  $this->model->where('constancia', '=', $constancia)->where('tipo_constancia_id', '=', 4)->first();
        return $constancias ;
    }
    public function validar_constancia_discapacidad($constancia)
    {

        $constancias =  $this->model->where('constancia', '=', $constancia)->where('tipo_constancia_id', '=', 1)->first();
        return $constancias ;
    }

    public function ultimostramites()
    {

        $fecha = date('y-m-d');
        $fechafin = date('y-m-d', strtotime('-3 day', strtotime($fecha)));

        return $this->model->where('fecha_tramite', '=', $fechafin)->orderBy('fecha_tramite', 'dsc')->get();
    }

    public function tarjetasporimprimir()
    {
        $tramites = $this->model->
        where('modulo_id', '=', Auth::user()->modulo_id)->whereNotIn('costo_id', [30,31,36,37,38,39,/*chatarrizacion*/])->get();

        return $tramites;

    }

    public function tramites_de_alta($vehiculo_id)
    {
        $tram = $this->model->where('vehiculo_id', '=', $vehiculo_id)->whereIn('costo_id', [1,4,32,33,40,42,2,34,35,41,43])->
        orderBy('id_tramite', 'desc')->get();
        return $tram;
    }

    public function ultimo_tramite_de_baja($vehiculo_id)
    {
        $tram = $this->model->where('vehiculo_id', '=', $vehiculo_id)->whereIn('costo_id', [39])->orderBy('fecha_tramite', 'desc')->
        first();
        return $tram;
    }

    public function cuentapdf($tramite_id)
    {
        $tram = $this->model->where('id_tramite', '=', $tramite_id)->get()->first();
        return $this->model->where('id_tramite', '=', $tramite_id)->update(array('cuenta_generacion_pdf' => $tram->cuenta_generacion_pdf+1));
    }
    public function ultimo_tramite_serie_lc($vehiculo_id,$lc)
    {
        $tram = $this->model->where('vehiculo_id', '=', $vehiculo_id)->where('linea_captura', '=', $lc)->first();
        return $tram;
    }



}