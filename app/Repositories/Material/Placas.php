<?php
/**
 * Created by PhpStorm.
 * User: Hp
 * Date: 13/02/2018
 * Time: 06:27 PM
 */

namespace App\Repositories\Material;

use App\Repositories\LoteMaterialRepositorio as Lotematerial;
use App\Repositories\Documento as Documento;


class Placas
{
    function __construct(Lotematerial $lotematerial, Documento $documento)
    {
        $this->lotematerial = $lotematerial;
        $this->documento = $documento;

    }

    function generarplacas($tipo = 1, $modulo)
    {

            $lote = $this->lotematerial->loteplacasdisponibles($modulo, $tipo);
            if(!isset($lote))
                return 0;
            $matriculas_asignadas = $this->documento->placasporlote($lote->id_lote_material);
            switch ($tipo) {

                case 1: ///Placa auto particular

                    $letra = substr($lote->numero_rango_inicio, 0, 1);
                    $letras = $lote->serie;
                    $rango_in = substr($lote->numero_rango_inicio, 1, 2);
                    $rango_fin = substr($lote->numero_rango_fin, 1, 2);
                    $a = 0;

                    for ($i = $rango_in - 1; $i < $rango_fin; $i++) {
                        $n = $rango_in + $a;
                        if ($n > 9)
                            $p[$a] = $letra . $n . $letras;
                        else
                            $p[$a] = $letra . '0' . $n . $letras;

                        $a++;
                    }
                    break;

                case 2: ///Placa discapacidad
                    $serie = $lote->serie;
                    $rango_in = $lote->numero_rango_inicio;
                    $rango_fin = $lote->numero_rango_fin;

                    $a = 0;

                    for ($i = $rango_in - 1; $i < $rango_fin; $i++) {
                        $n = $rango_in + $a;
                        if ($n > 99)
                            $p[$a] = $n . $serie;

                        elseif ($n < 99 && $n > 9)
                            $p[$a] = '0' . $n . $serie;

                        else
                            $p[$a] = '00' . $n . $serie;

                        $a++;
                    }
                    //dd($p);

                    break;

                case 6:  //////Placa verde

                    $rango = $lote->serie;
                    //$letra = substr($lote->numero_rango_inicio, 1, 2);
                    $rango_in = $lote->numero_rango_inicio;
                    $rango_fin = $lote->numero_rango_fin;
                    $a = 0;

                    for ($i = $rango_in; $i <= $rango_fin; $i++) {
                        $n = $rango_in + $a;
                        if ($n > 99)
                            $p[$a] = $rango ./* $letra . */
                                $n;

                        elseif ($n < 99 && $n > 9)
                            $p[$a] = $rango ./* $letra . */
                                '0' . $n;

                        else
                            $p[$a] = $rango . /*$letra . */
                                '00' . $n;

                        $a++;
                    }
                    $disponibles = array_diff($p, $matriculas_asignadas);
//                   if (in_array($this->incidencialote->allwhere(lote = $lote) )   unset(//valores de incidencia)
                    $placa = reset($disponibles);


                    $this->lotematerial->update(array('usado' => $lote->usado + 1,
                        'disponible' => $lote->disponible - 1), $lote->id_lote_material, 'id_lote_material');

                    $pl_o = $this;
                    $pl_o->placa = $placa;
                    $pl_o->lote = $lote->id_lote_material;

                    return $pl_o;

                    break;
                default:

                    break;

            }
            $disponibles = array_diff($p, $matriculas_asignadas);
            $num = array_rand($disponibles, 1);
            $placa = $p[$num];

            $this->lotematerial->update(array('usado' => $lote->usado + 1,
                'disponible' => $lote->disponible - 1), $lote->id_lote_material, 'id_lote_material');


            $pl_o = $this;
            $pl_o->placa = $placa;
            $pl_o->lote = $lote->id_lote_material;

            return $pl_o;


    }



}