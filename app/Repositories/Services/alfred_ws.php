<?php

namespace App\Repositories\Services;

use GuzzleHttp\Client;


class alfred_ws
{
    protected $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'http://128.222.200.41:7777',
            'timeout' => 15,
            'connect_timeout' => 15
        ]);


    }


    public function consultar_verde($placa)
    {

        try {
            $placa = trim(strtoupper($placa));
            $response = $this->client->post('/alfred/vehicles/type/verde', ['json' => ["service" => "vehicle", "vehicle" => ["plate" => $placa]]]);
            $objeto = json_decode($response->getBody()->getContents());

            if ($objeto[0]->errMsg == "Not Found" && $objeto[0]->errCode == 1)
                return 404;

            $objeto = $this->reconstruir($objeto[0]);
            $objeto = json_decode(json_encode($objeto));
            return $objeto;

        } catch (\GuzzleHttp\Exception\ConnectException $e) {

            return 500;

        }


    }

    public function reconstruir($objeto)
    {

        $modelo = array(
            "tramite" => array(
                [
                    "estatusg" => null,
                    "estatustc" => null,
                    "fecha_expedicion" => ($objeto->date_up) ?? null,
                    "folioteso" => null,
                    "id" => null,
                    "imptenencia" => null,
                    "modulo_t" => "OFICINA CENTRAL",
                    "movimiento_t" => null,
                    "notc" => null,
                    "operador_mov" => ($objeto->user_alta) ?? null,
                    "placa" => ($objeto->plate) ?? null,
                    "placaant" => null,
                    "propietario" => null,
                    "revisor" => null,
                    "serievh" => ($objeto->vin) ?? null,
                    "tipoplaca" => "DISCAPACITADOS",
                ]
            ),
            "propietario" => array(
                [
                    "clave_unica" => ($objeto->curp) ?? null,
                    "fecha_nacimiento" => null,
                    "id_colonia" => ($objeto->suburb) ?? null,
                    "id_propietario" => null,
                    "nombre_razon_social" => ($objeto->name) ?? null,
                    "pais_id" => ($objeto->country_id) ?? null,
                    "primer_apellido" => ($objeto->first_surname) ?? null,
                    "segundo_apellido" => ($objeto->second_surname) ?? null,
                    "sexo" => ($objeto->sex) ?? null,
                    "telefono" => ($objeto->telephone) ?? null,
                    "tipo_identificacion_id" => null,
                    "tipo_propietario_id" => null,
                ]
            ),
            "vehiculo" => array(


                [
                    "aseguradora" => null,
                    "capacidad_kwh" => null,
                    "capacidad_litros" => null,
                    "clase_tipo_vehiculo_id" => ($objeto->vehicle_type) ?? null,
                    "clave_vehicular_id" => ($objeto->vehicular_key) ?? null,
                    "distribuidora" => null,
                    "estatus_id" => null,
                    "fecha_alta" => ($objeto->date_up) ?? null,
                    "fecha_documento_legalizacion" => null,
                    "fecha_factura" => null,
                    "folio_documento_legalizacion" => null,
                    "importe_factura" => ($objeto->invoice_value) ?? null,
                    "modelo" => ($objeto->model) ?? null,
                    "numero_cilindros" => ($objeto->number_cylinders) ?? null,
                    "numero_factura" => ($objeto->insurance_policy) ?? null,
                    "numero_motor" => ($objeto->motor_number) ?? null,
                    "numero_personas" => ($objeto->number_passenger) ?? null,
                    "numero_poliza_seguro" => null,
                    "numero_puertas" => ($objeto->number_doors) ?? null,
                    "numero_repuve" => ($objeto->repuve) ?? null,
                    "origen_motor" => null,
                    "pais_id" => null,
                    "serie_vehicular" => ($objeto->vin) ?? null,
                    "tipo_combustible_id" => ($objeto->type_gas) ?? null,
                    "tipo_servicio_id" => ($objeto->service_type) ?? null,
                    "uso_vehiculo_id" => ($objeto->use) ?? null,
                ]
            ));
        return $modelo;
    }

}