<?php
/**
 * Created by PhpStorm.
 * User: Hp
 * Date: 07/02/2018
 * Time: 05:29 PM
 */

namespace App\Repositories\Services;

use GuzzleHttp\Client;



class DIF
{
    protected $client;
    public function __construct()
    {
        $this->client= new Client([
            'base_uri' => 'http://187.237.26.82:3000',
            'timeout' => 15,
        ]);

    }

    public function consultar($qr)
    {


        try {
            $response = $this->client->request('POST', '/api/cvp/consulta/dif',['json' => ["qr" => $qr]]);
            $status = $response->getStatusCode();


            if($status == 200){
                return json_decode($response->getBody()->getContents());

            }
            else{

                return 500;
            }

        } catch (\GuzzleHttp\Exception\ConnectException $e) {

           return 500;

        }


    }

}