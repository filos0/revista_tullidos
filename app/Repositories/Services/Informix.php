<?php

namespace App\Repositories\Services;

use GuzzleHttp\Client;
use App\Repositories\VehiculosRepositorio as Vehiculo;
use App\Repositories\PropietarioRepositorio as Propietario;
use App\Repositories\MiniatablaRerpositorio as Mini;
use App\Repositories\Catalogos\CatClaveVehicularRepositorio as ClaveVehicular;

use Illuminate\Support\Facades\DB;

class Informix
{
    protected $client;

    public function __construct(Vehiculo $vehiculo, Propietario $propietario, Mini $mini, ClaveVehicular $clave)
    {

        $this->Vehiculo = $vehiculo;
        $this->Propietario = $propietario;
        $this->mini = $mini;
        $this->ClaveVehicular = $clave;

        $this->client = new Client([
            'base_uri' => 'http://128.222.200.41:8888',
            'timeout' => 15.0,
        ]);

    }

    private function token()
    {
        $response = $this->client->request('POST', '/smeargle/altas/tenencia/token');

        return json_decode($response->getBody()->getContents())->token;
    }


    public function consultar($placa_serie, $parte = 'todo')
    {


        try {
            $placa_serie = trim(strtoupper($placa_serie));
            $response = $this->client->request('POST', '/smeargle/consulta/' . $parte, ['json' => ["consulta" => $placa_serie]]);
            $status = $response->getStatusCode();
            $data = json_decode($response->getBody()->getContents());
            if (empty($data->tramite) ||  empty($data->propietario) ||  empty($data->vehiculo))
                return 404;
            else
                return $data;

        } catch (\GuzzleHttp\Exception\ConnectException $e) {

            return 500;

        }



    }


}