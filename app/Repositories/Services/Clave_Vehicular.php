<?php
/**
 * Created by PhpStorm.
 * User: Hp
 * Date: 07/02/2018
 * Time: 05:30 PM
 */

namespace App\Repositories\Services;

use GuzzleHttp\Client;
use App\Models\Revista\Clave_Vehicular_Model as clave;

class Clave_Vehicular
{
    protected $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'http://128.222.200.178:8252/',
            'timeout' => 15.0,
        ]);

    }

    public function consultar($clave)
    {


        try {

            $clv = clave::where('clave_vehicular', $clave)->first();

#520UE
            if (!$clv) {
                $response = $this->client->request('POST', '/ClaveVehicularFinanzas', ['json' => ["numero" => $clave]]);
                $status = $response->getStatusCode();
                if ($status == 200) {

                    $result = json_decode($response->getBody()->getContents());

                    if ($result == null)
                        return 500;


                    $clv = clave::create(['clave_vehicular' => $clave, 'digito' => $result->claveDigito, 'marca' => $result->descripcionMarca,
                        'linea' => $result->descripcionLinea, 'version' => $result->descripcionModelo]);

                    return $clv;

                } else {

                    return 500;

                }
            }
            return $clv;

        } catch (\GuzzleHttp\Exception\ConnectException $e) {

            return 500;

        }


    }
}