<?php
/**
 * Created by PhpStorm.
 * User: Hp
 * Date: 07/02/2018
 * Time: 05:29 PM
 */

namespace App\Repositories\Services;
use GuzzleHttp\Client;


class REPUVE
{
    protected $client;
    public function __construct()
    {
        $this->client= new Client([
            'base_uri' => 'http://10.5.128.1:8100',
            'timeout' => 15,
        ]);

    }

    public function consultar($placa='' , $serie='' , $tipo ='consulta')
    {
        $response = $this->client->request('POST', '/repuve/'.$tipo .'/'. ($placa != '' ? 'placa' : ($serie != '' ? 'serie' : '')),
            ['json' => ["cadena" => $serie.'||'.$placa.'|||||']]);

        return json_decode($response->getBody()->getContents());

    }

    public function numero_repuve($serie){

        $c =$this->consultar('', $serie)->return;

        $r = explode("|", $c);
        return $r[2];
    }

}