<?php


namespace App\Repositories;


use App\Repositories\Repositorio;


class VehiculosRepositorio extends Repositorio
{
    function model()
    {
        return 'App\Models\Oklahoma\Vehiculo';
    }

    public function getVehiculoActivo($serie)
    {
        $v = $this->model->where('serie_vehicular', '=', $serie)->get()->first();

        if (!is_null($v)) {

            if ($v->estatus_id == 2){
                return 2;  //dado de baja
            }

            elseif ($v->estatus_id == 1){
                return 1;  // activo
            }

            elseif ($v->estatus_id == 3){
                return 3;  // chatarrizado
            }

        }
        else
            return null; // No hay vehiculo con ese numero de serie

    }

    public function dardebaja($id_vehiculo){

        $this->model->where('id_vehiculo', '=', $id_vehiculo)->update(array('estatus_id' => 2));
    }
    public function chatarrizar($id_vehiculo){

        $this->model->where('id_vehiculo', '=', $id_vehiculo)->update(array('estatus_id' => 3));
    }

    public function reAlta($serie, array $datos = []){

        $datos = array_merge($datos, array('estatus_id' => 1));

        $r2 = $this->model->where('serie_vehicular', '=', $serie)->update($datos);

        return $r = $this->model->where('serie_vehicular', '=', $serie)->get()->first()->id_vehiculo;



    }




}