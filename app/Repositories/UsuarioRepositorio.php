<?php
/**
 * Created by PhpStorm.
 * User: JosuéGarcía
 * Date: 15/08/2016
 * Time: 13:30
 */

namespace App\Repositories;


class UsuarioRepositorio extends Repositorio
{

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'App\Models\User';
    }







}