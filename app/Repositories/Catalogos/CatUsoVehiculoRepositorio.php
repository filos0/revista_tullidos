<?php
/**
 * Created by PhpStorm.
 * User: JosuéGarcía
 * Date: 21/07/2016
 * Time: 13:41
 */

namespace App\Repositories\Catalogos;


use App\Repositories\Repositorio;

class CatUsoVehiculoRepositorio extends Repositorio
{
    function model()
    {
        return 'App\Models\Oklahoma\Catalogos\Cat_uso_vehiculo';
    }
}