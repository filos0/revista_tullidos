<?php
/**
 * Created by PhpStorm.
 * Date: 2/02/17
 * Time: 12:43 PM
 */

namespace App\Repositories\Catalogos;
use App\Repositories\Repositorio;

class CatCosto extends Repositorio
{
    function model()
    {
        return 'App\Models\Oklahoma\Catalogos\Cat_costo';
    }

    public function precio($id_costo)
    {

        return $this->model->where('id_costo', '=', $id_costo)->get()->last();
    }


}