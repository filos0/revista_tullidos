<?php
namespace App\Repositories\Catalogos;


use App\Repositories\Repositorio;

class CatModulosRepositorio extends Repositorio
{
    function model()
    {
        return 'App\Models\Oklahoma\Catalogos\Cat_modulos';
    }

    public function modulos_x_nombre(){
        return $this->model->orderBy('modulo', 'asc')->get();

    }

}