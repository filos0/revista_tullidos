<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 12/08/2016
 * Time: 01:11 PM
 */

namespace App\Repositories\Catalogos;


use App\Repositories\Repositorio;

class CatColoniasRepositorio extends Repositorio
{
    function model()
    {
        return 'App\Models\Oklahoma\Catalogos\Cat_colonias';
    }

    /**
     * @return mixed
     */

    public function extraerCP($cp) {

        $colonias = $this->model->where('cp', '=', $cp)->orderBy('colonia', 'asc')->get();
            return $colonias;
    }

    public function delegacion(){

        return $this->model->get()->orderBy()->distinct();
    }

}