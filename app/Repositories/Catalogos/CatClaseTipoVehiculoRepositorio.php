<?php
/**
 * Created by PhpStorm.
 * User: 4naka
 * Date: 24/10/2016
 * Time: 02:02 PM
 */

namespace App\Repositories\Catalogos;


use App\Repositories\Repositorio;

class CatClaseTipoVehiculoRepositorio extends Repositorio
{
    function model()
    {
        return 'App\Models\Oklahoma\Catalogos\Cat_clase_tipo_vehiculo';
    }


    public function findAllby($attribute, $value) {
        $faw = $this->model->where($attribute, '=', $value)->get();
        return $faw;
    }

}