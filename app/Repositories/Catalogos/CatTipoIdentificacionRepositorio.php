<?php
/**
 * Created by PhpStorm.
 * User: progr
 * Date: 26/10/2016
 * Time: 14:31
 */

namespace App\Repositories\Catalogos;
use App\Repositories\Repositorio;

class CatTipoIdentificacionRepositorio extends Repositorio
{
    function model()
    {
        return 'App\Models\Oklahoma\Catalogos\Cat_tipo_identificacion';
    }


}