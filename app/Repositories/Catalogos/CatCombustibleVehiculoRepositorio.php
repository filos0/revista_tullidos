<?php
/**
 * Created by PhpStorm.
 * User: JosuéGarcía
 * Date: 21/07/2016
 * Time: 13:24
 */

namespace App\Repositories\Catalogos;


use App\Repositories\Repositorio;

class CatCombustibleVehiculoRepositorio extends Repositorio
{
    function model()
    {
        return 'App\Models\Oklahoma\Catalogos\CatCombustibleVehiculo';
    }
}