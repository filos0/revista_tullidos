<?php
/**
 * Created by PhpStorm.
 * User: progr
 * Date: 26/10/2016
 * Time: 13:34
 */

namespace App\Repositories;

use Illuminate\Support\Facades\DB;

class LoteMaterialRepositorio extends Repositorio
{
    function model()
    {

        return 'App\Models\Oklahoma\LoteMaterial';
    }

    public function todoElMaterial($moduloid)
    {
        $lote = $this->model->where('id_modulo', '=', $moduloid)->get();
        return $lote;
    }

    public function dispoibilidad($moduloid)
    {
        $lote = $this->model->where('id_modulo', '=', $moduloid)->where('disponible', '!=', 0)->where('estatus', '=', 1)->groupBy('cat_tipo_material_id')->orderBy('disponible', 'asc')->get();
        return $lote;
    }

    public function baja_lote($id_lote_material)
    {
        $lote = $this->model->where('id_lote_material', '=', $id_lote_material)->update(array(
            'estatus' => 2));
        return $lote;
    }

    public function alta_lote($id_lote_material)
    {
        $lote = $this->model->where('id_lote_material', '=', $id_lote_material)->update(array(
            'estatus' => 1));
        return $lote;
    }

    public function tipos_placa_disponible($moduloid)
    {
        $lote = $this->model->where('id_modulo', '=', $moduloid)
            ->whereIn('cat_tipo_material_id', [2,  6])
            ->groupBy('cat_tipo_material_id')
            ->get();
        return $lote;
    }

    public function dispoibilidadPlacas($moduloid)
    {
        $lote = $this->model->where('id_modulo', '=', $moduloid)
            ->where('disponible', '!=', 0)
            ->where('cat_tipo_material_id', '=', 1)
            ->get();
        return $lote;
    }

    public function dispoibilidadHoja_Seguridad($moduloid)
    {
        $lote = $this->model->where('id_modulo', '=', $moduloid)
            ->where('disponible', '!=', 0)
            ->where('cat_tipo_material_id', '=', 5)
            ->get();
        return $lote;
    }

    public function dispoibilidadPlacas_verdes($moduloid)
    {
        $lote = $this->model->where('id_modulo', '=', $moduloid)
            ->where('disponible', '!=', 0)
            ->where('cat_tipo_material_id', '=', 6)
            ->get();
        return $lote;
    }

    public function dispoibilidadPlacaDiscapacidad($moduloid)
    {
        $lote = $this->model->where('id_modulo', '=', $moduloid)
            ->where('disponible', '!=', 0)
            ->where('cat_tipo_material_id', '=', 2)
            ->get();
        return $lote;
    }

    public function dispoibilidadTarjetaCirculacion($moduloid)
    {
        $lote = $this->model->where('id_modulo', '=', $moduloid)
            ->where('disponible', '!=', 0)
            ->where('cat_tipo_material_id', '=', 4)
            ->get();
        return $lote;
    }

    public function loteplacasdisponibles($moduloid, $tipo = 1)
    {

        $lote = $this->model->where('id_modulo', '=', $moduloid)->where('disponible', '!=', 0)->
        where('cat_tipo_material_id', '=', $tipo)->first();

        if (!is_null($lote))

            return $lote;

        else
            return null;
    }

    public function lotetarjetasdisponibles($moduloid)
    {

        $lote = $this->model->where('id_modulo', '=', $moduloid)->where('disponible', '!=', 0)->
        where('cat_tipo_material_id', '=', 4)->get()->first();


        if (!is_null($lote))

            return $lote;

        else
            return null;
    }

    public function lote_hoja_seg_disponibles($moduloid)
    {

        $lote = $this->model->where('id_modulo', '=', $moduloid)->where('disponible', '!=', 0)->
        where('cat_tipo_material_id', '=', 5)->get()->first();


        if (!is_null($lote))

            return $lote;

        else
            return null;
    }


    public function exixte($serie, $atribute, array $in)
    {

        $lote = $this->model->whereIn($atribute, $in)->where('serie', '=', $serie)->get();

        if (isset($lote[0])) {
            return $lote;
        } else {
            return false;
        }
    }

    public function traer_rangos($serie, $l)
    {

        $lote = $this->model->select('numero_rango_inicio', 'numero_rango_fin')->where(DB::raw('
        SUBSTRING(numero_rango_inicio, 1, 1)'), '=', $l)->where('serie', '=', $serie)->where('estatus', '=', 1)->get();

        return $lote;

    }


}
