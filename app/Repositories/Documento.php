<?php
/**
 * Created by PhpStorm.
 * User: progr
 * Date: 20/10/2016
 * Time: 17:32
 */

namespace App\Repositories;


class Documento extends Repositorio
{
    function model()
    {
        return 'App\Models\Oklahoma\Documento';
    }

    public function placasporlote($lote)
    {

        $pla = $this->model->orderBy('folio_documento', 'asc')->select('folio_documento')->
        where('lote_material_id', '=', $lote)->get()->toArray();
        $placas = array();
        foreach ($pla as $key => $clave) {
            $p = $clave['folio_documento'];
            array_push($placas, $p);
        }

        return $placas;

    }


    public function bajadocumentosnuevotramite($tramite)
    {

        $pr = $this->model->where('tramite_id', '=', $tramite)///busca los documentos del tramite (tc y placa)
        ->where('estatus_id', '=', 1)
            ->get();

        $pl = array();
        if (!is_null($pr)) {//comprueba que no este vacío
            $pr->toArray();

            foreach ($pr as $key => $clave) {
                if ($clave['tipo_documento_id'] == 2) {
                    $this->model->where('id_documento', '=', $clave['id_documento'])->update(array(
                        'estatus_id' => 2, 'fecha_baja' => date('Y-m-d ')));
                    $pl = array_merge($pl, array('tc' => $clave['folio_documento'], 'lote_tc' => $clave['lote_material_id'],
                        'fecha_tc' => $clave['fecha_documento'], 'vigencia_tc' => $clave['vigencia_documento'],
                        'fecha_expedido_tc' => $clave['fecha_expedido'], 'modulo_id_tc' => $clave['modulo_id']));
                }


                if ($clave['tipo_documento_id'] == 1) {

                    $this->model->where('id_documento', '=', $clave['id_documento'])->update(array(
                        'estatus_id' => 2, 'fecha_baja' => date('Y-m-d ')));

                    $pl = array_merge($pl, array('placa' => $clave['folio_documento'], 'tramite' => $clave['tramite_id'],
                        'lote_material_id' => $clave['lote_material_id'], 'fecha_documento' => $clave['fecha_documento'],
                        'vigencia_documento' => $clave['vigencia_documento'], 'fecha_expedido' => $clave['fecha_expedido'],
                        'modulo_id' => $clave['modulo_id']));

                }


            }

            if ($pl == '' || !is_array($pl))
                return true;
            else
                return $pl;


        } else {//si esta vacío regresa nulo
            return null;
        }
    }


    public function dardebaja($id_documento)
    {

        $this->model->where('id_documento', '=', $id_documento)->update(array(
            'estatus_id' => 2,
            'fecha_baja' => date('Y-m-d ')));
    }

    public function findTramites($attribute, $value)
    {
        $faw = $this->model->where($attribute, '=', $value)->where('estatus_id', '=', 1)->where('tipo_documento_id', '=', 1)->get()->last();
        return $faw;
    }

    public function placa($placa, $inactivas = false)
    {
        $doc = $this->model->where('folio_documento', '=', $placa)->get()->last();

        if (!is_null($doc)) {
            if ($doc->estatus_id == 2 && !$inactivas)
                return true;
            else
                return $doc;
        } else
            return false;
    }

    public function ultima_tc_activa($tc)
    {
        $doc = $this->model->where('folio_documento', '=', $tc)->where('estatus_id', '=', 1)->where('tipo_documento_id', '=', 2)->get()->last();

        if (!is_null($doc)) {
            return $doc;
        } else
            return null;
    }

    public function folio_tc_por_placa($placa)
    {
        $folio_tc = $this->model->where('folio_documento', '=', $placa)->where('estatus_id', '=', 1)->where('tipo_documento_id', '=', 1)->get()->last();

        return $folio_tc;

    }

    public function ultima_tc_activa_folio_tramite($placa)
    {
        $doc = $this->model->where('numero_tarjeta', '=', $placa)->where('estatus_id', '=', 1)->where('tipo_documento_id', '=', 1)->get()->last();

        if (!is_null($doc)) {
            return $doc;
        } else
            return null;
    }

    public function placas_tramite_id($tramite_id)
    {
        $doc = $this->model->where('tramite_id', '=', $tramite_id)->where('tipo_documento_id', '=', 1)->get()->last();

        if (!is_null($doc)) {

            return $doc;
        } else
            return null;
    }

    public function buscar_hoja_baja($folio)
    {
        $doc = $this->model->where('folio_documento', '=', $folio)->where('tipo_documento_id', '!=', 4)->get()->last();

        if (!is_null($doc)) {

            return $doc;
        } else
            return null;
    }


}