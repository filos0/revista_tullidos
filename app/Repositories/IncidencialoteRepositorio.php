<?php
/**
 * Created by PhpStorm.
 * User: progr
 * Date: 11/11/2016
 * Time: 18:41
 */

namespace App\Repositories;



class IncidencialoteRepositorio extends Repositorio
{
    function model()
    {
        return 'App\Models\Oklahoma\Incidenciasmodel';
    }

    public function incidenciaportramite($tramite){
        $i = $this->model->where('tramite_id','=', $tramite)->get();

        if (isset($i[0]))
            return count($i);

        else
            return 0;
    }


}