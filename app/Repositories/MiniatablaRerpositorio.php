<?php
/**
 * Created by PhpStorm.
 * User: Hp
 * Date: 29/03/2017
 * Time: 02:03 PM
 */

namespace App\Repositories;


class MiniatablaRerpositorio extends Repositorio
{
    function model()
    {
        return 'App\Models\Oklahoma\minimodel';
    }

    public function buscar_placa($placa)
    {
        $F = $this->model->where('placa', '=', $placa)->orderBy('fecha_expedicion', 'desc')->get()->first();

        if (isset($F->estatustc))


            return $F;

        else
            return 0;

    }


    public function buscarplaca($value)
    {

        $F = $this->model->where('placa', '=', $value)->orderBy('fecha_expedicion', 'desc')->get()->first();
        if (isset($F->estatustc)) {

            if ($F->estatustc == 'A') {
                return $F;
            } else {
                return 1;
            }

        } else {
            return 0;
        }
    }

    public function tramite_por_serie($value)
    {

        $F = $this->model->where('serievh', '=', $value)->where('estatusg', '=', 'A')->orderBy('fecha_expedicion', 'desc')->get()->first();


        return $F;

    }

    public function tramite_por_serie_ABC($value)
    {

        $F = $this->model->where('serievh', '=', $value)->orderBy('fecha_expedicion', 'desc')->get()->first();


        return $F;

    }

    public function ultima_placa_ant($serie)
    {

        $F = $this->model->where('serievh', '=', $serie)->where('placaant', '!=', '')->orderBy('fecha_expedicion', 'desc')->get()->first();


        return $F;

    }

    public function buscar_folio_tc($placa)
    {
        $tc = $this->model->where('placa', '=', $placa)->orderBy('fecha_expedicion', 'desc')->get()->first();
        return $tc;
    }

    public function tramite_por_placa($value)
    {

        $F = $this->model->where('placa', '=', $value)->get()->last();
        return $F;

    }

    public function vehiculoactivo($value)
    {

        $F = $this->model->where('serievh', '=', $value)->orderBy('fecha_expedicion', 'desc')->get()->first();
        if (isset($F->estatustc)) {

            if ($F->estatustc == 'A') {
                return 1;
            } else {
                return 2;
            }

        } else {
            return null;
        }
    }

    public function dar_de_baja_tramites_informix($serie)
    {
        $tramties = $this->model->where('serievh', '=', $serie)->update(array(
            'estatustc' => 'B',
            'estatusg' => 'B'));
    }

    public function buscar_tc_activa($value)
    {

        $F = $this->model->where('placa', '=', $value)->orderBy('fecha_expedicion', 'dsc')->get()->first();


        return $F;

    }

}


