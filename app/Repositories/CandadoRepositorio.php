<?php
/**
 * Created by PhpStorm.
 * User: progr
 * Date: 11/11/2016
 * Time: 18:41
 */

namespace App\Repositories;


class CandadoRepositorio extends Repositorio
{
    function model()
    {
        return 'App\Models\Oklahoma\CandadoModel';
    }


    public function candado($serie = 0, $placa = '')
    {
        $c = $this->model->where('serievh', '=', $serie)->orwhere('placa', '=', $placa)->get()->last();

        if (is_null($c)) {

            return 0;// No hay vehiculo con ese numero de serie
        }
        else {

            if (!is_null($c))
                if ($c->estatus == 'A')
                    return 1;

                else
                    return 0;
        }


    }


}