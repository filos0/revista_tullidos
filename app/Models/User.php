<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;


class User extends Authenticatable
{
    use EntrustUserTrait;
   // use Notifiable;

    protected $connection = 'mysql_revista';
    protected $fillable = [
        'rfc', 'name', 'primer_apellido', 'segundo_apellido', 'password', 'modulo_id', 'estatus_id', 'email'
    ];
    public $timestamps = false;
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function modulo(){

        return $this->belongsTo('App\Models\Revista\Cat_moduloModel', 'modulo_id');

    }
}
