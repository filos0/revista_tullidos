<?php

namespace App\Models\Oklahoma;

use Illuminate\Database\Eloquent\Model;

class Tramite extends Model
{
    protected $connection = 'mysql_oklahoma';

    protected $table = 'tramite';

    protected $fillable = ['id_tramite', 'fecha_tramite', 'linea_captura', 'curp_solicitante', 'constancia',
        'costo_id', 'propietario_id', 'vehiculo_id', 'modulo_id', 'users_id', 'identificacion_solicitante',
        'importe_linea_captura', 'estatus_id', 'tipo_constancia_id', 'Observaciones', 'cuenta_generacion_pdf'];

    protected $primaryKey = 'id_tramite';

    public $timestamps = false;

    public function documentos()
    {

        return $this->hasMany('App\Models\Oklahoma\Documento');
    }

    public function propietario()
    {
        return $this->belongsTo('App\Models\Oklahoma\Propietario', 'propietario_id');
    }

    public function vehiculo()
    {
        return $this->belongsTo('App\Models\Oklahoma\Vehiculo', 'vehiculo_id');
    }

    public function costo(){
        return $this->belongsTo('App\Models\Oklahoma\Catalogos\Cat_costo', 'costo_id');
    }
    public function operador(){
        return $this->belongsTo('App\Models\Oklahoma\User', 'users_id');
    }

}
