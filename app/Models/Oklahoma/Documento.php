<?php

namespace App\Models\Oklahoma;

use Illuminate\Database\Eloquent\Model;

class Documento extends Model
{
    protected $connection = 'mysql_oklahoma';

    protected $table = 'documento';

    protected $fillable = ['id_documento', 'tipo_documento_id', 'lote_material_id', 'estatus_id', 'folio_documento', 'fecha_documento', 'vigencia_documento', 'fecha_expedido', 'fecha_baja', 'numero_tarjeta', 'numero_placa', 'modulo_id', 'tramite_id'];

    protected $primaryKey = 'id_documento';

    public $timestamps = false;

    public function tramite()
    {
        return $this->belongsTo('App\Models\Oklahoma\Tramite', 'tramite_id');
    }
    public function modulo()
    {
        return $this->belongsTo('App\Models\Oklahoma\Catalogos\Cat_modulos', 'modulo_id');
    }
}
