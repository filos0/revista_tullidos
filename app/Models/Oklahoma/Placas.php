<?php

namespace App\Models\Oklahoma;

use Illuminate\Database\Eloquent\Model;

class Placas extends Model
{
    protected $connection = 'mysql_oklahoma';

    protected $table = 'placas';

    protected $fillable = ['tipo', 'matricula', 'apartada', 'modulo_asignado_id' ,'status_id'];

    /**
     * @param boolean $timestamps
     */
    public $timestamps = false;


    public function vehiculo(){

        return $this->belongsToMany('App\Models\Oklahoma\Vehiculos', 'rmm_vehiculo_placa', 'id_vehiculo', 'id_placa' )->
        withPivot('fecha_inicio', 'fecha_fin', 'status');

    }

    Public function  modulo(){
        $this->belongsTo('App\Models\Oklahoma\Catalogos\CatModulos');

    }

    public function status(){
        return $this->belongsTo('App\Models\Oklahoma\Catalogos\CatStaus');
    }

    public function tipo()
    {
        return $this->belongsTo('App\Models\Oklahoma\Catalogos\CatTipoPlaca');
    }
}
