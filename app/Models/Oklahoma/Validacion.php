<?php

namespace App\Models\Oklahoma;

use Illuminate\Database\Eloquent\Model;

class Validacion extends Model
{
    protected $connection = 'mysql_oklahoma';

    protected $table = 'validacion';



    protected $fillable = ['serie_vehicular', 'modelo', 'numero_cilindros', 'numero_motor', 'origen_motor',
        'numero_personas', 'capacidad_litros', 'fecha_documento_legalizacion', 'pais_id', 'uso_vehiculo_id',
        'clase_tipo_vehiculo_id', 'tipo_combustible_id', 'capacidad_kwh', 'clave_vehicular_id', 'fecha_alta',
        'folio_documento_legalizacion', 'fecha_factura', 'numero_factura', 'importe_factura', 'numero_repuve',
        'aseguradora', 'numero_poliza_seguro', 'distribuidora', 'numero_puertas', 'tipo_servicio_id', 'estatus_id'];





}
