<?php

namespace App\Models\Oklahoma;

use Illuminate\Database\Eloquent\Model;

class LoteMaterial extends Model
{
    protected $connection = 'mysql_oklahoma';

    protected $table = 'lote_material';

    protected $fillable = ['id_modulo', 'cat_tipo_material_id', 'fecha_asignacion', 'total_lote', 'usado',
        'disponible', 'serie', 'numero_rango_inicio', 'numero_rango_fin'];

    protected $primaryKey = 'id_lote_material';

    public $timestamps = false;

    public function modulo(){

        return $this->belongsTo('App\Models\Oklahoma\Catalogos\Cat_modulos','id_modulo');
    }

    public function  tipo_material(){

        return $this->belongsTo('App\Models\Oklahoma\Catalogos\Cat_tipo_material','cat_tipo_material_id');
    }


}
