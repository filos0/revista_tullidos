<?php

namespace App\Models\Oklahoma;

use Illuminate\Database\Eloquent\Model;

class Incidenciasmodel extends Model
{
    protected $connection = 'mysql_oklahoma';

    protected $table = 'incidencia_lote';

    protected $fillable = ['folio_material', 'fecha', 'folio_orden_administrativa', 'lote_material_id','tramite_id'];

    protected $primaryKey = 'id_incidencia_lote';

    public $timestamps = false;

}
