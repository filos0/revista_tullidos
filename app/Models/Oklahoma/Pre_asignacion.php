<?php

namespace App\Models\Oklahoma;

use Illuminate\Database\Eloquent\Model;

class Pre_asignacion extends Model
{
    protected $connection = 'mysql_oklahoma';

    protected $table = 'preasignacion_discapacitados';

    protected $fillable = ['id_preasignacion_discapacitados', 'QR', 'placa', 'lote_material_id', 'fecha_asignacion', 'estatus'];

    protected $primaryKey = 'id_preasignacion_discapacitados';

    public $timestamps = false;

}
