<?php

namespace App\Models\Oklahoma;

use Illuminate\Database\Eloquent\Model;

class minimodel extends Model
{
    protected $connection = 'mysql_oklahoma';

    protected $table = 'minitabla';

    protected $fillable = ['serievh', 'placa', 'placaant', 'estatustc', 'estatusg', 'notc', 'fecha_expedicion',
        'operador_mov', 'movimiento_t', 'modulo_t', 'revisor', 'propietario', 'folioteso', 'imptenencia', 'tipoplaca'];


    public $timestamps = false;
    protected $primaryKey = 'id';

    public function Propietario()
    {
        return $this->belongsTo('App\Models\Oklahoma\Propietario', 'propietario');
    }

    public function vehiculo()
    {
        return $this->belongsTo('App\Models\Oklahoma\Vehiculo', 'serievh', 'id_vehiculo');
    }

}
