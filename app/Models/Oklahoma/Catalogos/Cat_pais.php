<?php

namespace App\Models\Oklahoma\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Cat_pais extends Model
{
    protected $connection = 'mysql_oklahoma';

    protected $table = 'cat_pais';
    protected $primaryKey = 'id_pais';
    protected $fillable = ['id_pais', 'pais', 'nacionalidad'];

    public $timestamps = false;

    public function vehiculo(){

        return $this->hasMany('App\Models\Vehiculo');
    }

    public function propietario(){

        return $this->hasMany('App\Models\Propietario');

    }


}
