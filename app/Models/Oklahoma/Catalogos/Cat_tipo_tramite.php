<?php

namespace App\Models\Oklahoma\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Cat_tipo_tramite extends Model
{
    protected $connection = 'mysql_oklahoma';

    protected $table = 'cat_tipo_tramite';

    protected $fillable = ['id_tipo_tramite', 'tipo_tramite',];

    protected $primaryKey = 'id_tipo_tramite';

    public $timestamps = false;
}
