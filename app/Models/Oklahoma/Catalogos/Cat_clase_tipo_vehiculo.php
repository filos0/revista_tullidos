<?php

namespace App\Models\Oklahoma\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Cat_clase_tipo_vehiculo extends Model
{
    protected $connection = 'mysql_oklahoma';

    protected $table = 'cat_clase_tipo_vehiculo';

    protected $fillable = ['clave_clase', 'clase', 'clave_tipo','tipo'];

    protected $primaryKey = 'id_cat_clase_tipo_vehiculo';

    public function vehiculo()
    {
        return $this->hasMany('App\Models\Oklahoma\Catalogos\Vehiculo');

    }


}