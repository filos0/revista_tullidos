<?php

namespace App\Models\Oklahoma\Catalogos;
use Illuminate\Database\Eloquent\Model;


class CatServicioVehiculo extends Model
{

    protected $connection = 'mysql_oklahoma';

    protected $table = 'cat_tipo_servicio';

    protected $fillable = ['clave_tipo_servicio', 'tipo_servicio'];

    protected $primaryKey = 'id_tipo_servicio';

    public function vehiculo(){
        return $this->hasMany('App\Models\Oklahoma\Vehiculo');
    }

}
