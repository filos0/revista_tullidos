<?php

namespace App\Models\Oklahoma\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Cat_tipo_vehiculo_pago extends Model
{
    protected $connection = 'mysql_oklahoma';

    protected $table = 'cat_tipo_vehiculo_pago';

    protected $fillable = ['id_uso_vehiculo', 'clave_uso_vehiculo', 'uso_vehiculo', 'id_uso_vehiculo',];

    protected $primaryKey = 'id_uso_vehiculo';

    public $timestamps = false;

}
