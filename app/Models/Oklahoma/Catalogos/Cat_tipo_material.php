<?php

namespace App\Models\Oklahoma\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Cat_tipo_material extends Model
{

    protected $connection = 'mysql_oklahoma';

    protected $table = 'cat_tipo_material';

    protected $fillable = ['clave_tipo_material', 'tipo_material'];

    protected $primaryKey = 'id_cat_tipo_material';

    public function lote_material(){

        return $this->hasMany('App\Models\Oklahoma\LoteMaterial');
    }
}
