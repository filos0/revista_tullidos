<?php

namespace App\Models\Oklahoma\Catalogos;
use Illuminate\Database\Eloquent\Model;

class CatDocLegalVehiculo extends Model
{
    protected $connection = 'mysql_oklahoma';

    protected $table = 'cat_tipo_documento';

    protected $primaryKey = 'id_tipo_documento';
}
