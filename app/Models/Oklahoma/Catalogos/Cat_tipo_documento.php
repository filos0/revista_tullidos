<?php

namespace App\Models\Oklahoma\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Cat_tipo_documento extends Model
{
    protected $connection = 'mysql_oklahoma';

    protected $table = 'cat_tipo_documento';

    protected $fillable = ['id_tipo_documento', 'tipo_documento',];

    public $timestamps = false;
}
