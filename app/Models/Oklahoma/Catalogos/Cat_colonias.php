<?php

namespace App\Models\Oklahoma\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Cat_colonias extends Model
{
    protected $connection = 'mysql_oklahoma';

    protected $table = 'cat_colonia';

    protected $fillable = ['cp', 'colonia', 'delegacion','entidad_federativa'];

    protected $primaryKey = 'id_colonia';



    public function propietario()
    {

        return $this->hasMany('App\Models\Oklahoma\Propietario');
    }



}
