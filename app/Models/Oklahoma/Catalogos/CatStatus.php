<?php

namespace App\Models\Oklahoma\Catalogos;

use Illuminate\Database\Eloquent\Model;

class CatStatus extends Model
{
    protected $connection = 'mysql_oklahoma';

    protected $table = 'cat_Status';

    protected $fillable = ['nombre', 'descripcion'];

    public function placa(){

        return $this->hasMany('App\Models\Oklahoma\Placas');
    }

    public function vehiculo(){
        return$this->hasMany('App\Models\Oklahoma\Vehiculo');

    }

    public function domicilio(){
        return$this->hasMany('App\Models\Oklahoma\Domicilio');

    }
}
