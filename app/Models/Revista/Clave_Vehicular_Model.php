<?php

namespace App\Models\Revista;

use Illuminate\Database\Eloquent\Model;

class Clave_Vehicular_Model extends Model
{
    protected $connection = 'mysql_revista';

    protected $table = 'cat_clave_vehicular';

    protected $fillable = ['id_cat_clave_vehicular', 'clave_vehicular', 'digito', 'marca', 'linea', 'version'];

    protected $primaryKey = 'id_cat_clave_vehicular';

    public $timestamps = false;

}
