<?php

namespace App\Models\Revista;

use Illuminate\Database\Eloquent\Model;

class Cat_modulos extends Model
{
    protected $connection = 'mysql_revista';

    protected $table = 'cat_modulo';

     public $timestamps = false;

    protected $fillable = ['clave_modulo', 'modulo', 'cat_estatus_id'];

    protected $primaryKey = 'id_modulo';


      public function user(){

        return $this->hasMany('App\Models\Revista\User');
    }

    public function lote_material(){

        return $this->hasMany('App\Models\Revista\LoteMaterial');
    }


}
