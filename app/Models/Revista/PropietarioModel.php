<?php

namespace App\Models\Revista;

use Illuminate\Database\Eloquent\Model;


class PropietarioModel extends Model
{

    protected $connection = 'mysql_revista';

    protected $table = 'propietario';

    protected $fillable = [
        "clave_unica",
        "primer_apellido",
        "segundo_apellido",
        "nombre",
        "fecha_nacimiento",
        "telefono",
        "pais_id",
    ];

    protected $primaryKey = 'id_propietario';
    public $timestamps = false;
}
