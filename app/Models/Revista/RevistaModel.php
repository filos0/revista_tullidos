<?php

namespace App\Models\Revista;

use Illuminate\Database\Eloquent\Model;

class RevistaModel extends Model
{
    protected $connection = 'mysql_revista';

    protected $table = 'revista';

    protected $fillable = ['id_revista', 'año', 'requisitos'];

    protected $primaryKey = 'id_revista';

    public $timestamps = false;
}
