<?php

namespace App\Models\Revista;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Revista_vehiculoModel extends Model
{
    use SoftDeletes;
    protected $connection = 'mysql_revista';

    protected $table = 'revista_vehiculo';

    protected $fillable = [
        'fecha_revista',
        'constancia',
        'constancia_data',
        'modulo_id',
        'users_id',
        'Observaciones',
        'placa',
        'revista_id',
        'vehiculo_id',
        'propietario_id',
        'impresion',
        'updated_at',
        'created_at',
        'deleted_at'];

    protected $primaryKey = 'id_revista_vehiculo';
    protected $dates = ['deleted_at'];

    public $timestamps = true;


    public function modulo()
    {

        return $this->belongsTo('App\Models\Revista\Cat_modulos', 'modulo_id');
    }

    public function usuario()
    {

        return $this->belongsTo('App\Models\User', 'users_id');
    }
    public function Vehiculo()
    {

        return $this->belongsTo('App\Models\Revista\VehiculoModel', 'vehiculo_id');
    } public function Propietario()
    {

        return $this->belongsTo('App\Models\Revista\PropietarioModel', 'propietario_id');
    }
    public function revista()
    {

        return $this->belongsTo('App\Models\Revista\RevistaModel', 'revista_id');
    }

    public function holograma()
    {
        return $this->hasOne('App\Models\Revista\HologramaModel', 'revista_vehiculo_id');
    }
}
