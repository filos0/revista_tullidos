<?php

namespace App\Models\Revista;

use Illuminate\Database\Eloquent\Model;

class Cat_moduloModel extends Model
{
    protected $connection = 'mysql_revista';

    protected $table = 'cat_modulo';

    protected $fillable = ['clave_modulo', 'modulo', 'cat_estatus_id'];

    protected $primaryKey = 'id_modulo';

    public $timestamps = false;

    public function user(){

        return $this->hasMany('App\Models\Revista\User');
    }

    public function lote_material(){

        return $this->hasMany('App\Models\Revista\Lote_materialModel','id_modulo');
    }
    public function revista_vehiculo(){

        return $this->hasMany('App\Models\Revista\Revista_vehiculoModel');
    }


}
