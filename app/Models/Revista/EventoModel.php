<?php

namespace App\Models\Revista;

use Illuminate\Database\Eloquent\Model;

class EventoModel extends Model
{
    protected $connection = 'mysql_revista';

    protected $table = 'evento';

    protected $fillable = ['id_evento', 'users_id', 'placa', 'constancia', 'evento', 'fecha'];

    protected $primaryKey = 'id_evento';

    public $timestamps = false;

    public function usuario(){

    return $this->belongsTo('App\Models\User','users_id');
}


}
