<?php

namespace App\Models\Revista;

use Illuminate\Database\Eloquent\Model;

class wsModel extends Model
{
    protected $connection = 'mysql_revista';

    protected $table = 'ws';

    protected $fillable = ['id_ws', 'folio', 'array'];

    protected $primaryKey = 'id_ws';

    public $timestamps = false;
}
