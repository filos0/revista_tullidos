<?php

namespace App\Models\Revista;

use Illuminate\Database\Eloquent\Model;

class Incidencia_loteModel extends Model
{
    protected $connection = 'mysql_revista';

    protected $table = 'incidencia_lote';

    protected $fillable = ['id_incidencia_lote', 'folio_material', 'fecha', 'folio_orden_administrativa', 'lote_material_id', 'revista_vehiculo_id'];

    protected $primaryKey = 'id_incidencia_lote';

    public $timestamps = false;

    public function revista(){

        return $this->BelongsTo('App\Models\Revista\Revista_vehiculoModel','revista_vehiculo_id');
    }
}
