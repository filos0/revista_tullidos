<?php

namespace App\Models\Revista;

use Illuminate\Database\Eloquent\Model;

class Cat_tipo_material extends Model
{
    protected $connection = 'mysql_revista';

    protected $table = 'cat_tipo_material';

    protected $fillable = ['clave_tipo_material', 'tipo_material'];

    protected $primaryKey = 'id_cat_tipo_material';

    public function lote_material(){

        return $this->hasMany('App\Models\Revista\Lote_material');
    }
}
