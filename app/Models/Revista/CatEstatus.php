<?php

namespace App\Models\Revista;

use Illuminate\Database\Eloquent\Model;

class CatEstatus extends Model
{
    protected $connection = 'mysql_revista';

    protected $table = 'cat_estatus';

    protected $fillable = ['clave_estatus', 'estatus'];


}
