<?php

namespace App\Models\Revista;

use Illuminate\Database\Eloquent\Model;

class HologramaModel extends Model
{
    protected $connection = 'mysql_revista';

    protected $table = 'holograma';

    protected $fillable = ['id_holograma', 'lote_material_id', 'estatus_id', 'folio_holograma', 'fecha_holograma', 'vigencia_documento', 'fecha_expedido', 'modulo_id', 'revista_vehiculo_id'];

    protected $primaryKey = 'id_holograma';

    public $timestamps = false;

    public function modulo(){

        return $this->belongsTo('App\Models\Revista\Cat_modulos','modulo_id');
    }

}
