<?php

namespace App\Http\Controllers\Oklahoma;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

use App\Repositories\VehiculosRepositorio as Vehiculo;
use App\Repositories\Documento as Documento;
use App\Repositories\PropietarioRepositorio as Propietario;
Use App\Repositories\TramiteRepositorio as Tramite;
use App\Repositories\Catalogos\CatCosto as Costos;
use App\Repositories\UsuarioRepositorio as Usuario;
use App\Repositories\Catalogos\CatDocLegalVehiculoRepositorio as DocLegal;
use App\Repositories\Catalogos\CatCombustibleVehiculoRepositorio as Combustible;
use App\Repositories\Catalogos\CatPaisVehiculoRepositorio as Pais;
Use App\Repositories\Catalogos\CatModulosRepositorio as Modulo;
use App\Repositories\Services\REPUVE as repuve;
use App\Repositories\Pre_Asignacion_Repositorio as Pre;
use App\Repositories\Catalogos\CatTipoIdentificacionRepositorio as Identificacion;

use App\Repositories\Material\Placas as Placas;
use App\Models\Role;
use App\Models\Permission;
use App\Http\Controllers\PDFController as PDFC;
use Validator;
use DB;
use Input;
use Sessions;

class AltasController extends Controller

{


    public function __construct(Vehiculo $vehiculo,
                                Propietario $propietario,
                                Combustible $claseCombustible,
                                Pais $pais,
                                DocLegal $docLegal,
                                Modulo $modulo,
                                Tramite $tramite,
                                Usuario $usuario,
                                Documento $documento,
                                Identificacion $identificacion,
                                PDFC $PDFC,
                                Costos $costo,
                                repuve $repuve,
                                Pre $pre,
                                Placas $placas)
    {
        $this->vehiculo = $vehiculo;
        $this->propietario = $propietario;
        $this->claseCombustible = $claseCombustible;
        $this->PDFC = $PDFC;
        $this->pais = $pais;
        $this->docLegal = $docLegal;
        $this->modulo = $modulo;
        $this->tramite = $tramite;
        $this->usuario = $usuario;
        $this->documento = $documento;
        $this->identificacion = $identificacion;
        $this->costo = $costo;
        $this->pre = $pre;
        $this->repuve = $repuve;
        $this->placas = $placas;

        $this->middleware('auth');


    }

    public
    function DarDeAlta(request $request)
    {
dd($request);
        $user = Auth::user();


        if ($user->hasRole(['administrador', 'jefe_modulo', 'operador', 'ecologico']) && $user->ip_logueo == $request->ip()) {

            try {

                DB::beginTransaction();


                if (isset($request->id_tipo_placa)) {
                    $lit = $request->lit;
                    $bat = $request->bat;
                    $tipo_p = $request->id_tipo_placa;
                }

                if ($tipo_p == 2) {//si sigue preasignacion
                    $folio = $this->pre->findBy('QR', $request->constancia);

                    $this->pre->update(array('estatus' => 2), $folio->id_preasignacion_discapacitados,'id_preasignacion_discapacitados');

                } else {
                    $folio = $this->placas->generarplacas($tipo_p, $user->modulo_id);
                    if ($folio == 0){

                    }
                }


                /* Si el propietario aparecio en lista */
                if (isset($request->propietario_id)) {
                    $propietario = $this->propietario->update(array(
                            'nombre_razon_social' => $request->nombre,
                            'primer_apellido' => (isset($request->apaterno)) ? $request->apaterno : null,
                            'segundo_apellido' => (isset($request->amaterno)) ? $request->amaterno : null,
                            'clave_unica' => $request->clave_unica,
                            'tipo_identificacion_id' => $request->identificacion,
                            'pais_id' => (isset($request->nacionalidad)) ? $request->nacionalidad : 82,
                            'fecha_nacimiento' => (isset($request->fecha_nacimiento)) ? $request->fecha_nacimiento : null,
                            'sexo' => (isset($request->sexo)) ? $request->sexo : null,
                            'telefono' => (isset($request->telefono)) ? ($request->telefono == '' ? null : $request->telefono) : null,
                            'tipo_propietario_id' => $request->tipo_propietario,
                            'calle' => $request->calle,
                            'numero_exterior' => $request->num_exterior,
                            'numero_interior' => (isset($request->num_interior)) ? ($request->num_interior == '' ? null : $request->num_interior) : null,
                            'id_colonia' => $request->colonia)
                        , $request->propietario_id, "id_propietario");

                } /* Si es propietario nuevo en lista */
                else {
                    $propietario = $this->propietario->create(array(
                        'nombre_razon_social' => $request->nombre,
                        'primer_apellido' => (isset($request->apaterno)) ? $request->apaterno : null,
                        'segundo_apellido' => (isset($request->amaterno)) ? $request->amaterno : null,
                        'clave_unica' => $request->clave_unica,
                        'tipo_identificacion_id' => $request->identificacion,
                        'pais_id' => (isset($request->nacionalidad)) ? $request->nacionalidad : 82,
                        'fecha_nacimiento' => (isset($request->fecha_nacimiento)) ? $request->fecha_nacimiento : null,
                        'sexo' => (isset($request->sexo)) ? $request->sexo : null,
                        'telefono' => (isset($request->telefono)) ? ($request->telefono == '' ? null : $request->telefono) : null,
                        'tipo_propietario_id' => $request->tipo_propietario,
                        'calle' => $request->calle,
                        'numero_exterior' => $request->num_exterior,
                        'numero_interior' => (isset($request->num_interior)) ? ($request->num_interior == '' ? null : $request->num_interior) : null,
                        'id_colonia' => $request->colonia))->id_propietario;
                }


                $clave_vehicular = $this->clave->findBy('clave_vehicular', $request->clave_vehicular)->id_cat_clave_vehicular;

                $vehiculo = $this->vehiculo->create(array(
                    'serie_vehicular' => $request->serie,
                    'modelo' => $request->ano,
                    'numero_cilindros' => isset($request->cilindros) ? $request->cilindros : 0,
                    'numero_motor' => $request->numero_motor,
                    'origen_motor' => $request->origen_motor,
                    'numero_personas' => $request->pasajeros,
                    'capacidad_kwh' => $bat,
                    'capacidad_litros' => $lit,
                    'fecha_documento_legalizacion' => $request->procedencia == 1 ? null : $request->fecha_leg,

                    'pais_id' => $request->procedencia == 1 ? 82 : $request->pais,

                    'uso_vehiculo_id' => 1,
                    'clase_tipo_vehiculo_id' => $request->clase_tipo_vehiculo_id,
                    'tipo_combustible_id' => $request->combustible,
                    'clave_vehicular_id' => $clave_vehicular,
                    'fecha_alta' => date('Y-m-d H:i:s'),
                    'folio_documento_legalizacion' => $request->procedencia == 1 ? null : $request->folio_leg,
                    'fecha_factura' => $request->fecha_factura,
                    'numero_factura' => $request->numero_factura,
                    'importe_factura' => str_replace(',', '', $request->valor_factura),
                    'numero_repuve' => $request->repuve,
                    // 'aseguradora' => $seguro,
                    // 'numero_poliza_seguro' => $poliza,
                    'distribuidora' => $request->distribuidora,
                    'numero_puertas' => $request->puertas,
                    'tipo_servicio_id' => 1,
                    'estatus_id' => 1))->id_vehiculo;

                $tramite = $this->tramite->create(array(
                    'costo_id' => $request->cat_costo,
                    'propietario_id' => $propietario,
                    'vehiculo_id' => $vehiculo,
                    'users_id' => $user->id,
                    'modulo_id' => $modulo = $user->modulo_id,
                    'fecha_tramite' => date('Y-m-d H:i:s'),
                    'linea_captura' => $request->linea_pago,
                    'curp_solicitante' => $request->curp_solicitante,
                    'identificacion_solicitante' => $request->doc_solicitante . '_' . $request->folio_doc_solicitante,
                    'importe_linea_captura' => '309',
                    'constancia' => $request->constancia == '' ? null : $request->constancia,
                    'tipo_constancia_id' => $request->constancia == '' ? 0 : 1))->id_tramite;

                $this->documento->create(array(
                    'tipo_documento_id' => 1,
                    'lote_material_id' => isset($folio->lote) ? $folio->lote : $folio->lote_material_id,
                    'estatus_id' => 1,
                    'folio_documento' => $folio->placa,
                    'fecha_documento' => date('Y-m-d H:i:s'),
                    'fecha_expedido' => date('Y-m-d H:i:s'),
                    'modulo_id' => $user->modulo_id,
                    'tramite_id' => $tramite,
                    'numero_tarjeta' => $modulo . $tramite));


                DB::commit();
                return $this->finalizarTramite($tramite);


            }  catch (\Exception $e) {

                DB::rollBack();
                \Log::error('DarDeAlta' . $e);

                //dd($e->getCode());

                return view('errors.error_global')
                    ->with('msg', str_limit($e->getMessage()))
                    ->with('code', $e->getCode());

            }


        } else {
            return redirect('home');
        }


    }


}
