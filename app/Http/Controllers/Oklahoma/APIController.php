<?php

namespace App\Http\Controllers\Oklahoma;


use App\Repositories\Services\Tenencias;
use Request;
use App\Models\Revista\HologramaModel as Holograma;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;

use App\Models\Revista\Incidencia_loteModel as Incidencia_lote;

use App\Repositories\Catalogos\CatClaveVehicularRepositorio as ClaveVehicular;
use App\Repositories\PropietarioRepositorio as Propietario;
use App\Repositories\Catalogos\CatColoniasRepositorio as Colonia;

use App\Repositories\Services\REPUVE as repuve;
use App\Repositories\Services\Tenencias as Ten;
use App\Repositories\Services\Informix as Info;
use App\Repositories\Services\DIF as Dif;
use App\Repositories\Services\Clave_Vehicular as clave;


class APIController extends Controller
{

    public function __construct(repuve $REPUVE, Ten $tenencias, Info $info , Propietario $propietario, Colonia $colonia,
                                Dif $dif, ClaveVehicular $ClaveVehicular, clave $clave)
    {
        $this->repube = $REPUVE;
        $this->tenencias = $tenencias;
        $this->info = $info;
        $this->dif = $dif;
        $this->ClaveVehicular = $ClaveVehicular;
        $this->clave = $clave;
        $this->propietario = $propietario;
        $this->colonia = $colonia;
        //$this->middleware('auth');
    }


    public  function servicios(){

        dd($this->info->comprobar_vehiculo('515YXW'));
    }

    public function index()
    {
        return view('QR_dif');
    }

    public
    function consulta_dif_okla()
    {


        $c = $this->dif->consultar(Input::get('qr'));

        return ["resultado" => $c->resultado,
            "curp" => $c->curp,
            "nombre" => $c->nombre,
            "oficio" => $c->oficio,
            "fecha_sol" => $c->fecha_sol,
            "fecha_vigencia" => $c->fecha_vigencia];

    }

    public
    function getClaveVehicular()
    {
        if (Request::ajax()) {

            $clave = Input::get('clave_vehicular');
            $vehiculo = $this->ClaveVehicular->findBy('clave_vehicular', $clave);

            if ($vehiculo == false) {
                $vehiculo = $this->clave($clave);

                if ($vehiculo == 500) {
                    return false;
                }

                $marca = $vehiculo->descripcionMarca;
                $linea = $vehiculo->descripcionLinea;
                $version = $vehiculo->descripcionModelo;
                $digito = $vehiculo->claveDigito;

                if ($marca == 'null' && $linea == 'null' && $version == 'null' && $digito == 'null') {
                    return 0;
                } else {

                    $claveVeh = $this->ClaveVehicular->create(['clave_vehicular' => $clave, 'digito' => $digito, 'marca' => $marca, 'linea' => $linea, 'version' => $version]);

                    return 1;

                }


            } else {
                return $vehiculo;

            }

        }

    }

    public
    function buscar_porqueria()
    {

        if (Request::ajax()) {

            $porqueria = Input::get('porqueria');
            $tipo = Input::get('tipo_propietario');

            $propietarios = $this->propietario->buscar_propietario_porqueria($porqueria, $tipo);
            if ($propietarios == false) {
                return 0;
            }

            return $propietarios;


        }
    }

    public
    function buscar_prop()
    {

        if (Request::ajax()) {
            $clave = Input::get('clave');
            $propietario = $this->propietario->buscar_propietario_ajax($clave);
            if ($propietario == false) {
                return 0;
            }

            $data = array(
                'propietario' => $propietario,
                'pais' => $propietario->pais,
                'colonia' => $propietario->colonia,
                'identificacion' => $propietario->identificacion);

            return $data;


        }
    }


    public
    function extraerCP()
    {
        if (Request::ajax()) {

            $cp = Input::get('cp');
            return $this->Colonia->extraerCP($cp);
        }
    }



}
