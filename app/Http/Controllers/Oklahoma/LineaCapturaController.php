<?php

namespace App\Http\Controllers\Oklahoma;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\LoteMaterialRepositorio as Lotematerial;
use App\Repositories\Services\Informix as Informix;
use App\Repositories\Services\REPUVE as Repuve;
use App\Repositories\Services\Lineas_Caprura as LC;
use App\Repositories\VehiculosRepositorio as Vehiculo;
use App\Repositories\CandadoRepositorio as Candado;
use App\Repositories\TramiteRepositorio as Tramite;
use App\Repositories\Services\Clave_Vehicular as Clave_Vehicular;
use App\Repositories\Services\DIF as Dif;
use App\Repositories\Catalogos\CatPaisVehiculoRepositorio as Cat_Pais;
use App\Repositories\Catalogos\CatCombustibleVehiculoRepositorio as Cat_Combustible;
use App\Repositories\Catalogos\CatTipoIdentificacionRepositorio as Cat_Identificaciones;

class LineaCapturaController extends Controller
{
    public function __construct(
        Lotematerial $lotematerial,
        Informix $informix,
        Repuve $repuve,
        LC $lc,
        Vehiculo $vehiculo,
        Candado $candado,
        Clave_Vehicular $Clave_Vehicular,
        Tramite $tramite,
        Dif $dif,
        Cat_Pais $Cat_Pais,
        Cat_Combustible $Cat_Combustible,
        Cat_Identificaciones $Cat_Identificaciones
    )

    {
        $this->lotematerial = $lotematerial;
        $this->informix = $informix;
        $this->Repuve = $repuve;
        $this->Clave_Vehicular = $Clave_Vehicular;
        $this->lc = $lc;
        $this->vehiculo = $vehiculo;
        $this->candado = $candado;
        $this->tramite = $tramite;
        $this->Dif = $dif;
        $this->Cat_Pais = $Cat_Pais;
        $this->Cat_Combustible = $Cat_Combustible;
        $this->Cat_Identificaciones = $Cat_Identificaciones;
        $this->middleware('auth');
    }

    public
    function vistaLineaPago_Alta(/*Request $request*/)
    {


        $tipos_placa = $this->lotematerial->tipos_placa_disponible(/*Auth::user()->modulo_id*/
            10);

        return view('LineaPago_Altas')
            ->with('tipos_placa', $tipos_placa);


    }

    public function validar_pago_alta(Request $request)
    {
        $quiero_validar = false;
        if ($quiero_validar) {
            ////////////////////////////////////////////  VALIDAR CORTESIA DIF //////////////////////////////////////////////////////////

            if ($request->id_tipo_placa == 2)
                if ($this->Dif->consultar($request->constancia)->resultado == "OK")
                    return redirect()->back()->withErrors(array('error', 'ERROR', "Cortesía de DIF fue Ocupada Anteriormente"));


            ////////////////////////////////////////////  VALIDAR NUMERO DE SERIE //////////////////////////////////////////////////////////

            $vehiculo = $this->vehiculo->getVehiculoActivo($request->serie);

            if ($vehiculo == 1)
                return redirect()->back()->withErrors(array('error', 'ERROR', "El Vehículo se encuentra ACTIVO"));
            elseif ($vehiculo == 2)
                return redirect()->back()->withErrors(array('error', 'ERROR', "El Vehículo se encuentra dado de BAJA"));
            elseif ($vehiculo == 3)
                return redirect()->back()->withErrors(array('error', 'ERROR', "El Vehículo se encuentra como CHATARRIZADO"));
            elseif ($vehiculo == null) {
                $vehiculo_informix = $this->informix->consultar($request->serie, 'vehiculo');
                if (isset($vehiculo_informix->vehiculo[0]->estatus_id))
                    if ($vehiculo_informix->vehiculo[0]->estatus_id == 1)
                        return redirect()->back()->withErrors(array('error', 'ERROR', "El Vehículo se encuentra ACTIVO"));
                    elseif ($vehiculo_informix->vehiculo[0]->estatus_id == 2)
                        return redirect()->back()->withErrors(array('error', 'ERROR', "El Vehículo se encuentra dado de BAJA"));
            }

            ////////////////////////////////////////////  VALIDAR REPUVE //////////////////////////////////////////////////////////

            $repuve = $this->Repuve->consultar('', $request->serie, 'robo');
            $repuve_numero = "";

            if ($repuve->return == "ERR:402")
                return redirect()->back()->withErrors(array('error', 'ERROR', "El Vehículo es ROBADOOOOOOOOOOOO"));

            $repuve_numero = $this->Repuve->numero_repuve($request->serie);

            ////////////////////////////////////////////  VALIDAR LINEA DE CAPTURA ///////////////////////////////////////////////////////

            $linea_usada = $this->tramite->buscar_lp($request->linea_pago);

            if (!$linea_usada)
                return redirect()->back()->withErrors(array('error', 'ERROR', "Línea de Captura fue Ocupada Anteriormente"));

            $linea_pagada = $this->informix->lc_altas($request->linea_pago);
            if ($linea_pagada->ERROR_MSG == "Linea de captura invalida" && $linea_pagada->ERROR_MSG == "Longitud de la cadena invalida")
                return redirect()->back()->withErrors(array('error', 'ERROR', "Línea de Captura es Invalida"));

            $linea_usada_informix = $this->informix->existe_lc($request->linea_pago);
            if ($linea_usada_informix->mensaje == 1)
                return redirect()->back()->withErrors(array('error', 'ERROR', "Línea de Captura Ocupada Anteriormente"));


            if ($request->serie == $linea_pagada->SERIE)
                return redirect()->back()->withErrors(array('error', 'ERROR', "Línea de Captura NO coincide con la Serie del Vehículo"));

            ///////////////////////////////////////////////  CANDADOS   //////////////////////////////////////////////////////////

            $candado = $this->candado->candado($request->serie);

            if ($candado == 1)
                return redirect()->back()->withErrors(array('error', 'ERROR', "El Vehículo tiene un candado"));

        }

        else {
            $repuve_numero = "0CCMI6LD";
            $datos = new \stdClass();
            $datos->SERIE = "1G1ND52J42M685931";
            $datos->RFC = "XAXX010101000";
            $datos->CLAVE_VEHICULAR = "0031904";
            $datos->PLACA = null;
            $datos->VALOR_FACTURA = "172419.00";
            $datos->PLACA_DE_OTRA_ENTIDAD = null;
            $datos->EJERCICIO_FISCAL = "2018";
            $datos->MODELO = "2002";
            $datos->FECHA_DE_FACTURACION = "20020823";
            $datos->TOTAL = "678.00";
            $datos->ENTIDAD_DE_ORIGEN = null;
            $datos->LINEA_CAPTURA = $request->linea;

        }
        $clave_vehicular = $this->Clave_Vehicular->consultar($datos->CLAVE_VEHICULAR);

        return view('alta_nuevo')
            ->with('constancia', $request->constancia)
            ->with('id_tipo_placa', $request->id_tipo_placa)
            ->with('serie', $request->serie)
            ->with('años', $datos->MODELO)
            ->with('clave_vehicular', $clave_vehicular)
            ->with('datos', $datos)
            ->with('repuve', $repuve_numero)
            ->with('linea_pago', $request->linea)
            ->with("tipoidentificacion", $this->Cat_Identificaciones->allBy('tipo_identificacion'))
            ->with("combustible", $this->Cat_Combustible->allBy('tipo_combustible'))
            ->with("pais", $this->Cat_Pais->allBy('pais'))
            ->with("procedencia", $this->Cat_Pais->allBy('pais'));


    }

}
