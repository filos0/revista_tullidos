<?php

namespace App\Http\Controllers;

use App\Http\Controllers\PDFController as PDF;
use App\Models\Revista\EventoModel as Evento;
use App\Models\Revista\Lote_materialModel as Material;
use App\Models\Revista\PropietarioModel as Propietario;
use App\Models\Revista\Revista_vehiculoModel as Revista;
use App\Models\Revista\RevistaModel as Periodo_Revista;
use App\Models\Revista\VehiculoModel as Vehiculo;
use App\Models\Revista\wsModel as WS;
use App\Repositories\Services\Clave_Vehicular as Clave_Vehicular;
use App\Repositories\Services\DIF as DIF;
use App\Repositories\Services\Informix as Informix;
use App\Repositories\Services\alfred_ws as alfred_ws;
use App\Repositories\Services\Tenencias as Tenencias;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\DocBlock;

class RevistaController extends Controller
{

    public function __construct(APIController $APIController, Periodo_Revista $Periodo_Revista, PDF $PDF, Tenencias $tenencias, Informix $Informix, DIF $DIF, Clave_Vehicular $Clave_Vehicular, WS $WS, alfred_ws $alfred_ws)
    {
        $this->middleware('auth');
        $this->APIController = $APIController;
        $this->PDF = $PDF;
        $this->WS = $WS;
        $this->alfred_ws = $alfred_ws;
        $this->Periodo_Revista = $Periodo_Revista;
        $this->Informix = $Informix;
        $this->tenencias = $tenencias;
        $this->DIF = $DIF;
        $this->Clave_Vehicular = $Clave_Vehicular;
    }


    public function index()
    {

        $mat_disponible = Material::where('id_modulo', Auth::user()->modulo_id)->where('disponible', '>', '0')->where('estatus', 'A')->get();

        if ($mat_disponible->isEmpty())
            return redirect()->back()->withErrors(array('error', 'No hay material disponible ', ':('));


        return view('QR_dif');


    }

    public function placa(Request $request)
    {

        try {


            DB::beginTransaction();


            $revista = Revista::where('constancia', $request->qr_dif)->first();
            if (isset($revista)) {

                Evento::insert(['users_id' => Auth::user()->id, 'constancia' => $request->qr_dif, 'evento' => 'Cortesía en uso', 'fecha' => date("Y-m-d H:i:s")]);
                DB::commit();


                $pdf = $this->PDF->pdf_constancia(
                    "cortesia_repetida",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    null,
                    $request->qr_dif);
                $resultado = "NO se aprobo la revista";


                return view('Finalizar')
                    ->withErrors(array('error', 'Cortesía Inválida', 'La Cortesía ya fue ocupada en el proceso de revista'))
                    ->with("pdf", $pdf)
                    ->with("resultado", $resultado)
                    ->with("qr_dif", $request->qr_dif)
                    ->with("placa", "-");


            }

            $datos_dif = $this->DIF->consultar($request->qr_dif);


            if (gettype($datos_dif) != "object")
                return redirect()->back()->withErrors(array('error', 'Web Service DIF no responde', 'Intentelo nuevamente: ' . $datos_dif));

            if ($datos_dif->resultado != "OK") {

                $datos_dif = $this->WS->where("folio", $request->qr_dif)->first();

                if (gettype($datos_dif) == "object") {


                    $datos_dif = (object)json_decode($datos_dif->array, true);

                    $datos_dif = array(

                        "resultado" => "OK",
                        "curp" => $datos_dif->curp,
                        "nombre" => $datos_dif->nombre,
                        "oficio" => "DIF-CDMX/DEDPD/DPER/PL-" . $datos_dif->folio . "/" . $datos_dif->anio,
                        "fecha_sol" => $datos_dif->fecha_sol,
                        "fecha_vigencia" => $datos_dif->fecha_vigencia

                    );

                    return view('Placa')
                        ->with("datos_dif", json_encode($datos_dif, true))
                        ->with("qr_dif", $request->qr_dif);

                }
                else {

                       /*   Solo utilizarla para pruebas*/

                    if ($request->qr_dif = "SoyUnByP4ss"){

                        $datos_dif = array(

                            "resultado" => "OK",
                            "curp" => "CURP_DE_PRUEBA",
                            "nombre" => "PRUEBA PRUEBA PRUEBA ",
                            "oficio" => "DIF-CDMX/DEDPD/DPER/PL-PRUEBA/201X",
                            "fecha_sol" => "01/01/2999",
                            "fecha_vigencia" => "01/01/2999"

                        );
                        return view('Placa')
                            ->with("datos_dif", json_encode($datos_dif, true))
                            ->with("qr_dif", $request->qr_dif);
                    }

                    Evento::insert(['users_id' => Auth::user()->id, 'constancia' => $request->qr_dif, 'evento' => 'Cortesía inexistente', 'fecha' => date("Y-m-d H:i:s")]);
                    DB::commit();


                    $pdf = $this->PDF->pdf_constancia(
                        "no_cortesia",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        null,
                        $request->qr_dif);

                    $resultado = "NO se aprobo la revista";


                    return view('Finalizar')
                        ->withErrors(array('error', 'Cortesía Inválida', 'La Cortesía no existe en la Base de Datos del DIF. (Procura no confundir letras)'))
                        ->with("pdf", $pdf)
                        ->with("resultado", $resultado)
                        ->with("qr_dif", $request->qr_dif)
                        ->with("placa", "-");


                }

            }


            return view('Placa')
                ->with("datos_dif", json_encode($datos_dif, true))
                ->with("qr_dif", $request->qr_dif);

        } catch (\Exception $e) {

            DB::rollBack();
            report($e);

            return view('errors.error_global')
                ->with('msg_1', str_limit($e->getMessage()))
                ->with('msg_2', $e->getCode());

        }

    }

    public function datos(Request $request)
    {
        try {

            DB::beginTransaction();


            $ano_revista = Periodo_Revista::where('año', date('Y'))->first();
            $revista = Revista::where('placa', $request->placa)->where('revista_id', $ano_revista->id_revista)->first();

            if (isset($revista)) {

                Evento::insert(['users_id' => Auth::user()->id, 'placa' => $request->placa, 'constancia' => $request->qr_dif, 'evento' => 'Esta placa ya finalizo revista', 'fecha' => date("Y-m-d H:i:s")]);
                DB::commit();

                $pdf = $this->PDF->pdf_constancia("placa_repetida","","","","",$request->placa,"","","","",null,$request->qr_dif);
                $resultado = "NO se aprobo la revista";

                return view('Finalizar')
                    ->withErrors(array('error', 'Placa Inválida', 'Esta placa ya finalizo revista: ' . $request->placa))
                    ->with("pdf", $pdf)
                    ->with("resultado", $resultado)
                    ->with("qr_dif", $request->qr_dif)
                    ->with("placa", $request->placa);

            }

            $datos_particular = $this->Informix->consultar($request->placa);

            if (gettype($datos_particular) == "integer") {

                if ($datos_particular == 500)
                    return view('home')->withErrors(array('error', 'Sistema de Informix no responde', 'Intentelo nuevamente: ' . $datos_particular));

                if ($datos_particular == 404){

                    $datos_particular = $this->alfred_ws->consultar_verde($request->placa);

                    if (gettype($datos_particular) == "integer") {

                        if ($datos_particular == 500)
                            return view('home')->withErrors(array('error', 'Sistema Alfred no responde', 'Intentelo nuevamente: ' . $datos_particular));

                        if ($datos_particular == 404)
                        {
                            Evento::insert(['users_id' => Auth::user()->id, 'placa' => $request->placa, 'constancia' => $request->qr_dif, 'evento' => 'No hay datos de la Placa', 'fecha' => date("Y-m-d H:i:s")]);
                            DB::commit();

                            $pdf = $this->PDF->pdf_constancia("no_placa", "", "", "", "", $request->placa, "", "", "", "", null, $request->qr_dif);

                            $resultado = "NO se aprobo la revista";

                            return view('Finalizar')
                                ->withErrors(array('error', 'Placa Inválida', 'No hay datos de la Placa: ' . $request->placa))
                                ->with("pdf", $pdf)
                                ->with("resultado", $resultado)
                                ->with("qr_dif", $request->qr_dif)
                                ->with("placa", $request->placa);
                        }

                    }

                }

            }


            $cv = $this->Clave_Vehicular->consultar($datos_particular->vehiculo[0]->clave_vehicular_id);

            if (gettype($cv) != "object")
                return view('home')->withErrors(array('error', 'Problemas con la Clave Vehicular ', $datos_particular->vehiculo[0]->clave_vehicular_id));

            $datos_dif = json_decode($request->datos_dif, true);
            return view('Datos')
                ->with("datos_particular", $datos_particular)
                ->with("datos_dif", $datos_dif)
                ->with("cv", $cv->toArray())
                ->with("qr_dif", $request->qr_dif)
                ->with("placa", $request->placa);


        } catch (\Exception $e) {

            report($e);
            DB::rollBack();

            return view('errors.error_global')
                ->with('msg_1', str_limit($e->getMessage()))
                ->with('msg_2', $e->getCode());

        }

    }

    public function documentacion(Request $request)
    {


        try {

            DB::beginTransaction();

            $datos_particular = json_decode($request->datos_particular, true);
            $datos_dif = json_decode($request->datos_dif, true);
            $cv = json_decode($request->cv, true);
            $placa_anterior = $datos_particular["tramite"][0]["placaant"];

            $tenencias = $this->tenencias->consultar($request->placa);

            if (gettype($tenencias) == "integer")
                return view('home')->withErrors(array('error', 'Sistema de Tenencias no responde', 'Intentelo nuevamente: ' . $tenencias));

            $tenencias_placa_anterior = null;

            if ($placa_anterior != null) {
                $tenencias_placa_anterior = $this->tenencias->consultar($placa_anterior);
                if (gettype($tenencias_placa_anterior) == "integer")
                    return view('home')->withErrors(array('error', 'Sistema de Tenencias no responde', 'Intentelo nuevamente: ' . $tenencias_placa_anterior));

                if ($tenencias_placa_anterior != null) {
                    $todas_tenencias = array_merge($tenencias, $tenencias_placa_anterior);
                    $todas_tenencias = array_unique($todas_tenencias);
                } else {
                    $todas_tenencias = array_unique($tenencias);
                }

            } else {
                $todas_tenencias = array_unique($tenencias);
            }

            if (count($todas_tenencias) > 6)
                $todas_tenencias = array_slice($todas_tenencias, 0, 6);

            if ($request->parentesco == "No acredito parentesco") {

                $pdf = $this->PDF->pdf_constancia("no_acredito_parentesco", $datos_dif["nombre"], $datos_dif["curp"], $datos_dif["oficio"], $datos_dif["fecha_sol"], $request->placa, $cv["marca"], $datos_particular["vehiculo"][0]["modelo"], $datos_particular["vehiculo"][0]["serie_vehicular"], $datos_particular["vehiculo"][0]["numero_motor"], $todas_tenencias, $request->qr_dif);


                $resultado = "NO se aprobo la revista";

                Evento::insert(['users_id' => Auth::user()->id, 'placa' => $request->placa, 'constancia' => $request->qr_dif, 'evento' => $request->parentesco, 'fecha' => date("Y-m-d H:i:s")]);
                DB::commit();

                return view('Finalizar')
                    ->with("pdf", $pdf)
                    ->with("resultado", $resultado)
                    ->with("qr_dif", $request->qr_dif)
                    ->with("placa", $request->placa);
            } else {


                //dd($todas_tenencias);
                return view('Documentacion')
                    ->with("tenencias", $todas_tenencias)
                    ->with("datos_particular", $request->datos_particular)
                    ->with("datos_dif", $request->datos_dif)
                    ->with("parentesco", $request->parentesco)
                    ->with("qr_dif", $request->qr_dif)
                    ->with("cv", $request->cv)
                    ->with("placa", $request->placa);
            }
        } catch (\Exception $e) {

            DB::rollBack();
            report($e);

            //dd($e->getCode());

            return view('errors.error_global')
                ->with('msg_1', str_limit($e->getMessage()))
                ->with('msg_2', $e->getCode());

        }


    }

    public function finalizar(Request $request)
    {


        try {

            DB::beginTransaction();

            $datos_dif = json_decode($request->datos_dif, true);
            $datos_particular = json_decode($request->datos_particular, true);
            $tenencias = json_decode($request->tenencias, true);
            $cv = json_decode($request->cv, true);

            $tipo_v = str_replace(' ', '', $datos_particular["vehiculo"][0]["clase_tipo_vehiculo_id"]);
            $tipo_v = $this->APIController->tipo_vehiculo($tipo_v);

            $revista = Revista::where('constancia', $request->qr_dif)->first();

            if (isset($revista))
                return view('errors.error_global')
                    ->with('msg_1', "Error! Llamar a sistemas")
                    ->with('msg_2', "No debes ACTUALIZAR o REFRESCAR la página");


            if ($request->resultado == "aprobado") {
                $resultado = "Se APROBO la revista";


                $observaciones =
                    json_encode(array([
                        'comprobante_credencial' => (isset($request->comprobante_credencial)) ? 'SI' : 'NO',
                        'comprobante_domicilio' => (isset($request->comprobante_domicilio)) ? 'SI' : 'NO',
                        'comprobante_factura' => (isset($request->comprobante_factura)) ? 'SI' : 'NO',
                        'comentarios' => (isset($request->comentarios)) ? $request->comentarios : 'SIN COMENTARIOS',
                        'parentesco' => $request->parentesco,
                        'tenencias' => ($tenencias[0] == null) ? "SIN TENENCIAS PAGADAS" : $tenencias
                    ]), true);


                $pdf_holograma = $this->PDF->pdf_holograma($datos_dif["nombre"], $cv["marca"], $datos_particular["vehiculo"][0]["modelo"], $tipo_v, $request->placa, ($datos_particular["vehiculo"][0]["numero_motor"] == "") ? "SIN NUMERO" : $datos_particular["vehiculo"][0]["numero_motor"], $datos_dif["oficio"]);

                /* Vehiculo */

                $vehiculo = Vehiculo::create([
                    "serie_vehicular" => $datos_particular["vehiculo"][0]["serie_vehicular"],
                    "clave_vehicular" => $datos_particular["vehiculo"][0]["clave_vehicular_id"],
                    "modelo" => $datos_particular["vehiculo"][0]["modelo"],
                    "numero_repuve" => $datos_particular["vehiculo"][0]["numero_repuve"],
                    "numero_puertas" => $datos_particular["vehiculo"][0]["numero_puertas"],
                    "numero_motor" => $datos_particular["vehiculo"][0]["numero_motor"],
                    "numero_cilindros" => $datos_particular["vehiculo"][0]["numero_cilindros"],
                    "tipo_servicio_id" => $datos_particular["vehiculo"][0]["tipo_servicio_id"],
                    "uso_vehiculo_id" => $datos_particular["vehiculo"][0]["uso_vehiculo_id"],
                    "clase_tipo_vehiculo_id" => $datos_particular["vehiculo"][0]["clase_tipo_vehiculo_id"],
                    "pais_id" => $datos_particular["vehiculo"][0]["pais_id"]
                ]);

                /* Propietario */



                $propietario = Propietario::create([
                    "clave_unica" => $datos_particular["propietario"][0]["clave_unica"],
                    "primer_apellido" => $datos_particular["propietario"][0]["primer_apellido"],
                    "segundo_apellido" => $datos_particular["propietario"][0]["segundo_apellido"],
                    "nombre" => $datos_particular["propietario"][0]["nombre_razon_social"],
                    "fecha_nacimiento" => $datos_particular["propietario"][0]["fecha_nacimiento"],
                    "telefono" => ($datos_particular["propietario"][0]["telefono"])??null,
                    "pais_id" => ($datos_particular["propietario"][0]["pais_id"]) ?? 82
                ]);


                /* Revista vehiculo*/
                $revista_vehiculo = Revista::create([
                    'fecha_revista' => date("Y-m-d H:i:s"),
                    'constancia' => $request->qr_dif,
                    'constancia_data' => $request->datos_dif,
                    'modulo_id' => Auth::user()->modulo_id, //crear relacion de modulo
                    'users_id' => Auth::user()->id,
                    'vehiculo_id' => $vehiculo->id_vehiculo,
                    'propietario_id' => $propietario->id_propietario,
                    'placa' => $request->placa,
                    'Observaciones' => $observaciones,
                    'revista_id' => 1

                ]);

                Evento::insert(['users_id' => Auth::user()->id, 'placa' => $request->placa, 'constancia' => $request->qr_dif, 'evento' => "Revista finalizada con éxito ", 'fecha' => date("Y-m-d H:i:s")]);


                DB::commit();


                return view('Finalizar')
                    ->with("pdf_holograma", $pdf_holograma)
                    ->with("datos_dif", $request->datos_dif)
                    ->with("datos_particular", $request->datos_particular)
                    ->with("datos_cv", $cv["marca"])
                    ->with("tenencias", $request->tenencias)
                    ->with("resultado", $resultado)
                    ->with("parentesco", $request->parentesco)
                    ->with("qr_dif", $request->qr_dif)
                    ->with("id_revista_vehiculo", $revista_vehiculo->id_revista_vehiculo)
                    ->with("placa", $request->placa);
            } else {
                $resultado = "NO se aprobo la revista";

                $pdf = $this->PDF->pdf_constancia("no_acredito_documentacion", $datos_dif["nombre"], $datos_dif["curp"], $datos_dif["oficio"], $datos_dif["fecha_sol"], $request->placa, $cv["marca"], $datos_particular["vehiculo"][0]["modelo"], $datos_particular["vehiculo"][0]["serie_vehicular"], $datos_particular["vehiculo"][0]["numero_motor"], $tenencias, $request->qr_dif);


                Evento::insert(['users_id' => Auth::user()->id, 'placa' => $request->placa, 'constancia' => $request->qr_dif, 'evento' => "No acredito la documentación", 'fecha' => date("Y-m-d H:i:s")]);
                DB::commit();

                return view('Finalizar')
                    ->with("pdf", $pdf)
                    ->with("resultado", $resultado)
                    ->with("qr_dif", $request->qr_dif)
                    ->with("placa", $request->placa);


            }


        } catch (\Exception $e) {

            DB::rollBack();
            report($e);

            //dd($e->getCode());

            return view('errors.error_global')
                ->with('msg_1', str_limit($e->getMessage()))
                ->with('msg_2', $e->getCode());

        }

    }

    public function tramite(Request $request)
    {
        $user = \Auth::user();

        if ($user->hasRole(['administrador', 'director'])) {
            $tramite = Revista::where("constancia", $request->folio_qr)->orwhere("placa", $request->placa)->get();
            if ($tramite->isEmpty())
                return redirect()->back()->withErrors(array('error', 'No hay ningún trámite ', 'Verifica los datos'));
            else
                return view("Admin.Tramite")->with("tramite", $tramite->first());
        }

    }

    public function tramite_cancelar(Request $request)
    {
        $user = \Auth::user();

        if ($user->hasRole(['administrador', 'director'])) {
            try {
                $tramite = Revista::where("id_revista_vehiculo", $request->id_revista)->get()->first();

                $tramite->observacion = "Cancelación por oficio: " . $request->folio_oficio_;
                $tramite->deleted_at = date('Y-m-d h:i:s');
                $tramite->save();
                return redirect()->back()->withErrors(array('success', 'Trámite Cancelado', ''));
            } catch (\Exception $e) {
                return redirect()->back()->withErrors(array('error', 'No se pudo cancelar el oficio :(', 'Hazlo manualmente'));
            }


        }
    }

}
