<?php

namespace App\Http\Controllers;

use App\Models\Revista\Revista_vehiculoModel;
use Illuminate\Http\Request;
use App\Models\Revista\Cat_moduloModel as Modulo;
use App\Models\Revista\Lote_materialModel as Material;
use App\Models\Revista\Incidencia_loteModel as Incidencia;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Validator;


class MaterialController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

    }


    public function index()
    {
        $user = \Auth::user();
        if ($user->hasRole(['administrador', 'director'])) {

            $modulos = Modulo::all();
            $incidencias = Incidencia::all();
            return view('Admin.Admin_Material')
                ->with('incidencias', $incidencias)
                ->with('modulos', $modulos);


        }
    }

    public function agregar_material(Request $request)
    {


        $rango_in = $request->rango_inicial;
        $rango_fin = $request->rango_final;

        $request->rango_inicial = str_pad($rango_in, 6, "0", STR_PAD_LEFT);
        $request->rango_final = str_pad($rango_fin, 6, "0", STR_PAD_LEFT);

        //dd($request->rango_inicial);
        $folios = array();

        /*if (strlen($rango_in) < 6 || strlen($rango_fin) < 6) {
            return redirect()->back()->withErrors(array('error', 'Formato Inválido', 'El formato de los folios es erroneo'));
        }
*/
        if (is_numeric($rango_in) && is_numeric($rango_fin) || $rango_in > $rango_fin) {

            $a = $rango_fin - $rango_in;
            $b = 0;

            for ($i = 0; $i <= $a; $i++) {
                $n = $rango_in + $b;
                if ($n <= 999999 && $n > 99999)
                    $folios[$b] = $n;
                elseif ($n <= 99999 && $n > 9999)
                    $folios[$b] = '0' . $n;
                elseif ($n <= 9999 && $n > 999)
                    $folios[$b] = '00' . $n;
                elseif ($n <= 999 && $n > 99)
                    $folios[$b] = '000' . $n;
                elseif ($n <= 99 && $n > 9)
                    $folios[$b] = '0000' . $n;
                else
                    $folios[$b] = '00000' . $n;
                $b++;
            }
            //qdd($folios);
            if (count($folios) != $request->cantidad) {
                // dd();
                return redirect()->back()->withErrors(array('error', 'Cantidad incorrecta', 'La cantidad no coincide con el rango de material proporcionado'));

            } else {
                $exite = $this->existencia($folios);
                if ($exite == 1) {
                    // dd($exite);

                    return redirect()->back()->withErrors(array('error', 'Folios incorrectos', 'Los folios ya se encuentran dados de alta'));


                } elseif ($exite == 2) {
                    return redirect()->back()->withErrors(array('error', 'Folios incorrectos', 'Los folios ya fueron descartados anteriormente'));
                }
            }
        } else {
            return redirect()->back()->withErrors(array('error', 'Error', 'Exixte un problema con los datos, favor de verificarlo'));

        }

        try {

            DB::beginTransaction();
            Material::insert([
                'id_modulo' => $request->id_modulo,
                'cat_tipo_material_id' => 1,
                'fecha_asignacion' => date('Y-m-d'),
                'total_lote' => $request->cantidad,
                'usado' => 0,
                'disponible' => $request->cantidad,
                'numero_rango_inicio' => $request->rango_inicial,
                'numero_rango_fin' => $request->rango_final,
                'estatus' => "A"
            ]);

            DB::commit();
            return redirect()->back()->withErrors(array('success', 'Éxito', 'Material agregado correctamente'));

        } catch (\Exception $e) {
            report($e);
            DB::rollBack();

            //dd($e->getCode());

            return view('errors.error_global')
                ->with('msg', str_limit($e->getMessage()))
                ->with('code', $e->getCode());

        }

    }


    private function existencia($p)
    {

        $inicio = Material::whereIn('numero_rango_inicio', $p)->first();
        $fin = Material::whereIn('numero_rango_fin', $p)->first();
        $incidencia = Incidencia::whereIn('folio_material', $p)->first();

        if (isset($inicio) || isset($fin)) {
            return 1;
        } elseif (isset($incidencia)) {
            return 2;
        } else {

            return 0;

        }

    }

    public function holograma_en_lote($holograma)
    {
        $lote = Material::where('id_modulo', \Auth::user()->modulo_id)->where('disponible', '>', '0')->where('estatus', 'A')->get()->first();
        if ($lote == null)
            return 'no_hay_lote';

        $rango_in = $lote->numero_rango_inicio;
        $rango_fin = $lote->numero_rango_fin;
        $folios = array();
        $a = 0;

        foreach (range($rango_in, $rango_fin) as $folio) {
            if ($folio <= 999999 && $folio > 99999)
                $folios[] = $folio;
            elseif ($folio <= 99999 && $folio > 9999)
                $folios[] = '0' . $folio;
            elseif ($folio <= 9999 && $folio > 999)
                $folios[] = '00' . $folio;
            elseif ($folio <= 999 && $folio > 99)
                $folios[] = '000' . $folio;
            elseif ($folio <= 99 && $folio > 9)
                $folios[] = '0000' . $folio;
            else
                $folios[] = '00000' . $folio;
        }

        if (in_array($holograma, $folios)) {
            return "folio_disponible";
        } else {
            return "no_hay_lote";
        }


    }

    public function get_lote_modulo()
    {
        $lote = Material::where('id_modulo', \Auth::user()->modulo_id)->where('disponible', '>', '0')->where('estatus', 'A')->get()->first();
        return $lote->id_lote_material;
    }

    public function lote_modulo($modulo_id)
    {
        $lote = Material::where('id_modulo', $modulo_id)->get();
        return $lote;
    }

    public function reducir_lote_modulo()
    {
        $lote = Material::where('id_modulo', \Auth::user()->modulo_id)->where('disponible', '>', '0')->where('estatus', 'A')->get()->first();
        $lote->usado = $lote->usado + 1;
        $lote->disponible = $lote->disponible - 1;
        $lote->save();
    }
}


