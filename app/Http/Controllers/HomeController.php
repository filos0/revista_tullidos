<?php

namespace App\Http\Controllers;

use App\Repositories\LoteMaterialRepositorio;
use Illuminate\Http\Request;
use App\Repositories\LoteMaterialRepositorio as Lote;
use App\Models\Revista\Revista_vehiculoModel as Revista;

class HomeController extends Controller
{

    public function __construct(Lote $lote)
    {
        $this->middleware('auth');
        $this->Lote = $lote;

    }


    public function index()
    {
        $user = \Auth::user();
        if ($user->hasRole(['administrador', 'director'])) {
            $fecha = date('Y-m-d');
            $revistas = Revista::whereDate('fecha_revista', $fecha)->get();
            return view('home')
                ->with('revistas', $revistas);
        }
        else
            return view('home');
    }


}
