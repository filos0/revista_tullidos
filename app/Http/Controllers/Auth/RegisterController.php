<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Models\Revista\Cat_moduloModel as Modulo;
use Illuminate\Support\Facades\DB;
use Illuminate\Auth\Events\Registered;
use App\Models\Role as Roles;
use Illuminate\Http\Request;

class RegisterController extends Controller
{

    use RegistersUsers;
    protected $redirectTo = '/';

    public function __construct(Modulo $Modulo, Roles $Roles)
    {
        $this->Modulo = $Modulo;
        $this->Roles = $Roles;
        $this->middleware('auth');
    }


    public function showRegistrationForm()
    {
        $user = \Auth::user();


        if ($user->hasRole(['administrador', 'registro_usuarios'])) {
            $modulos = $this->Modulo->all();
            $roles = $this->Roles->all();
            return view('auth.register')
                ->with("roles", $roles)
                ->with("modulos", $modulos);
        }
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [

            'rfc' => 'unique:users,rfc'
        ], [

            'rfc.unique' => 'EL RFC ya fue utilizado',
        ]);
    }


    protected function create(array $data)
    {
        try {

            DB::beginTransaction();
            $user = User::create([
                'rfc' => $data['rfc'],
                'email' => $data['rfc'],
                'name' => $data['nombre'],
                'primer_apellido' => $data['primer_apellido'],
                'segundo_apellido' => $data['segundo_apellido'],
                'password' => bcrypt('123456'),
                'modulo_id' => $data['modulo'],
                'estatus_id' => 3,
            ]);
            $user->attachRole($data['rol']);

            DB::commit();
            return $user;
        } catch (\Exception $e) {

            DB::rollBack();
            report($e);

            return view('errors.error_global')
                ->with('msg_1', str_limit($e->getMessage()))
                ->with('msg_2', $e->getCode());

        }
    }


    public function register(Request $request)
    {
        $user = User::where('rfc',$request->rfc)->get()->first();

        if ($user != null)
            return redirect()->back()->withErrors(array('error',  'El RFC ya se encuentra en uso',$request->rfc));


        event(new Registered($user = $this->create($request->all())));

        if (isset($user->id))
            return redirect()->back()->withErrors(array('success', 'Usuario Creado Correctamente', ''));
        else
            return $user;


    }
}
