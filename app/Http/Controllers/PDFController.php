<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PDFController extends Controller
{
    public function pdf_holograma($nombre, $marca, $modelo, $tipo, $placa, $no_motor, $oficio)
    {

        $view = \View::make('PDF/Holograma_Discapacitado', compact('nombre', 'marca', 'modelo', 'tipo', 'placa', 'no_motor', 'oficio'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $pdf->setPaper("letter", 'portrait');
        //return $pdf->stream();

        $output = $pdf->output();
        // dd($placa);
        $nombre_pdf = $placa . '_holograma.pdf';
        file_put_contents($nombre_pdf, $output);
        return $nombre_pdf;

    }
    public function pdf_constancia($razon,$nombre,$curp_rfc,$oficio_dif,$fecha_exp,$placa,$marca,$modelo,$num_serie,$num_motor,$tenencias,$qr,$folio_holograma=""){
        //dd($razon);

        $view = \View::make('PDF/Constancia', compact('razon','nombre','curp_rfc','oficio_dif','fecha_exp', 'placa', 'marca', 'modelo', 'num_serie', 'num_motor','tenencias','qr','folio_holograma'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $pdf->setPaper("letter", 'portrait');

        $output = $pdf->output();
        // dd($placa);
        $nombre_pdf = $placa . '_constancia.pdf';
        file_put_contents($nombre_pdf, $output);
        return $nombre_pdf;

    }

    public function pdf_prueba_constancia($razon="aprobado",$nombre="MACIAS CARRILLO ANDRES",$curp_rfc="MACA940219HDFCRN03",$oficio_dif="DIF-CDMX/DEDPD/DPER/PL-3266/2017",$fecha_exp="06/12/2017",$placa="666XX",$marca="MARCA",$modelo="MODELO",$num_serie="ABCD123456789",$num_motor="HECHO EN MEX",$qr="TlRVek1UYzBOVFE9",$folio_holograma="000102")
    {
        $tenencias = array('2018','2017');
        $view = \View::make('PDF/Constancia', compact('razon','nombre','curp_rfc','oficio_dif','fecha_exp', 'placa', 'marca', 'modelo', 'num_serie', 'num_motor','qr','tenencias','folio_holograma'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $pdf->setPaper("letter", 'portrait');
        return $pdf->stream();

    }
    public function pdf_prueba_holograma($nombre= "MACIAS CARRILLO ANDRES", $marca = "MARCA", $modelo = 2014, $tipo = "TIPO", $placa = "666XX", $no_motor="HECHO EN MEXICO", $oficio= "DIF-CDMX/DEDPD/DPER/PL-3266/2017")
    {

        $view = \View::make('PDF/Holograma_Discapacitado', compact('nombre', 'marca', 'modelo', 'tipo', 'placa', 'no_motor', 'oficio'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $pdf->setPaper("letter", 'portrait');
        return $pdf->stream();



    }
}
