<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\Revista\Incidencia_loteModel as Incidencia;
use App\Models\Revista\Revista_vehiculoModel as Revista;
use App\Models\Revista\HologramaModel as Holograma;
use App\Models\Revista\EventoModel as Evento;
use App\Repositories\Services\Informix as Informix;
use App\Repositories\Services\Tenencias as Tenencias;
use App\Http\Controllers\APIController as APIController;
use App\Repositories\Services\Clave_Vehicular as Clave_Vehicular;
use App\Repositories\Services\DIF as DIF;
use App\Http\Controllers\PDFController as PDF;


class ReimpresionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Incidencia $Incidencia, Evento $Evento, Revista $Revista, Holograma $Holograma, Informix $Informix, Tenencias $Tenencias, APIController $APIController, Clave_Vehicular $Clave_Vehicular, DIF $DIF, PDF $PDF)
    {
        $this->middleware('auth');
        $this->Incidencia = $Incidencia;
        $this->Holograma = $Holograma;
        $this->Informix = $Informix;
        $this->Tenencias = $Tenencias;
        $this->Evento = $Evento;
        $this->PDF = $PDF;
        $this->DIF = $DIF;
        $this->Revista = $Revista;
        $this->APIController = $APIController;
        $this->Clave_Vehicular = $Clave_Vehicular;

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function buscar(Request $request)
    {
        $revista = $this->Revista->where("constancia", $request->folio_qr)->where("placa", $request->placa)->get()->first();
        if ($revista == null)
            return view('Impresion')
                ->withErrors(array('error', 'Datos Incorrectos', 'No existen datos de alguna revista con los datos otorgados'));
        if ($revista->holograma != null)
            return view('Impresion')
                ->withErrors(array('error', 'Ya se imprimio el holograma de la revista', ''));

        $datos_particular = $this->Informix->consultar($request->placa);

        if (gettype($datos_particular) != "object")
            return view('Impresion')->withErrors(array('error', 'Sistema de Informix no responde', 'Intentelo nuevamente: ' . $datos_particular));


        $datos_dif = $this->DIF->consultar($request->folio_qr);

        if (gettype($datos_dif) != "object")
            return view('Impresion')->withErrors(array('error', 'Sistema de DIF no responde', 'Intentelo nuevamente: ' . $datos_dif));

        $cv = $this->Clave_Vehicular->consultar($datos_particular->vehiculo[0]->clave_vehicular_id);

        if ($cv == null)
            return view('Impresion')->withErrors(array('error', 'La Clave Vehicular No existe', $datos_particular->vehiculo[0]->clave_vehicular_id));

        $tenencias = $this->Tenencias->consultar($request->placa);
        if (gettype($tenencias) == "integer")
            return view('Impresion')->withErrors(array('error', 'Sistema de Tenencias no responde', 'Intentelo nuevamente: ' . $tenencias));

        $tenencias_placa_anterior = null;

        $placa_anterior = $datos_particular->tramite[0]->placaant;

        if ($placa_anterior != null) {
            $tenencias_placa_anterior = $this->Tenencias->consultar($placa_anterior);
            if (gettype($tenencias_placa_anterior) == "integer")
                return view('Impresion')->withErrors(array('error', 'Sistema de Tenencias no responde', 'Intentelo nuevamente: ' . $tenencias_placa_anterior));

            if ($tenencias_placa_anterior != null) {
                $todas_tenencias = array_merge($tenencias, $tenencias_placa_anterior);
                $todas_tenencias = array_unique($todas_tenencias);
            } else {
                $todas_tenencias = array_unique($tenencias);
            }

        } else {
            $todas_tenencias = array_unique($tenencias);
        }

        if (count($todas_tenencias) > 6)
            $todas_tenencias = array_slice($todas_tenencias, 0, 6);

        $tipo_v = str_replace(' ', '', $datos_particular->vehiculo[0]->clase_tipo_vehiculo_id);
        $tipo_v = $this->APIController->tipo_vehiculo($tipo_v);

        $pdf_holograma = $this->PDF->pdf_holograma(
            $datos_dif->nombre,
            $cv->marca,
            $datos_particular->vehiculo[0]->modelo,
            $tipo_v,
            $request->placa,
            ($datos_particular->vehiculo[0]->numero_motor == "") ? "SIN NÚMERO" : $datos_particular->vehiculo[0]->numero_motor,
            $datos_dif->oficio
        );
        return view('Impresion')
            ->with("pdf_holograma", $pdf_holograma)
            ->with("datos_dif", $datos_dif)
            ->with("datos_particular", $datos_particular)
            ->with("datos_cv", $cv->marca)
            ->with("tenencias", $todas_tenencias)
            ->with("qr_dif", $request->folio_qr)
            ->with("id_revista_vehiculo", $revista->id_revista_vehiculo)
            ->with("placa", $request->placa);


    }

}
