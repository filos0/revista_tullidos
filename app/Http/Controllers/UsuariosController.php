<?php

namespace App\Http\Controllers;

use App\Models\Role as Roles;
use App\Models\User as Usuarios;
use App\Models\Revista\Cat_moduloModel as Modulo;
use Illuminate\Http\Request;


class UsuariosController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index()
    {

        $user = \Auth::user();


        if ($user->hasRole(['administrador', 'director'])) {
            $usuarios = Usuarios::all();
            $modulos = Modulo::all();
            $roles = Roles::all();

            return view('Admin.Admin_Usuarios')
                ->with('Roles', $roles)
                ->with('Usuarios', $usuarios)
                ->with('Modulos', $modulos);


        }
    }

    public function baja_usuario(Request $request)
    {
        $user = \Auth::user();

        if ($user->hasRole(['administrador', 'director'])) {
            try {
                $usuario = Usuarios::where('id', $request->modal_usuario)->get()->first();
                $usuario->estatus_id = 2;
                $usuario->save();
                return redirect()->back()->withErrors(array('success', 'Usuario Dado de Baja', ''));
            } catch (\Exception $e) {
                return redirect()->back()->withErrors(array('error', 'Algo paso :(', ''));
            }


        }
    }

    public function alta_usuario(Request $request)
    {
        $user = \Auth::user();

        if ($user->hasRole(['administrador', 'director'])) {
            try {
                $usuario = Usuarios::where('id', $request->modal_usuario)->get()->first();
                $usuario->estatus_id = 1;
                $usuario->save();
                return redirect()->back()->withErrors(array('success', 'Usuario Dado de Alta', ''));
            } catch (\Exception $e) {
                return redirect()->back()->withErrors(array('error', 'Algo paso :(', ''));
            }
        }
    }

    public function restablecer_contrasena(Request $request)
    {
        $user = \Auth::user();

        if ($user->hasRole(['administrador', 'director'])) {
            try {
                $usuario = Usuarios::where('id', $request->modal_usuario)->get()->first();
                $usuario->estatus_id = 3;
                $usuario->password = bcrypt("123456");
                $usuario->save();
                return redirect()->back()->withErrors(array('success', 'Contraseña Restablecida', ''));
            } catch (\Exception $e) {
                return redirect()->back()->withErrors(array('error', 'Algo paso :(', ''));
            }
        }
    }


}
