<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\Revista\Incidencia_loteModel as Incidencia;
use App\Models\Revista\Revista_vehiculoModel as Revista;
use App\Models\Revista\HologramaModel as Holograma;
use App\Models\Revista\EventoModel as Evento;
use App\Models\Revista\Cat_modulos as Modulo;

class ReportesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Incidencia $Incidencia, Evento $Evento, Revista $Revista, Holograma $Holograma, Modulo $Modulo)
    {
        $this->middleware('auth');
        $this->Incidencia = $Incidencia;
        $this->Holograma = $Holograma;
        $this->Modulo = $Modulo;
        $this->Evento = $Evento;
        $this->Revista = $Revista;

    }

    public function index()
    {
        $user = \Auth::user();


        if ($user->hasRole(['administrador', 'director'])) {

            return view('Reportes/Reportes')
                ->with('modulos', $this->Modulo->all());
        }
    }

    public function generar_reportes(Request $request)
    {
        $user = \Auth::user();


        if ($user->hasRole(['administrador', 'director'])) {


            if ($request->dia_inicio == $request->dia_fin) {

                if ($request->modulo == "TODOS") {
                    $revistas = $this->Revista->whereDate('fecha_revista', $request->dia_inicio)
                        ->orderBy('fecha_revista', 'asc')
                        ->get();

                    $incidencias = $this->Incidencia->whereDate('fecha', $request->dia_inicio)
                        ->orderBy('fecha', 'asc')
                        ->get();
                    $modulo = "TODOS";
                } else {
                    $revistas = $this->Revista->whereDate('fecha_revista', $request->dia_inicio)
                        ->where('modulo_id', $request->modulo)
                        ->orderBy('fecha_revista', 'asc')
                        ->get();

                    $incidencias = $this->Incidencia->whereDate('fecha', $request->dia_inicio)
                        ->orderBy('fecha', 'asc')
                        ->get();


                    $modulo = $this->Modulo->where("id_modulo", $request->modulo)->get()->first()->modulo;
                }

            } else {
                if ($request->modulo == "TODOS") {

                    $revistas = $this->Revista->whereDate('fecha_revista', '>=', $request->dia_inicio)
                        ->whereDate('fecha_revista', '<=', $request->dia_fin)
                        ->orderBy('fecha_revista', 'asc')
                        ->get();

                    $incidencias = $this->Incidencia->whereDate('fecha', '>=', $request->dia_inicio)
                        ->whereDate('fecha', '<=', $request->dia_fin)
                        ->orderBy('fecha', 'asc')
                        ->get();
                    $modulo = "TODOS";
                } else {
                    $revistas = $this->Revista->whereDate('fecha_revista', '>=', $request->dia_inicio)
                        ->whereDate('fecha_revista', '<=', $request->dia_fin)
                        ->where('modulo_id', $request->modulo)
                        ->orderBy('fecha_revista', 'asc')
                        ->get();

                    $incidencias = $this->Incidencia->whereDate('fecha', '>=', $request->dia_inicio)
                        ->whereDate('fecha', '<=', $request->dia_fin)
                        ->orderBy('fecha', 'asc')
                        ->get();


                    $modulo = $this->Modulo->where("id_modulo", $request->modulo)->get()->first()->modulo;

                }
            }


            return view('Reportes/Reportes')
                ->With('fecha_inicio', $request->dia_inicio)
                ->With('fecha_fin', $request->dia_fin)
                ->With('modulo', $modulo)
                ->With('Revistas', $revistas)
                ->With('Incidencias', $incidencias);


        }
    }

    public function hoy()
    {
        $user = \Auth::user();


        if ($user->hasRole(['administrador', 'director'])) {

            $hoy = date('Y-m-d');

            $revistas = $this->Revista->whereDate('fecha_revista', $hoy)->get();
            $incidencias = $this->Incidencia->whereDate('fecha', $hoy)->get();
            return view('Reportes/Reporte_Hoy')
                ->With('Revistas', $revistas)
                ->With('Incidencias', $incidencias);
        }
    }

}
