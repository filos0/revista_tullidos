<?php

namespace App\Http\Controllers;


use App\Repositories\Services\Tenencias;
use Request;
use App\Models\Revista\HologramaModel as Holograma;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\PDFController as PDF;

use App\Models\Revista\Incidencia_loteModel as Incidencia_lote;

use App\Repositories\Catalogos\CatClaveVehicularRepositorio as ClaveVehicular;
use App\Models\Revista\Revista_vehiculoModel as Revista;
use App\Repositories\Services\Tenencias as Ten;
use App\Repositories\Services\Informix as Info;
use App\Repositories\Services\DIF as Dif;
use App\Repositories\Services\Clave_Vehicular as clave;


class APIController extends Controller
{

    public function __construct(MaterialController $MaterialController, Ten $tenencias, Info $info,
                                PDF $PDF, Dif $dif, ClaveVehicular $ClaveVehicular, clave $clave)
    {
        $this->MaterialController = $MaterialController;

        $this->tenencias = $tenencias;
        $this->info = $info;
        $this->dif = $dif;
        $this->PDF = $PDF;
        $this->ClaveVehicular = $ClaveVehicular;
        $this->clave = $clave;
        //$this->middleware('auth');
    }


    public function servicios()
    {

        dd($this->info->comprobar_vehiculo('515YXW'));
    }

    public function index()
    {
        return view('QR_dif');
    }


    function tramites($placa)
    {


        $requestData = array
        (
            "placa" => $placa
        );
        $request = json_encode($requestData);

        $url = 'http://128.222.200.45/webservice/smirnoff/tramites/' . $placa;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        // curl_setopt($ch, CURLOPT_POST, count($request));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
        $response = json_decode(curl_exec($ch), true);
        return $response;

    }


    function tipo_vehiculo($id_tipo_vehiculo)
    {
        switch ($id_tipo_vehiculo) {
            case 1:
                return "Convertible";
                break;
            case 2:
                return "Couper";
                break;
            case 3:
                return "Jeep";
                break;
            case 4:
                return "Limousine";
                break;
            case 5:
                return "Sedan";
                break;
            case 6:
                return "Sport";
                break;
            case 7:
                return "Vagoneta";
                break;
            case 8:
                return "No especificado";
                break;
            case 19:
                return "Hatchback";
                break;
            case 27:
                return "Hard top";
                break;
            case 28:
                return "No especificado";
                break;

            default:
                return "No especificado";
                break;

        }
    }

    function validar_folio_holograma()
    {


        if (Request::ajax()) {

            $folio_holograma = Input::get('folio_holograma');

            $res = Holograma::where('folio_holograma', $folio_holograma)->first();
            $inc = Incidencia_lote::where('folio_material', $folio_holograma)->first();

            if ($res != null)
                return "folio_ocupado";
            if ($inc != null)
                return "folio_ocupado";

            else {
                return $this->MaterialController->holograma_en_lote($folio_holograma);
            }

        }
    }

    function material_modulo()
    {


        if (Request::ajax()) {

            $material = $this->MaterialController->lote_modulo(Input::get('id_modulo'));

            return $material;

        }
    }

    function holograma()
    {

        if (Request::ajax()) {

            $folio_holograma = Input::get('folio_holograma');
            $id_revista_vehiculo = Input::get('id_revista_vehiculo');
            $impresion_estatus = Input::get('impresion_estatus');
            $datos_dif = json_decode(Input::get('datos_dif'), true);
            $datos_particular = json_decode(Input::get('datos_particular'), true);
            $tenencias = json_decode(Input::get('tenencias'), true);
            $datos_cv = Input::get('datos_cv');
            $qr_dif = Input::get('qr_dif');
            $placa = Input::get('placa');

            $pdf = $this->PDF->pdf_constancia(
                "aprobado",
                $datos_dif["nombre"],
                $datos_dif["curp"],
                $datos_dif["oficio"],
                $datos_dif["fecha_sol"],
                $placa,
                $datos_cv,
                $datos_particular["vehiculo"][0]["modelo"],
                $datos_particular["vehiculo"][0]["serie_vehicular"],
                $datos_particular["vehiculo"][0]["numero_motor"],
                $tenencias,
                $qr_dif,
                $folio_holograma);

            $this->MaterialController->reducir_lote_modulo();

            if ($impresion_estatus == "SI") {
                Holograma::create([
                    'fecha_holograma' => date("Y-m-d H:i:s"),
                    'fecha_expedido' => date("Y-m-d H:i:s"),
                    'vigencia_documento' => date("Y-m-d H:i:s", strtotime('+1 year')),
                    'fecha_baja' => "",
                    'modulo_id' => \Auth::user()->modulo_id,
                    'revista_vehiculo_id' => $id_revista_vehiculo,
                    'folio_holograma' => $folio_holograma,
                    'estatus_id' => 1,
                    'lote_material_id' => $this->MaterialController->get_lote_modulo(\Auth::user()->id_modulo)
                ]);


                return url("/" . $pdf);

            } else {
                Incidencia_lote::create([

                    'folio_material' => $folio_holograma,
                    'fecha' => date("Y-m-d H:i:s"),
                    'folio_orden_administrativo' => "",
                    'lote_material_id' => $this->MaterialController->get_lote_modulo(\Auth::user()->id_modulo),
                    'revista_vehiculo_id' => $id_revista_vehiculo,

                ]);
                return "incidencia";
            }
        }
    }

    public
    function getClaveVehicular()
    {
        if (Request::ajax()) {

            $clave = Input::get('clave_vehicular');
            $vehiculo = $this->ClaveVehicular->findBy('clave_vehicular', $clave);

            if ($vehiculo == false) {
                $vehiculo = $this->clave($clave);

                if ($vehiculo == 500) {
                    return false;
                }

                $marca = $vehiculo->descripcionMarca;
                $linea = $vehiculo->descripcionLinea;
                $version = $vehiculo->descripcionModelo;
                $digito = $vehiculo->claveDigito;

                if ($marca == 'null' && $linea == 'null' && $version == 'null' && $digito == 'null') {
                    return 0;
                } else {

                    $claveVeh = $this->ClaveVehicular->create(['clave_vehicular' => $clave, 'digito' => $digito, 'marca' => $marca, 'linea' => $linea, 'version' => $version]);

                    return 1;

                }


            } else {
                return $vehiculo;

            }

        }

    }

    function impresion()
    {
        if (Request::ajax()) {

            $id_revista_vehiculo = Input::get('id_revista_vehiculo');
            $revista = Revista::where('id_revista_vehiculo', $id_revista_vehiculo)->get()->first();
            $revista->impresion = $revista->impresion + 1;
            $revista->save();
            return $revista->impresion;
        }

    }

    function grafica_data()
    {

        if (Request::ajax()) {
            $fecha = Input::get('fecha');


            $permisos = Revista::whereDate('fecha_revista', $fecha)->get();
            return count($permisos);
        }
    }


}
