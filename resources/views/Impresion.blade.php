@extends('layouts.app')

@section('content')



    @if( isset($pdf_holograma))

        <iframe id="printf_holograma" name="printf_holograma" src="{{url('/'.$pdf_holograma)}}" onfocus="window.close()"
                hidden></iframe>
        <iframe id="printf_constancia" name="printf_constancia" src="" onfocus="window.close()" hidden></iframe>

        <div class="panel panel-flat">


            <div class="panel-heading text-left">
                <h2>
                    <b>Revista Placa Discapacidad</b><br>
                </h2>
                <label class="text-pink" style="font-size: 18px">Paso 5 - Impresión</label>
            </div>
            <div class="panel-body">
                <div class="col-md-12">

                    <div class="col-md-5 col-md-offset-1 ">

                        <div class="table-responsive">
                            <table class="table table-borderless" style="width: 100%">
                                <tbody style="font-size: 20px">

                                <tr>
                                    <td><b>Placa</b></td>
                                    <td>{{$placa}}</td>
                                </tr>
                                <tr>
                                    <td><b>Folio QR</b></td>
                                    <td>{{$qr_dif}}</td>
                                </tr>
                                <tr>
                                    <td><b>Fecha</b></td>
                                    <td>@php echo date('d-m-Y');@endphp</td>
                                </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>


                    <div class="col-md-3">


                        <div class="text-center" style="font-size: 22px">
                            <br>
                            <b>Imprimir Holograma</b>


                        </div>


                        <div class="text-center">
                            <br> <br>
                            <button id="btn_imprimir" onclick="imprimir_holograma()" style="font-size: 15px"
                                    type="button"
                                    class="btn btn-xlg bg-pink">
                                IMPRIMIR <i style="margin-left: 10px" class="icon icon-printer2"></i>
                            </button>
                            <br> <br>

                        </div>


                    </div>
                    <div class="col-md-3">
                        <div id="constancia">


                            <div class="text-center" style="font-size: 22px">
                                <br>
                                <b>Imprimir Constancia</b>


                            </div>


                            <div class="text-center">
                                <br> <br>
                                <button onclick="imprimir_constancia()" style="font-size: 15px"
                                        type="button"
                                        class="btn btn-xlg bg-primary-800">
                                    IMPRIMIR <i style="margin-left: 10px" class="icon icon-printer2"></i>
                                </button>
                                <br> <br>

                            </div>
                        </div>


                    </div>
                </div>
                <br>
                <br>
                <br>

            </div>
            <div class="panel-footer">
                <div class="text-center">
                    <a href="{{url('/')}}">
                        <button type="button" id="btn_salir" class="btn btn-xlg bg-danger-800"> SALIR <i
                                    class="icon icon-cross"></i></button>
                    </a>
                </div>
            </div>

        </div>

        <div id="modal_impresion" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">

                    <input type="hidden" name="datos_dif" value="{{json_encode($datos_dif,true)}}">
                    <input type="hidden" name="datos_particular" value="{{json_encode($datos_particular,true)}}">
                    <input type="hidden" name="tenencias" value="{{json_encode($tenencias,true)}}">
                    <input type="hidden" name="datos_cv" value="{{$datos_cv}}">

                    <input type="hidden" name="qr_dif" value="{{$qr_dif}}">
                    <input type="hidden" name="placa" value="{{$placa}}">
                    <input type="hidden" name="id_revista_vehiculo" value="{{$id_revista_vehiculo}}">

                    <div class="modal-header bg-pink text-center">

                        <h2 class="modal-title">Impresión</h2>
                    </div>

                    <div class="modal-body">
                        <h3>¿Se imprimió correctamente?</h3>

                        <div class="row col-md-12 text-center">
                            <div class="col-md-2 col-md-offset-2">
                                <span style="font-size: 35px">SI</span>
                            </div>
                            <div class="col-md-2">
                                <input type="radio" style="width: 3em;height: 3em;" name="imp_estatus" value="SI">
                            </div>
                            <div class="col-md-2">
                                <span style="font-size: 35px">NO</span>
                            </div>
                            <div class="col-md-2">
                                <input type="radio" style="width: 3em;height: 3em;" name="imp_estatus" value="NO">
                            </div>

                        </div>
                        <br>
                        <br>

                        <div class="row" hidden id="div_holograma">
                            <div class="col-md-12 ">
                                <h3>¿El holograma fue utilizado?</h3>
                                <div class="text-center ">
                                    <div class="col-md-2 col-md-offset-2">
                                        <span style="font-size: 35px">SI</span>
                                    </div>
                                    <div class="col-md-2">
                                        <input type="radio" style="width: 3em;height: 3em;" name="holograma_estatus"
                                               value="SI">
                                    </div>
                                    <div class="col-md-2">
                                        <span style="font-size: 35px">NO</span>
                                    </div>
                                    <div class="col-md-2">
                                        <input type="radio" style="width: 3em;height: 3em;" name="holograma_estatus"
                                               value="NO">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>

                        <div class="row" hidden id="div_folio">
                            <div class="col-md-12 ">
                                <h3>Ingresa el folio del holograma</h3>
                                <div class="col-md-8 col-md-offset-2">
                                    <input type="text" style="font-size: 35px" name="folio_holograma" maxlength="6"
                                           class="form-control text-center input-xlg numeros">
                                </div>
                            </div>
                        </div>

                        <br>
                        <br>

                        <div class="row" hidden id="div_reimpresion">
                            <div class="text-center">
                                <h3>Reimprimir el vale</h3>
                                <button onclick="reimprimir()" type="button" class="btn bg-teal">
                                    REIMPRIMIR
                                </button>
                            </div>
                        </div>

                    </div>


                    <div class="modal-footer">
                        <button type="button" id="btn_modal_aceptar" class="btn btn-xlg bg-pink"
                                onclick="validar_impresion()">Aceptar
                        </button>
                    </div>

                </div>
            </div>
        </div>
        <script type="text/javascript">
            $('div#constancia').hide();
            $('#btn_salir').hide();

            function reimprimir() {
                $('[name = imp_estatus]').prop('checked', false);
                $('[name = holograma_estatus]').prop('checked', false);
                $('[name = folio_holograma]').val("").prop("disabled", true);
                $('#div_folio').attr("hidden", true);
                $('#div_reimpresion').attr("hidden", true);
                $('#div_holograma').attr("hidden", true);
                $('[id= btn_modal_aceptar]').prop("disabled", true);
                window.frames["printf_holograma"].focus();
                window.frames["printf_holograma"].print();
            }

            function imprimir_constancia() {
                window.frames["printf_constancia"].focus();
                window.frames["printf_constancia"].print();
                $('#btn_salir').show();
                swal({
                    text: ":)",
                    title: "Revista Terminada",
                    type: "",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                }).then(function () {

                });
            }


            function imprimir_holograma() {

                window.frames["printf_holograma"].focus();
                window.frames["printf_holograma"].print();
                $('#modal_impresion').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('[name = imp_estatus]').prop('checked', false);
                $('[name = holograma_estatus]').prop('checked', false);
                $('[name = folio_holograma]').val("").prop("disabled", true);
                $('[id= btn_modal_aceptar]').prop("disabled", true);

            }

            function validar_impresion() {
                const impresion_estatus = $('[name = imp_estatus]:checked').val();
                const folio_holograma = $('[name = folio_holograma]').val();
                const id_revista_vehiculo = $('[name = id_revista_vehiculo]').val();
                const datos_dif = $('[name = datos_dif]').val();
                const datos_particular = $('[name = datos_particular]').val();
                const tenencias = $('[name = tenencias]').val();
                const qr_dif = $('[name = qr_dif]').val();
                const placa = $('[name = placa]').val();
                const datos_cv = $('[name = datos_cv]').val();

                if (folio_holograma === "") {
                    swal({
                        title: "Debes ingresar un folio.",
                        text: "",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#ff0005",
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        confirmButtonText: "Aceptar"
                    }).then(function () {
                        $('[name = folio_holograma]').focus();
                    });
                    return false;
                }
                else {
                    $.ajax({
                        url: " {{url('/API/Validar_Folio_Holograma/')}} ",
                        type: "POST",
                        data: {"folio_holograma": folio_holograma},
                        success: function (data) {
                            if (data === "folio_ocupado") {
                                swal({
                                    title: "El folio del holograma ya fue utilizado",
                                    text: "",
                                    type: "error",
                                    showCancelButton: false,
                                    confirmButtonColor: "#ff0005",
                                    allowOutsideClick: false,
                                    allowEscapeKey: false,
                                    confirmButtonText: "Aceptar"
                                }).then(function () {
                                    $('[name = folio_holograma]').focus();
                                });
                            }
                            else if (data === "no_hay_lote") {
                                swal({
                                    title: "El folio del holograma no existe en el lote actual",
                                    text: "",
                                    type: "error",
                                    showCancelButton: false,
                                    confirmButtonColor: "#ff0005",
                                    allowOutsideClick: false,
                                    allowEscapeKey: false,
                                    confirmButtonText: "Aceptar"
                                }).then(function () {
                                    $('[name = folio_holograma]').focus();
                                });
                            }
                            else if (data === "folio_disponible") {


                                $.ajax({
                                    url: " {{url('/API/Holograma/')}} ",
                                    type: "POST",
                                    data: {
                                        "folio_holograma": folio_holograma,
                                        "id_revista_vehiculo": id_revista_vehiculo,
                                        "impresion_estatus": impresion_estatus,
                                        "datos_dif": datos_dif,
                                        "datos_particular": datos_particular,
                                        "qr_dif": qr_dif,
                                        "placa": placa,
                                        "datos_cv": datos_cv,
                                        "tenencias": tenencias
                                    },
                                    success: function (data) {
                                        if (data === "incidencia") {

                                            swal({
                                                title: "Se imprimirá de nuevo el holograma",
                                                text: "",
                                                type: "info",
                                                showCancelButton: false,
                                                confirmButtonColor: "#ff0005",
                                                allowOutsideClick: false,
                                                allowEscapeKey: false,
                                                confirmButtonText: "Aceptar"
                                            }).then(function () {
                                                reimprimir();
                                            });

                                        }
                                        else {
                                            $('#modal_impresion').modal('toggle');
                                            $('#printf_constancia').attr("src", data);


                                            $('[id= btn_imprimir]').prop("disabled", true);
                                            swal({
                                                title: "Imprime la Constancia ",
                                                text: "",
                                                type: "success",
                                                showCancelButton: false,
                                                confirmButtonColor: "#ff0005",
                                                allowOutsideClick: false,
                                                allowEscapeKey: false,
                                                confirmButtonText: "Aceptar"
                                            }).then(function () {
                                                $('div#constancia').show();
                                            });


                                        }
                                    }
                                });


                            }
                        }
                    });
                }
            }


            $('[name = holograma_estatus]').on('change', function (x) {
                $('[name = folio_holograma]').val("");

                if ($(this).val() === "NO") {

                    $('#div_folio').attr("hidden", true);
                    $('#div_reimpresion').attr("hidden", false);
                    $('[id= btn_modal_aceptar]').prop("disabled", true);
                }
                else {

                    $('#div_reimpresion').attr("hidden", true);
                    $('#div_folio').attr("hidden", false);
                    $('[id= btn_modal_aceptar]').prop("disabled", false);
                    $('[name = folio_holograma]').val('').prop("disabled", false).focus();
                }


            });


            $('[name = imp_estatus]').on('change', function (x) {

                $('[name = folio_holograma]').val("");
                $('[name = holograma_estatus]').prop('checked', false);
                $('#div_reimpresion').attr("hidden", true);

                if ($(this).val() === "SI") {
                    $('#div_holograma').attr("hidden", true);
                    $('#div_folio').attr("hidden", false);

                    $('[id= btn_modal_aceptar]').prop("disabled", false);
                    $('[name = folio_holograma]').val('').prop("disabled", false).focus();
                } else {
                    $('[id= btn_modal_aceptar]').prop("disabled", true);
                    $('#div_holograma').attr("hidden", false);
                    $('#div_folio').attr("hidden", true);
                }

            });


            function repeatString(n, string) {
                var repeat = [];
                repeat.length = n + 1;
                return repeat.join(string);
            }

            $('[name = folio_holograma]').on('change', function (x) {
                var folio = $(this).val().toString();
                var tam_folio = $(this).val().length;
                var dif = 6 - tam_folio;
                var zeros = repeatString(dif, '0');
                $(this).val(zeros + folio)
                $(this).focus();
            });


        </script>

    @else


        @if($errors->any())

            @foreach($errors->all() as $error)
                <script>
                    swal({
                        title: "{{$errors->all()[1]}}",
                        text: "{{$errors->all()[2]}}",
                        type: "{{$errors->all()[0]}}",
                        showCancelButton: false,
                        confirmButtonColor: "#ff0005",
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        confirmButtonText: "Aceptar"
                    }).then(function () {

                        $('[ name = folio_qr]').focus();
                    });
                </script>
            @endforeach

        @endif
        <div class="panel panel-flat">
            <div class="panel-heading text-left">
                <h2>
                    <b>Impresión de Holograma</b>
                </h2>
                <label style="font-size: 18px" class="text-pink">Ingresa la cortesia Qr y la placa ocupada en la
                    revista</label><br>
                <label style="font-size: 18px"></label>

            </div>
            <div class="panel-body">
                <form action="{{url('/Impresión/Holograma')}}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="row ">
                        <div class="col-md-12">

                            <div class="col-md-4 col-md-offset-2 text-center">

                                <h2>Inserte cortesía Qr <br>
                                    <small></small>
                                </h2>
                                <input type="text" class="form-control text-center folio_text input-xlg"
                                       maxlength="16"
                                       name="folio_qr" autofocus required>

                            </div>
                            <div class="col-md-4 text-center">

                                <h2>Inserte placa <br>
                                    <small></small>
                                </h2>
                                <input type="text" class="form-control text-center placa_text input-xlg"
                                       maxlength="5"
                                       name="placa" required>

                            </div>

                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="row">
                        <div class="text-center">
                            <button class="btn bg-pink btn-xlg">
                                BUSCAR <i style="margin-left: 10px" class="icon icon-search4 "></i>
                            </button>
                        </div>
                    </div>


                </form>

            </div>


        </div>



    @endif

@endsection
