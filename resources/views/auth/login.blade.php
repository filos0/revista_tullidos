<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Inicio de sesión</title>

    <!-- Global stylesheets -->
    <link href="{{url('/assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('/assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('/assets/css/core.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('/assets/css/components.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('/assets/css/colors.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('/assets/css/sweetalert2.css')}}" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="{{url('/assets/js/plugins/loaders/pace.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/core/libraries/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/core/libraries/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/plugins/loaders/blockui.min.js')}}"></script>


    <script type="text/javascript" src="{{url('/assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/plugins/ui/moment/moment.min.js')}}"></script>


    <script type="text/javascript" src="{{url('/assets/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/sweetalert2.js')}}"></script>



    <!-- /theme JS files -->

</head>


<body class="login-container login-cover">


<div class="page-container">


    <div class="page-content">

        <div class="content-wrapper">

            <div class="content pb-20">


                @if($errors->any())

                    @foreach($errors->all() as $error)
                        <script>
                            swal({
                                title: "{{$error}}",
                                text: "",
                                showCancelButton: false,
                                confirmButtonColor: "#ff0005",
                                allowOutsideClick: false,
                                allowEscapeKey: false,
                                confirmButtonText: "Aceptar"
                            }).then(function () {

                                $('[ name = email]').focus();
                            });
                        </script>
                    @endforeach

                @endif

                <form class="form-horizontal" id="login" role="form" method="POST" action="{{ url('/login') }}">
                    {{ csrf_field() }}

                    <div class="panel panel-body login-form">
                        <div class="text-center">

                            <h5 class="content-group" style="color: deeppink"><img
                                        src="{{asset('/assets/img/logo_semovi.png')}}"
                                        alt="SEMOVI" width="250px"><br><br>Revista Discapacitados</h5>
                        </div>
                        <br>
                        <div class="form-group has-feedback has-feedback-left ">
                            <input  type="text" class="form-control alfanumerico" name="rfc"
                                   placeholder="RFC"
                                   autofocus autocomplete="false" maxlength="13">


                            <div class="form-control-feedback">
                                <i class="icon-user text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group has-feedback has-feedback-left ">
                            <input id="password" type="password" class="form-control" name="password"
                                   placeholder="Contraseña">


                            <div class="form-control-feedback">
                                <i class="icon-lock2 text-muted"></i>
                            </div>
                        </div>


                        <div class="form-group">
                            <br>

                            <button type="submit" class="btn bg-pink-400 btn-block">Entrar <i
                                        class="icon-circle-right2 position-right"></i></button>
                        </div>

                    </div>
                </form>


            </div>


        </div>


    </div>

</div>

<script type="text/javascript">
    $('.alfanumerico').keyup(function () {

        cadena = $(this).val().toString().toUpperCase();
        cadena = cadena.match(/[0-9a-zA-ZñÑ@\s.,-]+/);
        $(this).val(cadena);


    });
</script>
</body>


</html>