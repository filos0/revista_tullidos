<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>EcoParq</title>
    <link rel="shortcut icon" type='image/x-icon' href="{{asset('/assets/img/logo.ico') }}"/>

    <link href="{{asset('/assets/remodal.css') }}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/remodal-default-theme.css') }}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/core.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/components.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/colors.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/sweetalert2.css')}}" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="{{asset('/assets/js/plugins/loaders/pace.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core/libraries/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core/libraries/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/loaders/blockui.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/remodal.js') }}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('/assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/ui/moment/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/sweetalert2.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/pages/datatables_responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('/assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/media/fancybox.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/pages/components_thumbnails.js')}}"></script>

</head>
<body>
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand">Secretaría de Movilidad</a>

        <ul class="nav navbar-nav pull-right visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
        </ul>
    </div>

</div>
<div class="page-container">

    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Content area -->
                <div class="content">

                    <!-- Error title -->
                    <div class=" content-group">
                       <div class="row">
                           @if($errors->any())

                               @foreach($errors->all() as $error)
                                   <script>
                                       swal({
                                           title: "{{$error}}",
                                           text: "",
                                           type: "",
                                           showCancelButton: false,
                                           confirmButtonColor: "#ff0005",
                                           allowOutsideClick: false,
                                           allowEscapeKey: false,
                                           confirmButtonText: "Aceptar"
                                       }).then(function () {

                                           $('[ name = password]').val('');
                                           $('[ name = email]').val('').focus();
                                       });
                                   </script>
                               @endforeach

                           @endif
                           <div class="text-center">
                                   <h2><b class="text-pink">CAMBIAR CONTRASEÑA</b></h2><br>

                               <br>
                               <img width="100px" height="100px" src="{{url('assets/img/lock.png')}}" alt="">
                               <br>
                               <br>
                               <br>
                               <h5>Estimado <b>{{$usuario['name']}}</b>, debe cambiar su contraseña para continuar. (8 Caracteres mínimo)</h5>
                               <h5> Recuerde que su usuario es <b>personal</b> e <b>intransferible</b></h5>
                               <br>
                               <br>
                           </div>
                       </div>

                        <form class="form-horizontal" id="nueva_pass" method="POST" action="{{ url('/Cambiar/Constraseña') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="usuario" value="{{$usuario}}">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-4">


                                    <div class="form-group ">
                                        <label class="col-md-2 control-label"><b>RFC de Usuario</b></label>

                                        <div class="col-md-4">
                                            <input id="name" type="text"
                                                   class="form-control text-uppercase text-center"
                                                   name="name"
                                                   value="{{ $usuario['rfc'] }}" readonly>

                                        </div>

                                    </div>
                                    <div class="form-group ">
                                        <label class="col-md-2 control-label"><b>Nueva Contraseña</b></label>

                                        <div class="col-md-4">
                                            <input type="password" maxlength="12"
                                                   class="form-control text-uppercase text-center pass_formato"
                                                   name="password" required
                                                   autofocus>

                                        </div>

                                    </div>


                                    <div class="form-group">
                                        <label class="col-md-2 control-label"><b>Confirmar Contraseña</b></label>

                                        <div class="col-md-4">
                                            <input type="password"
                                                   class="form-control text-uppercase text-center pass_formato"
                                                   name="password_confirm" required maxlength="12">


                                        </div>
                                    </div>


                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="row">
                                <div class=" col-md-8 col-md-offset-2">
                                    <div class="col-md-1 col-md-offset-1">
                                        <div class="checkbox form-group">
                                            <br>
                                            <input type="checkbox" class="control-primary" required name="terminos"
                                                   style="transform: scale(3,3)">
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <label style="font-size: 14px;text-align: justify">

                                            <b> Reconozco que es personal e intransferible y me responsabilizo de
                                                realizar
                                                el uso
                                                correcto y las funciones para las que fueron hechas y autorizadas, en el
                                                cargo que
                                                desempeño y de no ser así me someto a las Leyes de la Ciudad de México
                                                con
                                                fundamento en el artículo 47 de la Ley Federal de Responsabilidades de
                                                los
                                                Servidores Públicos, fracciones III y IV, así como en el artículo 259
                                                fracción
                                                III del Código Penal del Distrito Federal.</b>
                                        </label>
                                    </div>


                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="row">
                                <div class="text-center">
                                    <button id="btn_sbm" type="button" class="btn bg-pink btn-lg">
                                        <i class="fa fa-btn fa-refresh"></i> Actualizar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /error title -->


                <!-- Error content -->

                <!-- /error wrapper -->


                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>

</div>

</body>
</html>

<script type="text/javascript">

    $('.pass_formato').keyup(function () {
        cadena = $(this).val();
        cadena = cadena.toString();
        cadena = cadena.match(/[0-9a-zA-Z]+/);
        //alert(cadena);
        $(this).val(cadena);
    });

    $('#btn_sbm').on('click', function (e) {

        let pass_1 = $("[name = password]").val();
        let pass_2 = $("[name = password_confirm]").val();
        let terminos = $("[name = terminos]").is(':checked');

        if (pass_1 === "" || pass_2 === "") {
            swal({
                title: "Debes de ingresar los dos campos",
                text: "",
                type: "error",
                showCancelButton: false,
                confirmButtonColor: "#FF0000",
                confirmButtonText: "Aceptar.",
                allowOutsideClick: false,
                allowEscapeKey: false

            }).then(function () {

                $('[name = password]').focus();
            });
        }
        else {
            if (pass_1 === pass_2) {

                if (terminos)
                    $('#nueva_pass').submit();
                else {
                    swal({
                        title: "Debe aceptar la responsiva",
                        text: "",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#FF0000",
                        confirmButtonText: "Aceptar.",
                        allowOutsideClick: false,
                        allowEscapeKey: false

                    }).then(function () {

                        $('[name = password_confirm]').val('');
                        $('[name = password]').val('').focus();

                    });
                }

            }


            else {

                swal({
                    title: "Las Contraseñas no son iguales",
                    text: "",
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#FF0000",
                    confirmButtonText: "Aceptar.",
                    allowOutsideClick: false,
                    allowEscapeKey: false

                }).then(function () {

                    $('[name = password_confirm]').val('');
                    $('[name = password]').val('').focus();

                });
            }
        }
    });

    $('.pass_formato').on('change', function (e) {
        valor = $(this).val().length;
        if (valor < 8) {
            swal({
                title: "La Contraseña debe de tener por lo menos 8 caracteres entre Numeros y Letras",
                text: "",
                type: "error",
                showCancelButton: false,
                confirmButtonColor: "#FF0000",
                confirmButtonText: "Aceptar",
                allowOutsideClick: false,
                allowEscapeKey: false

            }).then(function () {
                $(this).val('').focus();
            });
        }
    });
</script>