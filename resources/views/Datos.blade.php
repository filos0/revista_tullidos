@extends('layouts.app')

@section('content')



    @if($errors->any())
        @foreach($errors->all() as $error)
            <script>
                swal({
                    title: "{{$errors->all()[0]}}",
                    text: "",
                    type: "{{$errors->all()[1]}}",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                }).then(function () {

                    $('[ name = parentesco]').focus();
                });
            </script>
        @endforeach
    @endif


    <div class="panel panel-flat">
        <form action="{{url('/Revista/Documentacion')}}" id="validacion" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="placa" value="{{$placa}}">
            <input type="hidden" name="qr_dif" value="{{$qr_dif}}">
            <input type="hidden" name="datos_dif" value="{{json_encode($datos_dif,true)}}">
            <input type="hidden" name="datos_particular" value="{{json_encode($datos_particular, true)}}">
            <input type="hidden" name="cv" value="{{json_encode($cv, true)}}">
            <div class="panel-heading text-left">
                <h2>
                    <b>Revista Placa Discapacidad</b>
                </h2>
                <label class="text-pink" style="font-size: 18px">Paso 3 - Comprobación de Datos</label><br>
            </div>
            <div class="panel-body">
                <div class="col-md-12">


                    <div class="col-md-6">
                        <div class="panel panel-flat">
                            <div class="panel-heading text-center">
                                <h2>Datos Control Vehicular</h2>
                            </div>
                            <div class="panel-body">
                                <div class="inline fields">
                                    <div class="field">
                                        <label style="font-weight: bold">PROPIETARIO</label>
                                        <input disabled type="text" class="form-control"
                                               value="{{$datos_particular->propietario[0]->nombre_razon_social .' '.$datos_particular->propietario[0]->primer_apellido. ' ' .$datos_particular->propietario[0]->segundo_apellido}}">
                                    </div>
                                </div>
                                <div class="inline fields">
                                    <div class="field">
                                        <label style="font-weight: bold">CURP</label>
                                        <input disabled type="text" class="form-control" value="{{$datos_particular->propietario[0]->clave_unica}}">
                                    </div>
                                </div>


                                <div class="inline fields">
                                    <div class="field">
                                        <label style="font-weight: bold">PLACA</label>
                                        <input disabled type="text" class="form-control" value="{{$datos_particular->tramite[0]->placa}}">
                                    </div>
                                </div>

                                <div class="inline fields">
                                    <div class="field">
                                        <label style="font-weight: bold">MARCA</label>
                                        <input disabled type="text" class="form-control" value="{{$cv["marca"]}}">
                                    </div>
                                </div>
                                <div class="inline fields">
                                    <div class="field">
                                        <label style="font-weight: bold">LÍNEA</label>
                                        <input disabled type="text" class="form-control" value="{{$cv["linea"]}}">
                                    </div>
                                </div><div class="inline fields">
                                    <div class="field">
                                        <label style="font-weight: bold">Versión</label>
                                        <input disabled type="text" class="form-control" value="{{$cv["version"]}}">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-flat">
                            <div class="panel-heading text-center">
                                <h2> Datos DIF</h2>
                            </div>
                            <div class="panel-body">

                                <div class="inline fields">
                                    <div class="field">
                                        <label style="font-weight: bold">PROPIETARIO</label>
                                        <input disabled type="text" class="form-control"
                                               value="{{$datos_dif["nombre"]}}">
                                    </div>
                                </div>
                                <div class="inline fields">
                                    <div class="field">
                                        <label style="font-weight: bold">CURP</label>
                                        <input disabled type="text" class="form-control" value="{{$datos_dif["curp"]}}">
                                    </div>
                                </div>
                                <div class="inline fields">
                                    <div class="field">
                                        <label style="font-weight: bold">NO. DE DICTAMEN O CORTESIA URBANA</label>
                                        <input disabled type="text" class="form-control"
                                               value="{{$datos_dif["oficio"]}}">
                                    </div>
                                </div>


                                <div class="inline fields">
                                    <div class="field">
                                        <label style="font-weight: bold">FOLIO DIF</label>
                                        <input disabled type="text" class="form-control" value="{{$qr_dif}}">
                                    </div>
                                </div>
                                <div class="inline fields">
                                    <div class="field">
                                        <label style="font-weight: bold">FECHA SOLICITUD</label>
                                        <input disabled type="text" class="form-control" value="{{$datos_dif["fecha_sol"]}}">
                                    </div>
                                </div>
                                <div class="inline fields">
                                    <div class="field">
                                        <label style="font-weight: bold">FECHA VIGENCIA</label>
                                        <input disabled type="text" class="form-control" value="{{$datos_dif["fecha_vigencia"]}}">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                </div>
                <div class="row">

                    <div class="col-md-12">

                        <div class="col-md-6 col-md-offset-3">
                            <div class="text-center">
                                <h3><b>Selecciona el tipo de parentesco.</b></h3>
                                <br>
                            </div>
                            <div class="input-group">
                                <select name="parentesco" autofocus required class="form-control input-xlg" style="font-size: 15px">
                                    <option class="form-control" value=""></option>
                                    <option class="form-control" value="Conyuge">Conyuge</option>
                                    <option class="form-control" value="Hijo/Hija">Hijo/Hija</option>
                                    <option class="form-control" value="Padre/Madre">Padre/Madre</option>
                                    <option class="form-control" value="Tutor">Tutor</option>
                                    <option class="form-control" value="Misma Persona">Misma Persona</option>
                                    <option class="form-control" value="No acredito parentesco">No acredito parentesco
                                    </option>
                                </select>
                                <span class="input-group-btn">
                                  <button type="submit" class="btn btn-xlg bg-teal"><b>Siguiente</b></button>
                            </span>
                            </div>
                            <br>
                        </div>

                    </div>
                </div>
            </div>

        </form>
    </div>
    <script>

    </script>


@endsection
