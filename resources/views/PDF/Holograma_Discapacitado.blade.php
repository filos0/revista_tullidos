<html>
<head>
    <style type="text/css">
        html {
            margin: 0;
            padding: 0;
            font-size: .27cm;
        }
        label{
            font-weight: bold;
        }

    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

</head>
<body>
<div style="width: 27.8cm;height:2.4cm;">

</div>

<div style="width: 10.6cm;height:10.9cm; float:left;border: 0px solid black">

    <div style="width: 10.1cm;height: 2cm;border: 0px black solid;border-radius: 10px;margin-left: .2cm;margin-top: .3cm">
        <label style="margin-left: 10px;color: white" for="">PROPIETARIO:</label>
        <br>
        <br>
        <label style="margin-left: 20px" for="">{{$nombre}}</label>
    </div>
    <div style="width: 10.1cm;height: 1.1cm;border: 0px black solid;border-radius: 10px;margin-left: .2cm;">
        <label style="margin-left: 10px;color: white" for="">MARCA:</label>
        <label style="margin-left: 10px" for="">{{$marca}}</label>

    </div>

    <table style="width: 10.1cm;">
        <tr>
            <td>
                <div style="width: 3.5cm;height: 1cm;border: 0px black solid;border-radius: 10px;margin-left: .3cm">
                    <label style="margin-left: 10px;color: white" for="">MODELO:</label>
                    <label style="margin-left: 10px" for="">{{$modelo}}</label>
                </div>

            </td>
            <td>
                <div style="width: 6.3cm;height: 1cm;border: 0px black solid;border-radius: 10px;margin-left: .3cm;">
                    <label style="margin-left: 10px;color: white" for="">TIPO:</label>
                    <label style="margin-left: 10px" for="">{{strtoupper($tipo)}}</label>
                </div>
            </td>
        </tr>
    </table>

    <div style="width: 10.1cm;height: 1.1cm;border: 0px black solid;border-radius: 10px;margin-left: .2cm;">
        <label style="margin-left: 10px;color: white" for="">PLACAS:</label>
        <label style="margin-left: 10px" for="">{{$placa}}</label>
    </div>
    <div style="width: 10.1cm;height: 1.1cm;border: 0px black solid;border-radius: 10px;margin-left: .2cm;">
        <label style="margin-left: 10px;color: white" for="">NO. DE MOTOR:</label>
        <label style="margin-left: 10px" for="">{{$no_motor}}</label>
    </div>
    <div style="width: 10.1cm;height: 1.1cm;border: 0px black solid;border-radius: 10px;margin-left: .2cm;">
        <label style="margin-left: 10px;color: white" for="">NO. DE DICTAMEN O CORTESIA URBANA:</label><br>
        <label style="margin-left: 20px" for="">{{$oficio}}</label>
    </div>


    <table style="width: 10.1cm;font-size: .25cm;">
        <tr>
            <td>
                <div style="width: 4.5cm;height: 1.6cm;border: 0px black solid;border-radius: 10px;margin-left: 3cm;margin-top: .3cm;font-size: .20cm">

                    <div>
                        <br>
                        <label style="margin-left:  .22cm" for="">{{$placa}}</label>


                        <br>
                        <br>

                        <label style="margin-left:  .22cm" for="">{{$oficio}}</label>
                    </div>
                </div>

            </td>
            <td>
                <div style="width: 2cm;height: 2cm;border: 0px black solid;margin-left: .7cm;margin-top: .2cm;">

                    <img style="width: 1.1cm;height: 1.1cm;opacity: .4;margin-top: .4cm;" src="" alt="">
                </div>
            </td>
        </tr>
    </table>


</div>
<div style="width: 10.6cm;height:10.9cm; float: right;border: 0px solid black">

    <div style="width: 10.1cm;height: 2cm;border: 0px black solid;border-radius: 10px;margin-left: .2cm;margin-top: .3cm">
        <label style="margin-left: 10px;color: white" for="">PROPIETARIO:</label>
        <br>
        <br>
        <label style="margin-left: 20px" for="">{{$nombre}}</label>
    </div>
    <div style="width: 10.1cm;height: 1.1cm;border: 0px black solid;border-radius: 10px;margin-left: .2cm;">
        <label style="margin-left: 10px;color: white" for="">MARCA:</label>
        <label style="margin-left: 10px" for="">{{$marca}}</label>

    </div>

    <table style="width: 10.1cm;">
        <tr>
            <td>
                <div style="width: 3.5cm;height: 1cm;border: 0px black solid;border-radius: 10px;margin-left: .3cm">
                    <label style="margin-left: 10px;color: white" for="">MODELO:</label>
                    <label style="margin-left: 10px" for="">{{$modelo}}</label>
                </div>

            </td>
            <td>
                <div style="width: 6.3cm;height: 1cm;border: 0px black solid;border-radius: 10px;margin-left: .3cm;">
                    <label style="margin-left: 10px;color: white" for="">TIPO:</label>
                    <label style="margin-left: 10px" for="">{{strtoupper($tipo)}}</label>
                </div>
            </td>
        </tr>
    </table>

    <div style="width: 10.1cm;height: 1.1cm;border: 0px black solid;border-radius: 10px;margin-left: .2cm;">
        <label style="margin-left: 10px;color: white" for="">PLACAS:</label>
        <label style="margin-left: 10px" for="">{{$placa}}</label>
    </div>
    <div style="width: 10.1cm;height: 1.1cm;border: 0px black solid;border-radius: 10px;margin-left: .2cm;">
        <label style="margin-left: 10px;color: white" for="">NO. DE MOTOR:</label>
        <label style="margin-left: 10px" for="">{{$no_motor}}</label>
    </div>
    <div style="width: 10.1cm;height: 1.1cm;border: 0px black solid;border-radius: 10px;margin-left: .2cm;">
        <label style="margin-left: 10px;color: white" for="">NO. DE DICTAMEN O CORTESIA URBANA:</label><br>
        <label style="margin-left: 20px" for="">{{$oficio}}</label>
    </div>


    <table style="width: 10.1cm;font-size: .25cm;">
        <tr>
            <td>
                <div style="width: 4.5cm;height: 1.6cm;border: 0px black solid;border-radius: 10px;margin-left: 3cm;margin-top: .3cm;font-size: .20cm">

                    <div>
                        <br>
                        <label style="margin-left:  .22cm" for="">{{$placa}}</label>


                        <br>
                        <br>

                        <label style="margin-left:  .22cm" for="">{{$oficio}}</label>
                    </div>
                </div>

            </td>
            <td>
                <div style="width: 2cm;height: 2cm;border: 0px black solid;margin-left: .7cm;margin-top: .2cm;">
                    <img style="width: 1.1cm;height: 1.1cm;opacity: .4;margin-top: .4cm;" src="" alt="">
                </div>
            </td>
        </tr>
    </table>


</div>





</body>
</html>