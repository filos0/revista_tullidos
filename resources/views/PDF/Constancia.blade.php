<html>
<head>
    <style type="text/css">
        html {
            margin: 0;
            padding: 0;
            font-family: 'Open Sans', sans-serif;
        }

        label {
            font-weight: bold;
        }

        .header {
        / / border: black 1 px solid;
            height: 100px;
            width: 100%;
        }

        table {
            border-collapse: collapse;
        }

        table, th, td {
        / / border: 1 px solid black;
        }

        .titulo {
            text-align: center;
        }

        .circulo_vacio {
            border-radius: 90%;
            border: 3px solid black;
            width: 20px;
            height: 20px;
            margin-left: 35%;
        }

        .circulo_lleno {

            border-radius: 90%;
            border: 3px solid black;
            background: black;
            width: 20px;
            height: 20px;
            margin-left: 35%;
        }

    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

</head>
<body>
<div class="header">


    <div style="width: 50%;display: inline-block;text-align: left">
        <img style="height: 75px;margin-top: 35px;margin-left: 30px;" src="{{asset('/assets/img/semovi')}}" alt="">
    </div>
    <div style="width: 50%;display: inline-block;text-align: right;">
        <img style="height: 50px;margin-top: 25px;margin-right: 30px;" src="{{asset('/assets/img/Logo-cdmx.png')}}"
             alt="">
    </div>

</div>
<div class="titulo">
    <h4>CONSTANCIA DE ASISTENCIA AL PROCESO DE REVISTA VEHÍCULAR {{date('Y')}}</h4>
</div>
<div style="margin-left: 35px;margin-right: 35px">
    <table style="border-spacing: 5px;width: 100%;font-size: 13px;border-collapse: separate;">

        <tr>
            <td style="width:25%"><b>DATOS DEL PROPIETARIO</b></td>
            <td style="width:25% "></td>
            <td style="width:25% "></td>
            <td style="width:25%;text-align: center "><b>FECHA DE IMPRESIÓN</b></td>
        </tr>

        <tr>
            <td style="width:25% "><b>NOMBRE:</b></td>
            <td style="width:25% ">{{$nombre}}</td>
            <td style="width:25% "></td>
            <td style="width:25% ;text-align: center">
                {{date('d-m-Y')}}
            </td>
        </tr>

        <tr>
            <td style="width:25% "><b>CURP/RFC:</b></td>
            <td style="width:25% ">{{$curp_rfc}}</td>
            <td style="width:25% "></td>
            <td style="width:25% "></td>
        </tr>

    </table>
    <table style="margin-left: 3px;width: 100%;font-size: 13px;border-collapse: separate;">

        <tr>
            <td style="width:25% "><b>N° DE OFICIO DIF:</b></td>
            <td style="width:75% ;text-align: left">{{$oficio_dif}}</td>
        </tr>
    </table>
    <table style="margin-left: 3px;width: 100%;font-size: 13px;border-collapse: separate;">

        <tr>
            <td style="width:25% "><b>FECHA EXPEDICIÓN</b></td>
            <td style="width:75% ;text-align: left">{{$fecha_exp}}</td>
        </tr>
    </table>
</div>
<br>
<div style="margin-left: 35px;margin-right: 35px">
    <b style="font-size: 13px;margin-left: 5px">DATOS DEL VEHÍCULO</b>
    <table style="border-spacing: 5px;width: 100%;font-size: 13px;border-collapse: separate;">

        <tr>
            <td style="width:25% "><b>PLACA:</b></td>
            <td style="width:25% ">{{$placa}}</td>
            <td style="width:25% "></td>
            <td style="width:25% "></td>
        </tr>

        <tr>
            <td style="width:25% "><b>MARCA:</b></td>
            <td style="width:25% ">{{$marca}}</td>
            <td style="width:25% "><b>MODELO:</b></td>
            <td style="width:25% ">{{$modelo}}</td>
        </tr>
        <tr>
            <td style="width:25% "><b>N° DE SERIE:</b></td>
            <td style="width:25% ">{{$num_serie}}</td>
            <td style="width:25% "><b>N° DE MOTOR:</b></td>
            <td style="width:25% ">{{$num_motor}}</td>
        </tr>
    </table>
    <table style="margin-left: 3px;width: 100%;font-size: 13px;border-collapse: separate;">

        <tr>
            <td style="width:25% "><b>TENENCIAS PAGADAS:</b></td>
            <td style="width:75% ">
                @if($tenencias[0]!=null)

                    @for($ano = date('Y'); $ano >= (date('Y')-5) ;$ano--  )

                        @if(in_array($ano,$tenencias))
                            <b>{{$ano}} ,</b>
                        @else
                            <b style="text-decoration: line-through">{{$ano}} ,</b>
                        @endif
                    @endfor
                @else
                    @for($ano = date('Y'); $ano >= (date('Y')-5) ;$ano--  )

                        <b style="text-decoration: line-through;">{{$ano}} ,</b>

                    @endfor
                @endif
            </td>

        </tr>
    </table>
</div>
<br>
<div style="margin-left: 15px;margin-right: 15px">
    <table style="border-spacing: 5px;width: 100%;font-size: 13px;border-collapse: separate;">

        <tr>
            <td style="width:10% ">
                @if($razon == "aprobado")
                    <div class="circulo_lleno"></div>
                @else
                    <div class="circulo_vacio"></div>
                @endif
            </td>
            <td style="width:60%;"><b>CUMPLIÓ SATISFATORIAMENTE CON LA REVISTA VEHICULAR</b></td>
            <td style="width:15%;"><b>FOLIO DEL HOLOGRAMA:</b></td>
            <td style="width:15%;">{{$folio_holograma}}</td>

        </tr>

    </table>
    <table style="border-spacing: 5px;width: 100%;font-size: 13px;border-collapse: separate;">


        <tr>
            <td style="width:10%;"></td>
            <td style="width:80%;text-align: center"><b>MOTIVOS DE RECHAZO:</b></td>

        </tr>
        <tr>
            <td style="width:10% ">
                @if($razon == "no_cortesia")
                    <div class="circulo_lleno"></div>
                @else
                    <div class="circulo_vacio"></div>
                @endif
            </td>
            <td style="width:80% ">

                @if($razon == "no_cortesia")
                    <b>SIN DATOS DE VALIDACIÓN DE LA CORTESÍA URBANA EMITIDA POR EL DIF-CDMX</b>
                    <br> <b>CORTESÍA : </b>" {{$qr}} "
                @else
                    <b>SIN DATOS DE VALIDACIÓN DE LA CORTESÍA URBANA EMITIDA POR EL DIF-CDMX</b>
                @endif


            </td>

        </tr>
        <tr>
            <td style="width:10% ">
                @if($razon == "no_placa")
                    <div class="circulo_lleno"></div>
                @else
                    <div class="circulo_vacio"></div>
                @endif
            </td>
            <td style="width:80% ">

                @if($razon == "no_placa")
                    <b>SIN DATOS DE VALIDACIÓN DE PLACAS</b>
                    <br> <b>Placa : </b>" {{$placa}} "
                @else
                    <b>SIN DATOS DE VALIDACIÓN DE PLACAS</b>
                @endif

            </td>

        </tr>
        <tr>
            <td style="width:10% ">
                @if($razon == "no_acredito_parentesco")
                    <div class="circulo_lleno"></div>
                @else
                    <div class="circulo_vacio"></div>
                @endif
            </td>
            <td style="width:80% "><b>NO ACREDITÓ PARENTESCO (PADRE, MADRE, HIJO, HIJA, CÓNYUGE, TUTOR O TUTORÍA LEGAL). </b>
            </td>

        </tr>

        <tr>
            <td style="width:10% ">
                @if($razon == "no_acredito_documentacion")
                    <div class="circulo_lleno"></div>
                @else
                    <div class="circulo_vacio"></div>
                @endif

            </td>
            <td style="width:80% "><b>NO PRESENTÓ DOCUMENTACIÓN DE ACREDITACIÓN DE PROPIEDAD DEL VEHÍCULO. </b></td>

        </tr>


        <tr>
            <td style="width:10% ">
                @if($razon == "cortesia_repetida")
                    <div class="circulo_lleno"></div>
                @elseif($razon == "placa_repetida")
                    <div class="circulo_lleno"></div>
                @else
                    <div class="circulo_vacio"></div>
                @endif
            </td>


            <td style="width:80%">
                @if($razon == "cortesia_repetida")
                    <b>OTRO: LA CORTESÍA URBANA YA FUE UTILIZADA ANTERIORMENTE :</b> "{{$qr}}"
                @elseif($razon == "placa_repetida")
                    <b>OTRO: LA PLACA YA FUE UTILIZADA ANTERIORMENTE :</b> "{{$placa}}"
                @else
                    <b>OTRO:_________________________________________________________________</b>
                @endif


            </td>

        </tr>
    </table>
</div>
<br>
<br>
<div style="margin-left: 75px;margin-right: 75px;font-size: 11px;text-align: justify">
    <small>“En cumplimiento a lo dispuesto en los artículos 114 Fracción IV, 115 Fracción I y 120 del Reglamento de la
        Ley de Movilidad del Distrito Federal y
        en lo publicado en la Gaceta Oficial de la Ciudad de México de fecha XXX de enero de 2018, tomo conocimiento de
        que el vehículo cuyos datos se
        describen, no cumplió con los requisitos del proceso de Revista Vehicular, por lo que manifiesto que estoy
        consciente que de no efectuar el trámite
        de baja de las placas anteriormente señaladas, podré ser sancionado (a) por las autoridades competentes.
    </small>
</div>
<br>

<div class="titulo">
    <h4><b>MANIFIESTO</b></h4>
</div>
<table style="border-spacing: 5px;width: 100%;font-size: 13px;border-collapse: separate;margin-left: 55px;margin-right: 55px">
    <tr>
        <td style="width:45%"><b>___________________________________________</b></td>
        <td style="width:10% "></td>
        <td style="width:45%"><b>___________________________________________</b></td>

    </tr>
    <tr>
        <td style="width:45%;text-align: center"><b>FIRMA DEL OPERADOR</b></td>
        <td style="width:10% "></td>
        <td style="width:45%;text-align: center"><b>FIRMA DEL SOLICITANTE</b></td>

    </tr>


</table>
<br>

<div style="margin-left: 75px;margin-right: 75px;font-size: 11px;text-align: justify">
    <small>De conformidad con el artículos 6 apartado A fracción II de la Constitución Política de los Estados Unidos
        Mexicanos, 2, 5 y 13 de la Ley de Protección de Datos
        Personales del Distrito Federal, los entes públicos deben de garantizar la confidencialidad e integridad de los
        datos personales que posean, con la finalidad de preservar
        el pleno ejercicio de los derechos tutelados, frente a su alteración, pérdida, transmisión y acceso no
        autorizado. Por lo que su indebida divulgación recaerá en violaciones
        a la Ley de Protección de Datos Personales para el Distrito Federal y se incurrirá en las conductas previstas
        como infracciones en su artículo 41 fracciones VII, XI y XIII.
    </small>
</div>
<br>

</body>
</html>