@extends('layouts.app')

@section('content')


    @if($errors->any())
        @foreach($errors->all() as $error)
            <script>
                swal({
                    title: "{{$errors->all()[0]}}",
                    text: "",
                    type: "{{$errors->all()[1]}}",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                }).then(function () {

                });
            </script>
        @endforeach
    @endif

    <div class="panel panel-flat">
        <form action="{{url('/Revista/Finalizar')}}" id="form_finalizar" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="placa" value="{{$placa}}">
            <input type="hidden" name="qr_dif" value="{{$qr_dif}}">
            <input type="hidden" name="cv" value="{{$cv}}">
            <input type="hidden" name="parentesco" value="{{$parentesco}}">
            <input type="hidden" name="datos_dif" value="{{$datos_dif}}">
            <input type="hidden" name="datos_particular" value="{{$datos_particular}}">
            <input type="hidden" name="tenencias" value="{{json_encode($tenencias, true)}}">
            <input type="hidden" name="resultado" value="">
            <div class="panel-heading text-left">
                <h2>
                    <b>Revista Placa Discapacidad</b><br>
                </h2>
                <label class="text-pink" style="font-size: 18px">Paso 4 - Comprobar Documentación</label>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <div class="col-md-12">
                        <br>

                        <div class="col-md-7 col-md-offset-3" style="font-size: 20px">
                            <div class="row">
                                <div class="col-md-7">
                                    <label for=""><b>Documento</b></label>
                                </div>
                                <div class="col-md-4 text-center">
                                    <label for=""><b></b></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <label for="">Credencial de Elector</label>
                                </div>
                                <div class="col-md-4 text-center">
                                    <input class="form-control doc_comprobante" type="checkbox"
                                           name="comprobante_credencial">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <label>Comprobante de Domicilio</label>
                                </div>
                                <div class="col-md-4 text-center">
                                    <input class="form-control doc_comprobante" type="checkbox"
                                           name="comprobante_domicilio">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <label>Factura del Vehículo</label>
                                </div>
                                <div class="col-md-4 text-center">
                                    <input class="form-control doc_comprobante" type="checkbox"
                                           name="comprobante_factura">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <label>Tenencias Pagadas</label><br>

                                    <label style="color: deeppink;font-size: 18px;">
                                        @if($tenencias[0]!=null)

                                            @for($ano = date('Y'); $ano >= (date('Y')-5) ;$ano--  )

                                                @if(in_array($ano,$tenencias))
                                                    <b>{{$ano}} ,</b>
                                                @else
                                                    <b style="text-decoration: line-through">{{$ano}} ,</b>
                                                @endif
                                            @endfor
                                        @else
                                            @for($ano = date('Y'); $ano >= (date('Y')-5) ;$ano--  )

                                                <b style="text-decoration: line-through;">{{$ano}} ,</b>

                                            @endfor
                                        @endif

                                    </label>


                                </div>
                                <div class="col-md-4 text-center">
                                    <input class="form-control doc_comprobante" type="checkbox"
                                           name="comprobante_tenencias" style="margin-top: 15px">
                                </div>
                            </div>

                        </div>


                    </div>
                </div>
                <div class="form-group">
                    <div class="row col-md-12">
                        <div class="form-group">
                            <br>
                            <div class="row text-center" style="font-size: 20px">

                                <label for=""><b>Comentarios</b></label>
                            </div>
                        </div>
                        <div class="col-md-8 col-md-offset-2">
                                    <textarea style="resize: none;height: 100px"
                                              class="form-control mayusculas" name="comentarios"
                                              placeholder="OBSERVACIONES DE REVISIÓN" maxlength="200">
                                    </textarea>

                        </div>

                    </div>

                    <div class="col-md-8 col-md-offset-2 text-grey text-left">
                        <span id="caracteres">200 Caracteres restantes</span>
                    </div>
                </div>
            </div>
        </form>
        <div class="form-group">
            <div class="row text-center">
                <button type="button" onclick="finalizar('reprobado')" id="btn_reprobado" name="que_paso"
                        value="no_aprobado"
                        class="btn btn-xlg bg-danger-800">No Aprobado<i
                            class="icon icon-cross"></i></button>
                <button type="button" disabled onclick="finalizar('aprobado')" id="btn_aprobado" name="que_paso"
                        value="aprobado"
                        class="btn btn-xlg bg-teal">Aprobado
                    <i class="icon icon-check"></i></button>
            </div>
        </div>
        </form>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $('[name = comentarios]').val('').focus();

        });

        $('.doc_comprobante').on('change', function (x) {
            var comprobante_credencial = $('[name = comprobante_credencial]').is(':checked');
            var comprobante_domicilio = $('[name =comprobante_domicilio ]').is(':checked');
            var comprobante_factura = $('[name =comprobante_factura ]').is(':checked');
            var comprobante_tenencias = $('[name =comprobante_tenencias ]').is(':checked');

            if (comprobante_credencial === true && comprobante_domicilio === true && comprobante_factura === true && comprobante_tenencias === true) {
                $('#btn_aprobado').attr("disabled", false);
                $('#btn_reprobado').attr("disabled", true);
            }

            else {
                $('#btn_aprobado').attr("disabled", true);
                $('#btn_reprobado').attr("disabled", false);
            }


        });
        $('[name = comentarios]').keyup(function () {
            var carac = $(this).val().length;
            var restantes = 200 - carac;
            $('#caracteres').text(restantes + " caracteres restantes.")

        });

        function finalizar(resultado) {
            $('[name = resultado]').val(resultado);
            $('#form_finalizar').submit();
        }
    </script>



@endsection
