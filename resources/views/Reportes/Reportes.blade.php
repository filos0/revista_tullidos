@extends('layouts.app')

@section('content')




    @if(isset($Revistas))


        <div class="panel panel-flat">
            <div class="panel-heading ">
                <h2><b>REPORTE DEL DÍA <span class="text-pink">{{$fecha_inicio}}</span> A <span
                                class="text-pink">{{$fecha_fin}}</span></b></h2>

                <h2><b>MÓDULO : <span class="text-pink">{{$modulo}}</span></b></h2>

                <div class="text-center">
                    <h2><b>TOTAL DE REVISTAS <span class="text-pink">{{count($Revistas)}}</span></b></h2>
                </div>
                <br>
            </div>

            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center text-bold">MÓDULO</th>
                            <th class="text-center text-bold">FECHA</th>
                            <th class="text-center text-bold">OPERADOR</th>
                            <th class="text-center text-bold">CONSTANCIA</th>
                            <th class="text-center text-bold">PLACA</th>
                            <th class="text-center text-bold">HOLOGRAMA</th>
                            <th class="text-center text-bold">OBSERVACIONES</th>


                        </tr>
                        </thead>
                        <tbody>
                        @foreach($Revistas as $revista)
                            @if($revista->holograma)
                                <tr>
                                    <td class="text-center">{{$revista->modulo->modulo}}</td>
                                    <td class="text-center">{{$revista->fecha_revista}}</td>
                                    <td class="text-center">{{$revista->usuario->rfc}}</td>

                                    <td class="text-center">{{$revista->constancia}}</td>
                                    <td class="text-center">{{$revista->placa}}</td>
                                    <td class="text-center">{{$revista->holograma->folio_holograma}}</td>
                                    <td>
                                        <ul>
                                            @php
                                                $observaciones = json_decode($revista->Observaciones,true);
                                            @endphp
                                            <li>Identificación : <span
                                                        class="text-pink">{{$observaciones[0]["comprobante_credencial"]}}</span>
                                            </li>
                                            <li>Domicilio : <span
                                                        class="text-pink">{{$observaciones[0]["comprobante_domicilio"]}}</span>
                                            </li>
                                            <li>Factura : <span
                                                        class="text-pink">{{$observaciones[0]["comprobante_factura"]}}</span>
                                            </li>
                                            <li>Parentesco : <span
                                                        class="text-pink">{{$observaciones[0]["parentesco"]}}</span>
                                            </li>

                                            <li>Tenencias :
                                                <span class="text-pink">
                                                    @if($observaciones[0]["tenencias"] =="SIN TENENCIAS PAGADAS")
                                                        SIN TENENCIAS PAGADAS
                                                    @else
                                                        @foreach($observaciones[0]["tenencias"] as $tenencia)
                                                            {{$tenencia}} ,
                                                        @endforeach
                                                    @endif


                                                </span>
                                            </li>
                                            <li>Comentarios : <span
                                                        class="text-pink">{{$observaciones[0]["comentarios"]}}</span>
                                            </li>
                                        </ul>

                                    </td>

                                </tr>


                            @else


                                <tr style="background-color: rgba(0,0,0,0.25)">
                                    <td class="text-center">{{$revista->modulo->modulo}}</td>
                                    <td class="text-center">{{$revista->fecha_revista}}</td>
                                    <td class="text-center">{{$revista->usuario->rfc}}</td>

                                    <td class="text-center">{{$revista->constancia}}</td>
                                    <td class="text-center">{{$revista->placa}}</td>
                                    <td class="text-center"><b> SIN HOLOGRAMA</b></td>
                                    <td>
                                        <ul>
                                            @php
                                                $observaciones = json_decode($revista->Observaciones,true);
                                            @endphp
                                            <li>Identificación : <span
                                                        class="text-pink">{{$observaciones[0]["comprobante_credencial"]}}</span>
                                            </li>
                                            <li>Domicilio : <span
                                                        class="text-pink">{{$observaciones[0]["comprobante_domicilio"]}}</span>
                                            </li>
                                            <li>Factura : <span
                                                        class="text-pink">{{$observaciones[0]["comprobante_factura"]}}</span>
                                            </li>
                                            <li>Parentesco : <span
                                                        class="text-pink">{{$observaciones[0]["parentesco"]}}</span>
                                            </li>

                                            <li>Tenencias :
                                                <span class="text-pink">

                                                           @if($observaciones[0]["tenencias"] =="SIN TENENCIAS PAGADAS")
                                                        SIN TENENCIAS PAGADAS
                                                    @else
                                                        @foreach($observaciones[0]["tenencias"] as $tenencia)
                                                            {{$tenencia}} ,
                                                        @endforeach
                                                    @endif

                                                </span>
                                            </li>
                                            <li>Comentarios : <span
                                                        class="text-pink">{{$observaciones[0]["comentarios"]}}</span>
                                            </li>
                                        </ul>

                                    </td>

                                </tr>

                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel-footer">
                <div class="text-center">
                    <a class="btn btn-xlg bg-pink" href="{{url('/Reportes')}}">
                        REGRESAR
                    </a>
                </div>
            </div>
        </div>








    @else
        <div class="panel panel-flat">
            <div class="panel-heading  text-pink">
                <h1><b>Selecciona los parametros para el reporte</b></h1>

            </div>

            <div class="panel-body">
                <form action="{{url('/Reportes/')}}" method="POST" id="form_reporte">
                    <div class="row">
                        <br>
                        <div class="col-md-10 col-md-offset-1 text-center">

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="col-md-4 col-md-offset-2">
                                <label style="font-size: 18px"><b>FECHA DE INICIO</b></label>
                                <input style="font-size: 18px" type="date" class="form-control text-center" required autofocus
                                       name="dia_inicio"  max="{{date('Y-m-d')}}" min="2018-03-01">
                            </div>

                            <div class="col-md-4">
                                <label style="font-size: 18px"><b>FECHA DE FIN</b></label>
                                <input style="font-size: 18px" type="date" class="form-control text-center" required
                                       name="dia_fin" value="{{date('Y-m-d')}}" max="{{date('Y-m-d')}}" min="2018-03-01">
                            </div>


                        </div>


                    </div>
                    <div class="row">
                        <br>
                        <br>
                        <div class="text-center">
                            <div class="col-md-2 col-md-offset-5">
                                <label style="font-size: 18px"><b>MÓDULO</b></label>
                                <select class="form-control" name="modulo" required>
                                    <option value=""></option>
                                    @foreach($modulos as $modulo)
                                        <option value="{{$modulo->id_modulo}}">{{$modulo->modulo}}</option>
                                    @endforeach
                                    <option value="TODOS">TODOS</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                </form>
            </div>
            <div class="panel-footer">
                <div class="text-center">
                    <button type="submit" form="form_reporte" class="btn bg-pink btn-xlg"> GENERAR</button>
                </div>
            </div>
        </div>
    @endif






@endsection
