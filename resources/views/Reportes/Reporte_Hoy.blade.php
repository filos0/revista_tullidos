@extends('layouts.app')

@section('content')


    <div class="panel panel-flat">
        <div class="panel-heading text-center text-pink">
            <h1><b>Revistas Realizadas Hoy [{{count($Revistas)}}]</b></h1>

        </div>

        <div class="panel-body">

            <div class="row">
                <h3 style="margin-left: 50px"><b>Revistas Terminadas Correctamente</b></h3>
            </div>
            <div class="row">
                @foreach($Revistas as $Revista)
                    @if($Revista->holograma)
                        <div class="col-md-4">
                            <div class="panel bg-teal">
                                <div class="panel-body">
                                    <div class="text-center">
                                        <label style="font-size: 20px" class=""><b>FOLIO DEL
                                                HOLOGRAMA: {{$Revista->holograma->folio_holograma}}</b></label>

                                    </div>
                                    <label style="font-size: 15px" class=""><b>PLACA:</b> {{$Revista->placa}}</label>

                                    <br>
                                    <label style="font-size: 15px" class=""><b>MÓDULO :</b> {{$Revista->modulo->modulo}}
                                    </label>
                                    <br>
                                    <label style="font-size: 15px" class=""><b>OPERADOR: </b> {{$Revista->usuario->rfc}}
                                    </label>
                                    <br>
                                    <label style="font-size: 15px" class=""><b>CONSTANCIA DIF
                                            :</b> {{$Revista->constancia}}
                                    </label>


                                </div>
                                <div class="panel-footer bg-teal-800 text-right">
                                    <div class="">
                                        <b>{{$Revista->fecha_revista}}</b>
                                    </div>
                                </div>
                            </div>

                        </div>
                    @endif
                @endforeach
            </div>
            <div class="row">
                <h3 style="margin-left: 50px"><b>Revistas Terminadas Sin Holograma</b></h3>
            </div>
            <div class="row">
                @foreach($Revistas as $Revista)
                    @if($Revista->holograma == null)
                        <div class="col-md-4">
                            <div class="panel bg-warning">
                                <div class="panel-body">
                                    <div class="text-center">
                                        <label style="font-size: 20px" class=""><b> FALTA IMPRIMIR HOLOGRAMA </b>
                                        </label>

                                    </div>
                                    <label style="font-size: 15px" class=""><b>PLACA:</b> {{$Revista->placa}}</label>

                                    <br>
                                    <label style="font-size: 15px" class=""><b>MÓDULO :</b> {{$Revista->modulo->modulo}}
                                    </label>
                                    <br>
                                    <label style="font-size: 15px" class=""><b>OPERADOR: </b> {{$Revista->usuario->rfc}}
                                    </label>
                                    <br>
                                    <label style="font-size: 15px" class=""><b>CONSTANCIA DIF
                                            :</b> {{$Revista->constancia}}
                                    </label>


                                </div>
                                <div class="panel-footer bg-warning-800 text-right">
                                    <div class="">
                                        <b>{{$Revista->fecha_revista}}</b>
                                    </div>
                                </div>
                            </div>

                        </div>
                    @endif
                @endforeach
            </div>

            <div class="row">
                <h3 style="margin-left: 50px"><b>Hologramas Mermados</b></h3>
            </div>
            <div class="row">
                @foreach($Incidencias as $Incidencia)

                    <div class="col-md-4">
                        <div class="panel bg-danger">
                            <div class="panel-body">
                                <div class="text-center">
                                    <label style="font-size: 20px" class=""><b>FOLIO DEL
                                            HOLOGRAMA: {{$Incidencia->folio_material}}</b> </label>

                                </div>
                                <br>
                                <label style="font-size: 15px" class=""><b>OPERADOR
                                        : </b> {{$Incidencia->revista->usuario->rfc}} </label>

                                <br>
                                <label style="font-size: 15px" class=""><b>MÓDULO
                                        : </b>{{$Incidencia->revista->modulo->modulo}}
                                </label>


                            </div>
                            <div class="panel-footer bg-danger-800 text-right">
                                <div class="">
                                    <b>{{$Incidencia->fecha}}</b>
                                </div>
                            </div>
                        </div>

                    </div>

                @endforeach
            </div>

        </div>
    </div>









@endsection
