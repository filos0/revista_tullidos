<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Revista Discapacitados</title>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">


    <link href="{{ asset('/assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/colors.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/js/core/libraries/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css">


    <!-- Core JS files -->


    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/jquery-ui.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/oklahomajs/core.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/oklahomajs/moment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/bootstrap-datetimepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/datepicker-es.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/jquery.maskedinput.js') }}"></script>



    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('/assets/js/plugins/forms/wizards/stepy.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/jasny-bootstrap.min.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('/assets/js/plugins/forms/validation/validate.min.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('/assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/pages/wizard_stepy.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/pages/form_bootstrap_select.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('/assets/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>


    <script type="text/javascript" src="{{ asset('/assets/js/plugins/ui/ripple.min.js') }}"></script>

    <!-- /theme JS files -->


</head>
<body>
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" >Secretaría de Movilidad</a>

        <ul class="nav navbar-nav pull-right visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
        </ul>
    </div>

</div>
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">

                <!-- Error title -->
                <div class="text-center content-group">

                    <img width="300" height="300" src="{{url('assets/img/chrome.png')}}" alt="">
                    <br><h2 ><label for="">Navegador No Soportado</label></h2><br>
                    <h5>Esta aplicación trabaja mejor con <b>Google Chrome</b></h5>

                    <h5>Si no cuentas con el navegador lo puedes conseguir desde la <a target="_blank" href="https://www.google.com.mx/chrome/browser/desktop/">página oficial</a> o del siguiente botón  </h5><br>

                </div>
                <!-- /error title -->


                <!-- Error content -->
                <div class="row">
                    <div class="col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3">


                        <a class="btn bg-pink btn-block" href="#" target="_blank">Bajar Chrome</a>

                    </div>
                </div>
                <!-- /error wrapper -->



                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>

</body>
</html>

<script type="text/javascript">


        window.frames["printf"].focus();
        window.frames["printf"].print();
       //    setTimeout(function(){window.close();}, 1);

</script>