@extends('layouts.app')

@section('content')


    @if($resultado == 'Se APROBO la revista')

        <iframe id="printf_holograma" name="printf_holograma" src="{{url('/'.$pdf_holograma)}}" onfocus="window.close()" hidden></iframe>
        <iframe id="printf_constancia" name="printf_constancia" src="" onfocus="window.close()" hidden></iframe>

        <div class="panel panel-flat">


            <div class="panel-heading text-left">
                <h2>
                    <b>Revista Placa Discapacidad</b><br>
                </h2>
                <label class="text-pink" style="font-size: 18px">Paso 5 - Impresión</label>
            </div>
            <div class="panel-body">
                <div class="col-md-12">

                    <div class="col-md-5 col-md-offset-1 ">

                        <div class="table-responsive">
                            <table class="table table-borderless" style="width: 100%">
                                <tbody style="font-size: 20px">
                                <tr>
                                    <td><b>Resultado</b></td>

                                    <td style="color: #009688"><b>APROBADO :) </b></td>

                                </tr>
                                <tr>
                                    <td><b>Placa</b></td>
                                    <td>{{$placa}}</td>
                                </tr>
                                <tr>
                                    <td><b>Folio QR</b></td>
                                    <td>{{$qr_dif}}</td>
                                </tr>
                                <tr>
                                    <td><b>Fecha</b></td>
                                    <td>@php echo date('d-m-Y');@endphp</td>
                                </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>


                    <div class="col-md-3">


                        <div class="text-center" style="font-size: 22px">
                            <br>
                            <b>Imprimir Holograma</b>


                        </div>


                        <div class="text-center">
                            <br> <br>
                            <button id="btn_imprimir" onclick="imprimir_holograma()" style="font-size: 15px"
                                    type="button"
                                    class="btn btn-xlg bg-pink">
                                IMPRIMIR <i style="margin-left: 10px" class="icon icon-printer2"></i>
                            </button>
                            <br> <br>

                        </div>


                    </div>
                    <div class="col-md-3">
                        <div id="constancia">


                            <div class="text-center" style="font-size: 22px">
                                <br>
                                <b>Imprimir Constancia</b>


                            </div>


                            <div class="text-center">
                                <br> <br>
                                <button onclick="imprimir_constancia()" style="font-size: 15px"
                                        type="button"
                                        class="btn btn-xlg bg-primary-800">
                                    IMPRIMIR <i style="margin-left: 10px" class="icon icon-printer2"></i>
                                </button>
                                <br> <br>

                            </div>
                        </div>


                    </div>
                </div>
                <br>
                <br>
                <br>

            </div>
            <div class="panel-footer">
                <div class="text-center">
                    <a href="{{url('/')}}">
                        <button type="button" id="btn_salir" class="btn btn-xlg bg-danger-800"> SALIR <i
                                    class="icon icon-cross"></i></button>
                    </a>
                </div>
            </div>

        </div>

        <div id="modal_impresion" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">

                    <input type="hidden" name="datos_dif" value="{{$datos_dif}}">
                    <input type="hidden" name="datos_particular" value="{{$datos_particular}}">
                    <input type="hidden" name="tenencias" value="{{$tenencias}}">
                    <input type="hidden" name="datos_cv" value="{{$datos_cv}}">

                    <input type="hidden" name="qr_dif" value="{{$qr_dif}}">
                    <input type="hidden" name="placa" value="{{$placa}}">
                    <input type="hidden" name="id_revista_vehiculo" value="{{$id_revista_vehiculo}}">

                    <div class="modal-header bg-pink text-center">

                        <h2 class="modal-title">Impresión</h2>
                    </div>

                    <div class="modal-body">
                        <h3>¿Se imprimió correctamente?</h3>

                        <div class="row col-md-12 text-center">
                            <div class="col-md-2 col-md-offset-2">
                                <span style="font-size: 35px">SI</span>
                            </div>
                            <div class="col-md-2">
                                <input type="radio" style="width: 3em;height: 3em;" name="imp_estatus" value="SI">
                            </div>
                            <div class="col-md-2">
                                <span style="font-size: 35px">NO</span>
                            </div>
                            <div class="col-md-2">
                                <input type="radio" style="width: 3em;height: 3em;" name="imp_estatus" value="NO">
                            </div>

                        </div>
                        <br>
                        <br>

                        <div class="row" hidden id="div_holograma">
                            <div class="col-md-12 ">
                                <h3>¿El holograma fue utilizado?</h3>
                                <div class="text-center ">
                                    <div class="col-md-2 col-md-offset-2">
                                        <span style="font-size: 35px">SI</span>
                                    </div>
                                    <div class="col-md-2">
                                        <input type="radio" style="width: 3em;height: 3em;" name="holograma_estatus"
                                               value="SI">
                                    </div>
                                    <div class="col-md-2">
                                        <span style="font-size: 35px">NO</span>
                                    </div>
                                    <div class="col-md-2">
                                        <input type="radio" style="width: 3em;height: 3em;" name="holograma_estatus"
                                               value="NO">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>

                        <div class="row" hidden id="div_folio">
                            <div class="col-md-12 ">
                                <h3>Ingresa el folio del holograma</h3>
                                <div class="col-md-8 col-md-offset-2">
                                    <input type="text" style="font-size: 35px" name="folio_holograma" maxlength="6"
                                           class="form-control text-center input-xlg numeros">
                                </div>
                            </div>
                        </div>

                        <br>
                        <br>

                        <div class="row" hidden id="div_reimpresion">
                            <div class="text-center">
                                <h3>Reimprimir el vale</h3>
                                <button onclick="reimprimir()" type="button" class="btn bg-teal">
                                    REIMPRIMIR
                                </button>
                            </div>
                        </div>

                    </div>


                    <div class="modal-footer">
                        <button type="button" id="btn_modal_aceptar" class="btn btn-xlg bg-pink"
                                onclick="validar_impresion()">Aceptar
                        </button>
                    </div>

                </div>
            </div>
        </div>
        <script type="text/javascript">
            $('div#constancia').hide();
            $('#btn_salir').hide();

            function checar_impresion() {
                var id_revista_vehiculo = $('[name = id_revista_vehiculo]').val();
                $.ajax({
                    url: " {{url('/API/Impresion/')}} ",
                    type: "POST",
                    data: {
                        "id_revista_vehiculo": id_revista_vehiculo,
                    },
                });

            }
            function reimprimir() {
                checar_impresion()
                window.frames["printf_holograma"].focus();
                window.frames["printf_holograma"].print();

                $('[name = imp_estatus]').prop('checked', false);
                $('[name = holograma_estatus]').prop('checked', false);
                $('[name = folio_holograma]').val("").prop("disabled", true);
                $('#div_folio').attr("hidden", true);
                $('#div_reimpresion').attr("hidden", true);
                $('#div_holograma').attr("hidden", true);
                $('[id= btn_modal_aceptar]').prop("disabled", true);

            }

            function imprimir_constancia() {
                window.frames["printf_constancia"].focus();
                window.frames["printf_constancia"].print();
                $('#btn_salir').show();
                swal({
                    text: ":)",
                    title: "Revista Terminada",
                    type: "",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                }).then(function () {

                });
            }


            function imprimir_holograma() {
                checar_impresion()
                window.frames["printf_holograma"].focus();
                window.frames["printf_holograma"].print();


                $('#modal_impresion').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('[name = imp_estatus]').prop('checked', false);
                $('[name = holograma_estatus]').prop('checked', false);
                $('[name = folio_holograma]').val("").prop("disabled", true);
                $('[id= btn_modal_aceptar]').prop("disabled", true);

            }

            function validar_impresion() {
                var impresion_estatus = $('[name = imp_estatus]:checked').val();
                var folio_holograma = $('[name = folio_holograma]').val();
                var id_revista_vehiculo = $('[name = id_revista_vehiculo]').val();
                var datos_dif = $('[name = datos_dif]').val();
                var datos_particular = $('[name = datos_particular]').val();
                var tenencias = $('[name = tenencias]').val();
                var qr_dif = $('[name = qr_dif]').val();
                var placa = $('[name = placa]').val();
                var datos_cv = $('[name = datos_cv]').val();

                if (folio_holograma === "") {
                    swal({
                        title: "Debes ingresar un folio.",
                        text: "",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#ff0005",
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        confirmButtonText: "Aceptar"
                    }).then(function () {
                        $('[name = folio_holograma]').focus();
                    });
                    return false;
                }
                else {
                    $.ajax({
                        url: " {{url('/API/Validar_Folio_Holograma/')}} ",
                        type: "POST",
                        data: {"folio_holograma": folio_holograma},
                        success: function (data) {
                            if (data === "folio_ocupado") {
                                swal({
                                    title: "El folio del holograma ya fue utilizado",
                                    text: "",
                                    type: "error",
                                    showCancelButton: false,
                                    confirmButtonColor: "#ff0005",
                                    allowOutsideClick: false,
                                    allowEscapeKey: false,
                                    confirmButtonText: "Aceptar"
                                }).then(function () {
                                    $('[name = folio_holograma]').focus();
                                });
                            }
                            else if (data === "no_hay_lote") {
                                swal({
                                    title: "El folio del holograma no existe en el lote actual",
                                    text: "",
                                    type: "error",
                                    showCancelButton: false,
                                    confirmButtonColor: "#ff0005",
                                    allowOutsideClick: false,
                                    allowEscapeKey: false,
                                    confirmButtonText: "Aceptar"
                                }).then(function () {
                                    $('[name = folio_holograma]').focus();
                                });
                            }
                            else if (data === "folio_disponible") {


                                $.ajax({
                                    url: " {{url('/API/Holograma/')}} ",
                                    type: "POST",
                                    data: {
                                        "folio_holograma": folio_holograma,
                                        "id_revista_vehiculo": id_revista_vehiculo,
                                        "impresion_estatus": impresion_estatus,
                                        "datos_dif": datos_dif,
                                        "datos_particular": datos_particular,
                                        "qr_dif": qr_dif,
                                        "placa": placa,
                                        "datos_cv": datos_cv,
                                        "tenencias": tenencias
                                    },
                                    success: function (data) {
                                        if (data === "incidencia") {

                                            swal({
                                                title: "Se imprimirá de nuevo el holograma",
                                                text: "",
                                                type: "info",
                                                showCancelButton: false,
                                                confirmButtonColor: "#ff0005",
                                                allowOutsideClick: false,
                                                allowEscapeKey: false,
                                                confirmButtonText: "Aceptar"
                                            }).then(function () {
                                                reimprimir();
                                            });

                                        }
                                        else {
                                            $('#modal_impresion').modal('toggle');
                                            $('#printf_constancia').attr("src", data);


                                            $('[id= btn_imprimir]').prop("disabled", true);
                                            swal({
                                                title: "Imprime la Constancia ",
                                                text: "",
                                                type: "success",
                                                showCancelButton: false,
                                                confirmButtonColor: "#ff0005",
                                                allowOutsideClick: false,
                                                allowEscapeKey: false,
                                                confirmButtonText: "Aceptar"
                                            }).then(function () {
                                                $('div#constancia').show();
                                            });


                                        }
                                    }
                                });


                            }
                        }
                    });
                }
            }


            $('[name = holograma_estatus]').on('change', function (x) {
                $('[name = folio_holograma]').val("");

                if ($(this).val() === "NO") {

                    $('#div_folio').attr("hidden", true);
                    $('#div_reimpresion').attr("hidden", false);
                    $('[id= btn_modal_aceptar]').prop("disabled", true);
                }
                else {

                    $('#div_reimpresion').attr("hidden", true);
                    $('#div_folio').attr("hidden", false);
                    $('[id= btn_modal_aceptar]').prop("disabled", false);
                    $('[name = folio_holograma]').val('').prop("disabled", false).focus();
                }


            });


            $('[name = imp_estatus]').on('change', function (x) {

                $('[name = folio_holograma]').val("");
                $('[name = holograma_estatus]').prop('checked', false);
                $('#div_reimpresion').attr("hidden", true);

                if ($(this).val() === "SI") {
                    $('#div_holograma').attr("hidden", true);
                    $('#div_folio').attr("hidden", false);

                    $('[id= btn_modal_aceptar]').prop("disabled", false);
                    $('[name = folio_holograma]').val('').prop("disabled", false).focus();
                } else {
                    $('[id= btn_modal_aceptar]').prop("disabled", true);
                    $('#div_holograma').attr("hidden", false);
                    $('#div_folio').attr("hidden", true);
                }

            });


            function repeatString(n, string) {
                var repeat = [];
                repeat.length = n + 1;
                return repeat.join(string);
            }

            $('[name = folio_holograma]').on('change', function (x) {
                var folio = $(this).val().toString();
                var tam_folio = $(this).val().length;
                var dif = 6 - tam_folio;
                var zeros = repeatString(dif, '0');
                $(this).val(zeros + folio)
                $(this).focus();
            });


        </script>





    @else


        @if($errors->any())

            @foreach($errors->all() as $error)
                <script>
                    swal({
                        title: "{{$errors->all()[1]}}",
                        text: "{{$errors->all()[2]}}",
                        type: "{{$errors->all()[0]}}",
                        showCancelButton: false,
                        confirmButtonColor: "#ff0005",
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        confirmButtonText: "Aceptar"
                    }).then(function () {

                    });
                </script>
            @endforeach

        @endif

        <iframe id="printf" name="printf" src="{{url('/'.$pdf)}}" onfocus="window.close()" hidden></iframe>

        <div class="panel panel-flat">


            <div class="panel-heading text-left">
                <h2>
                    <b>Revista Placa Dispacidad</b><br>
                </h2>
                <label class="text-pink" style="font-size: 18px">Paso 5 - Impresión</label>
            </div>
            <div class="panel-body">
                <div class="col-md-12">

                    <div class="col-md-6 col-md-offset-1">

                        <div class="table-responsive">
                            <table class="table table-borderless" style="width: 100%">
                                <tbody style="font-size: 20px">
                                <tr>
                                    <td><b>Resultado</b></td>

                                    <td style="color: #FF1717"><b>NO APROBADO :(</b></td>

                                </tr>
                                <tr>
                                    <td><b>Placa</b></td>
                                    <td>{{$placa}}</td>
                                </tr>
                                <tr>
                                    <td><b>Folio QR</b></td>
                                    <td>{{$qr_dif}}</td>
                                </tr>
                                <tr>
                                    <td><b>Fecha</b></td>
                                    <td>@php echo date('d-m-Y');@endphp</td>
                                </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>

                    <div class="col-md-5">


                        <div class="text-center" style="font-size: 22px">
                            <br>
                            <b>Imprimir Constancia</b>


                        </div>


                        <div class="text-center">
                            <br> <br>
                            <button id="btn_imprimir" onclick="imprimir()" style="font-size: 15px" type="button"
                                    class="btn btn-xlg bg-teal">
                                IMPRIMIR <i style="margin-left: 10px" class="icon icon-printer2"></i>
                            </button>
                            <br> <br>
                            <button id="btn_salir" onclick="window.location.replace('{{url('/')}}')"
                                    style="font-size: 15px" type="button"
                                    class="btn btn-xlg bg-pink">
                                SALIR <i style="margin-left: 10px" class="icon icon-home"></i>
                            </button>
                        </div>


                    </div>
                </div>

            </div>


        </div>


        <script type="text/javascript">
            $('[id= btn_salir]').hide();

            function imprimir() {
                window.frames["printf"].focus();
                window.frames["printf"].print();
            }

        </script>

    @endif




@endsection
