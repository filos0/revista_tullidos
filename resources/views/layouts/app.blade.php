<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<title>Revista Discapacitados</title>
<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <meta name="csrf-token" content="{{ csrf_token() }}">


    <link href="{{ asset('/assets/fonts/font.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/sweetalert2.css') }}" rel="stylesheet" type="text/css">

    <link href="{{asset('/assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/core.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/components.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/colors.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/print.css') }}" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="{{asset('/assets/js/plugins/loaders/pace.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core/libraries/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core/libraries/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/loaders/blockui.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('/assets/js/plugins/ui/moment/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/forms/selects/select2.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('/assets/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/sweetalert2.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core_revista.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/echart.js')}}"></script>

    <script type="text/javascript" src="{{asset('/assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/pages/datatables_basic.js')}}"></script>


</head>

<body>


<div class="navbar navbar-default header-highlight">
    <div class="navbar-header">
        <a style="color: white" class="navbar-brand" href="index.html"><img src="" alt=""></a>

        <ul class="nav navbar-nav visible-xs-block">

            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li>

                <b><a id="hora" class="navbar-brand"></a></b>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-left">
            <li>

                <a class="navbar-brand" style="padding: 0px;">
                    <img style="  height: 100%;padding: 5px;width: auto;"
                         src="{{asset('/assets/img/logo_semovi.png')}}" alt="">
                </a>

            </li>
        </ul>


    </div>
</div>

<div class="page-container">


    <div class="page-content">


        <div class="sidebar sidebar-main">
            <div class="sidebar-content">


                <div class="sidebar-user" style="cursor: pointer">
                    <div class="category-content">
                        <div class="media">
                            <a href="#" class="media-left"><img
                                        src="{{asset('/assets/img/angel.jpeg')}}"
                                        class="img-circle img-lg" alt=""></a>
                            <div class="media-body">
                                <span class="media-heading">
                                    <b>{{Auth::user()->rfc}}</b>
                                    <br>
                                    [ {{Auth::user()->roles[0]["display_name"]}} ]
                                </span>
                                <div class="text-left text-size-mini text-muted ">
                                    MÓDULO : {{Auth::user()->modulo->modulo}}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">

                            <li><a href="{{url('/')}}"><i class="icon-home4"></i>
                                    <span>Inicio</span></a></li>

                            @role('almacen')

                            <li><a href="{{url('/Material')}}"><i class=" glyphicon glyphicon-barcode"></i>
                                    <span>Administrar Material</span></a></li>
                            @endrole
                            @role('administrador')

                            <li><a href="{{url('/Revista')}}"><i class="icon icon-accessibility2"></i>
                                    <span>Revista Placa Dispacidad</span></a></li>
                            <li><a href="{{url('/Impresión/Holograma')}}"><i class="icon icon-printer"></i>
                                    <span>Impresión Holograma </span></a></li>


                            <li class="navigation-header">
                                <span>ADMINISTRACIÓN</span>
                                <i class="icon-menu"></i>
                            </li>

                            <li><a href="{{url('/Usuarios')}}"><i class="icon icon-users4"></i>
                                    <span>Administrar Usuarios</span></a></li>

                            <li><a href="{{url('/Material')}}"><i class=" glyphicon glyphicon-barcode"></i>
                                    <span>Administrar Material</span></a></li>

                            <li><a href="{{url('/Tramite')}}"><i class="icon icon-search4"></i>
                                    <span>Buscar Trámite</span></a></li>


                            <li class="navigation-header">
                                <span>Reportes</span>
                                <i class="icon-menu"></i>
                            </li>
                            <li><a href="{{url('/Reportes')}}"><i class="icon icon-file-spreadsheet2"></i>
                                    <span>Reportes</span></a></li>
                            <li><a href="{{url('/Reportes/Hoy')}}"><i class="icon  icon-certificate"></i>
                                    <span>Reportes de Hoy</span></a></li>




                            @endrole
                            @role('operador')

                            <li><a href="{{url('/Revista')}}"><i class="icon icon-accessibility2"></i>
                                    <span>Revista Placa Dispacidad</span></a></li>

                            @endrole

                            @role('director')

                            <li><a href="{{url('/Reportes')}}"><i class="icon icon-file-spreadsheet2"></i>
                                    <span>Reportes</span></a></li>
                            <li><a href="{{url('/Reportes/Hoy')}}"><i class="icon  icon-certificate"></i>
                                    <span>Reportes de Hoy</span></a></li>


                            @endrole

                            @role('registro_usuarios')

                            <li><a href="{{url('/register')}}"><i class="icon icon-users4"></i>
                                    <span>Registrar Usuario</span></a></li>


                            @endrole


                            <li>
                                <a class="legitRipple" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="icon  icon-cross"></i>
                                    <span>Cerrar Sesión</span>
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>


            </div>
        </div>

        <div class="content-wrapper">


            <div class="content">


                @yield('content')

            </div>


        </div>


    </div>


</div>

<script type="text/javascript">


</script>
</body>
</html>
