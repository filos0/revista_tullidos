@extends('layouts.app')

@section('content')


    @if($errors->any())

        @foreach($errors->all() as $error)
            <script>
                swal({
                    title: "{{$errors->all()[1]}}",
                    text: "{{$errors->all()[2]}}",
                    type: "{{$errors->all()[0]}}",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                }).then(function () {

                    $('[ name = placa]').focus();
                });
            </script>
        @endforeach

    @endif
    <div class="panel panel-flat">
        <div class="panel-heading text-left">
            <h2>
                <b>Revista Placa Discapacidad</b>
            </h2>
            <label style="font-size: 18px" class="text-pink">Paso 2 - Validación Placa</label><br>
            <label style="font-size: 18px">La cortesía del DIF <b>{{$qr_dif}}</b> es correcta <i
                        style="color: green;font-size: 20px" class="icon icon-check"></i> </label>

        </div>
        <div class="panel-body">
            <form action="{{url('/Revista/Datos')}}" id="validacion" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="datos_dif" value="{{ $datos_dif }}">
                <input type="hidden" name="qr_dif" value="{{$qr_dif}}">

                <div class="row ">
                    <div class="col-md-12">

                        <div class="col-md-4 col-md-offset-4 text-center">

                            <h2>Inserte placa <br>
                                <small></small>
                            </h2>
                            <input type="text" class="form-control text-center placa_text input-xlg"
                                   maxlength="5"
                                   name="placa" autofocus required>

                        </div>

                    </div>
                </div>
                <br>
                <br>
                <div class="row">
                    <div class="text-center">
                        <button class="btn bg-pink btn-xlg">
                            Validar<i style="margin-left: 10px" class="icon icon-accessibility2 "></i>
                        </button>
                    </div>
                </div>


            </form>

        </div>


    </div>





@endsection
