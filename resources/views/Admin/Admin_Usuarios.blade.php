@extends('layouts.app')

@section('content')


    @if($errors->any())

        @foreach($errors->all() as $error)
            <script>
                swal({
                    title: "{{$errors->all()[1]}}",
                    text: "{{$errors->all()[2]}}",
                    type: "{{$errors->all()[0]}}",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                }).then(function () {

                });
            </script>
        @endforeach

    @endif
    <div class="panel panel-flat">
        <div class="panel-heading text-left">
        </div>
        <div class="panel-body">
            <div class="col-md-12">

                <div class="panel">
                    <div class="panel-heading bg-pink">
                        <h4>AGREGAR USUARIO</h4>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form action="{{url('register')}}" method="POST" id="form_register">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="row">
                                <div class="text-left">
                                    <br>
                                    <label style="font-size: 18px;">
                                        La contraseña por defecto sera <b>123456</b> y el usuario la debera cambiar
                                        una vez que inicie sesión
                                    </label>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-4">
                                    <label style="font-size: 15px" class=""><b>Módulo del Operador</b></label>
                                    <div class="input-group">
                                        <label class="input-group-btn">
                                            <button class="btn btn-default btn-icon" type="button" disabled>
                                                <i class=" icon-office"></i>
                                            </button>
                                        </label>
                                        <select required class="form-control" name="modulo" id="" autofocus>
                                            <option class="form-control" value=""></option>
                                            @foreach($Modulos as $modulo)
                                                <option class="form-control"
                                                        value="{{$modulo->id_modulo}}"> {{$modulo->modulo}} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <br>

                            <div class="row">
                                <div class="col-md-4">
                                    <label style="font-size: 15px" class=""><b>Nombre del Operador</b></label>
                                    <div class="input-group">
                                        <label class="input-group-btn">
                                            <button class="btn btn-default btn-icon" type="button" disabled>
                                                <i class="icon-user"></i>
                                            </button>
                                        </label>
                                        <input type="text" required name="nombre" maxlength="20"
                                               class="form-control mayusculas">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label style="font-size: 15px" class=""><b>Primer Apellido del Operador</b></label>
                                    <div class="input-group">
                                        <label class="input-group-btn">
                                            <button class="btn btn-default btn-icon" type="button" disabled>
                                                <i class="icon-user"></i>
                                            </button>
                                        </label>
                                        <input type="text" required name="primer_apellido" maxlength="20"
                                               class="form-control mayusculas">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label style="font-size: 15px" class=""><b>Segundo Apellido del Operador</b></label>
                                    <div class="input-group">
                                        <label class="input-group-btn">
                                            <button class="btn btn-default btn-icon" type="button" disabled>
                                                <i class="icon-user"></i>
                                            </button>
                                        </label>
                                        <input type="text" name="segundo_apellido" maxlength="20"
                                               class="form-control mayusculas">
                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class="row">
                                <div class="col-md-4">
                                    <label style="font-size: 15px" class=""><b>RFC del Operador</b></label>
                                    <div class="input-group">
                                        <label class="input-group-btn">
                                            <button class="btn btn-default btn-icon" type="button" disabled>
                                                <i class="icon-user"></i>
                                            </button>
                                        </label>
                                        <input type="text" required name="rfc" class="form-control placa_text"
                                               maxlength="13">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label style="font-size: 15px" class=""><b>Rol del Usuario</b></label>
                                    <div class="input-group">
                                        <label class="input-group-btn">
                                            <button class="btn btn-default btn-icon" type="button" disabled>
                                                <i class="  icon-eye"></i>
                                            </button>
                                        </label>
                                        <select required class="form-control" name="rol" id="" autofocus>
                                            <option class="form-control" value=""></option>
                                            @foreach($Roles as $rol)
                                                <option class="form-control text-uppercase"
                                                        value="{{$rol->id}}"> {{strtoupper($rol->display_name)}} </option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>

                            </div>
                            <br>
                            <div class="row">
                                <br>
                                <div class="text-center">
                                    <button type="button" class="btn btn-xlg  bg-pink" onclick="limpiar()">LIMPIAR
                                        CAMPOS
                                    </button>
                                    <button type="submit" form="form_register" class="btn btn-xlg bg-teal">ACEPTAR
                                    </button>
                                </div>
                            </div>
                            <br>

                        </form>
                    </div>
                </div>
                <div class="panel ">
                    <div class="panel-heading bg-pink ">
                        <h4>ADMINISTRAR USUARIOS</h4>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="table-responsive">
                                <table class="table datatable-basic">
                                    <thead>
                                    <tr>
                                        <th class="text-center">RFC</th>
                                        <th class="text-center">NOMBRE</th>
                                        <th class="text-center">MÓDULO</th>
                                        <th class="text-center">ESTATUS</th>
                                        <th class="text-center"></th>
                                        <th class="text-center"></th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($Usuarios as $Usuario)
                                        <tr>
                                            <td class="text-center">{{$Usuario->rfc}}</td>
                                            <td class="text-center">{{$Usuario->primer_apellido}} {{$Usuario->segundo_apellido}} {{$Usuario->name}}</td>
                                            <td class="text-center">{{$Usuario->modulo->modulo}}</td>
                                            <td class="text-center">
                                                @if($Usuario->estatus_id == 1)
                                                    <b class="text-grey-800">ACTIVO</b>
                                                @elseif($Usuario->estatus_id == 2)

                                                    <b class="text-grey-800">BAJA</b>
                                                @elseif($Usuario->estatus_id == 3)
                                                    <b class="text-grey-800">ACTIVO <br> <code>CONTRASEÑA POR
                                                            DEFECTO</code></b>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($Usuario->estatus_id == 1 || $Usuario->estatus_id == 3)

                                                    <button type="button" class="btn btn-block bg-danger-800"
                                                            onclick="dar_baja({{$Usuario}} , {{$Usuario->modulo}})">
                                                        DAR DE BAJA
                                                    </button>
                                                @elseif($Usuario->estatus_id == 2)

                                                    <button type="button" class="btn btn-block bg-success-800"
                                                            onclick="dar_alta({{$Usuario}} , {{$Usuario->modulo}})">
                                                        DAR DE ALTA
                                                    </button>

                                                @endif

                                            </td>
                                            <td class="text-center">
                                                @if($Usuario->estatus_id == 1)

                                                    <button type="button" class="btn btn-block bg-orange-800"
                                                            onclick="restaurar_contrasena({{$Usuario}} , {{$Usuario->modulo}})">
                                                        RESTAURAR CONTRASEÑA
                                                    </button>
                                                @else
                                                    <button type="button" class="btn btn-block bg-orange-800"
                                                            disabled>
                                                        RESTAURAR CONTRASEÑA
                                                    </button>
                                                @endif

                                            </td>
                                        </tr>

                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>

                    </div>
                </div>


            </div>

        </div>

    </div>
    <div id="modal_confirmar" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="modal_form" action="" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="modal_usuario" id="modal_usuario">

                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group">
                                <div class="text-center">
                                    <span style="font-size: 25px" class="text-bold" id="modal_texto"> </span>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div style="font-size: 15px">
                                <div class="form-group">
                                    <div class="col md-8 col-md-offset-2">
                                        <div class="row">
                                            <span class="text-bold "> RFC : </span><span id="modal_rfc"></span>
                                        </div>

                                        <div class="row">
                                            <span class="text-bold "> NOMBRE : </span><span
                                                    id="modal_nombre"></span>
                                        </div>
                                        <div class="row">
                                            <span class="text-bold "> MÓDULO : </span><span
                                                    id="modal_modulo"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <code class="text-size-large" id="modal_recuerdo"></code>
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-xlg bg-danger-800" data-dismiss="modal">CANCELAR
                        </button>
                        <button type="submit" class="btn btn-xlg bg-success-800">CONFIRMAR</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        function limpiar() {
            $('#form_register').trigger("reset");
            $('[name = modulo]').focus();
        }

        function dar_baja(usuario, modulo) {

            $('#modal_confirmar').modal({backdrop: 'static', keyboard: false});
            $('#modal_confirmar').on('shown.bs.modal', function () {
                $('#modal_form').attr("action", "{{url('/Usuario/baja')}}");
                $('#modal_usuario').val(usuario.id);
                $('#modal_rfc').text(usuario.rfc);
                $('#modal_modulo').text(modulo.modulo);
                $('#modal_nombre').text(usuario.primer_apellido + " " + usuario.segundo_apellido + " " + usuario.name);
                $('#modal_texto').html('¿Estas Seguro de dar de <span class="text-danger-800">BAJA</span> el Usuario ?');
                $('#modal_recuerdo').text('');
            });
        }

        function dar_alta(usuario, modulo) {
            $('#modal_confirmar').modal({backdrop: 'static', keyboard: false});
            $('#modal_confirmar').on('shown.bs.modal', function () {
                $('#modal_form').attr("action", "{{url('/Usuario/alta')}}");
                $('#modal_usuario').val(usuario.id);
                $('#modal_rfc').text(usuario.rfc);
                $('#modal_modulo').text(modulo.modulo);
                $('#modal_nombre').text(usuario.primer_apellido + " " + usuario.segundo_apellido + " " + usuario.name);
                $('#modal_texto').html('¿Estas Seguro de dar de <span class="text-success-800">ALTA</span> el Usuario ?');
                $('#modal_recuerdo').text('');
            });
        }

        function restaurar_contrasena(usuario, modulo) {
            $('#modal_confirmar').modal({backdrop: 'static', keyboard: false});
            $('#modal_confirmar').on('shown.bs.modal', function () {
                $('#modal_form').attr("action", "{{url('/Usuario/restablecer_contraseña')}}");
                $('#modal_usuario').val(usuario.id);
                $('#modal_rfc').text(usuario.rfc);
                $('#modal_modulo').text(modulo.modulo);
                $('#modal_nombre').text(usuario.primer_apellido + " " + usuario.segundo_apellido + " " + usuario.name);
                $('#modal_texto').html('¿Estas Seguro de <span class="text-orange-800">RESTABLECER LA CONTRASEÑA </span> del Usuario ?');
                $('#modal_recuerdo').text('* Al restablecer la contraseña esta sera por defecto "123456" y el usuario tendra que cambiar su contraseña cuando entre al sistema');
            });

        }


    </script>
@endsection
