@extends('layouts.app')

@section('content')
    @if($errors->any())

        @foreach($errors->all() as $error)
            <script>
                swal({
                    title: "{{$errors->all()[1]}}",
                    text: "{{$errors->all()[2]}}",
                    type: "{{$errors->all()[0]}}",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                }).then(function () {
                    $("[name =folio_qr ]").focus();
                });
            </script>
        @endforeach

    @endif
    @if(isset($tramite))
        <div class="panel panel-flat">
            <div class="panel-heading text-left">
                <h2>
                    <b class="text-pink">¡ TRÁMITE ENCONTRADO !</b>
                </h2>


            </div>
            <div class="panel-body">


                <div class="col-md-10 col-md-offset-1">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text-center">
                                <label style="font-size: 20px"><b>DATOS DE TRÁMITE </b></label>
                            </div>
                            <br>
                            <table class="table table-bordered table-hover">
                                <tbody>
                                <tr>
                                    <td class="text-bold">FECHA DEL TRÁMITE</td>
                                    <td>{{$tramite->fecha_revista}}</td>
                                </tr>
                                <tr>
                                    <td class="text-bold">CORTESÍA</td>
                                    <td>{{$tramite->constancia}}</td>
                                </tr>

                                <tr>
                                    <td class="text-bold">PLACA</td>
                                    <td>{{$tramite->placa}}</td>
                                </tr>
                                <tr>
                                    <td class="text-bold">OPERADOR</td>
                                    <td>{{$tramite->usuario->rfc}}</td>
                                </tr>
                                <tr>
                                    <td class="text-bold">MÓDULO</td>
                                    <td>{{$tramite->modulo->modulo}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <div class="text-center">
                                <label style="font-size: 20px"><b>DATOS DEL DIF </b></label>
                            </div>
                            <br>
                            @if($tramite->constancia_data == null )
                                <table class="table table-bordered table-hover">
                                    <tbody>
                                    <tr>
                                        <td class="text-bold">FECHA DEL TRÁMITE</td>
                                        <td> -</td>
                                    </tr>
                                    <tr>
                                        <td class="text-bold">CORTESÍA</td>
                                        <td> -</td>
                                    </tr>

                                    <tr>
                                        <td class="text-bold">PLACA</td>
                                        <td> -</td>
                                    </tr>
                                    <tr>
                                        <td class="text-bold">OPERADOR</td>
                                        <td> -</td>
                                    </tr>
                                    <tr>
                                        <td class="text-bold">MÓDULO</td>
                                        <td> -</td>
                                    </tr>
                                    </tbody>
                                </table>
                            @else
                                @php
                                    $constancia = json_decode($tramite->constancia_data,true);

                                @endphp
                                <table class="table table-bordered table-hover">
                                    <tbody>
                                    <tr>
                                        <td class="text-bold">CURP / RFC</td>
                                        <td>{{$constancia["curp"]}}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-bold">NOMBRE</td>
                                        <td>{{$constancia["nombre"]}}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-bold">FECHA SOLICITUD</td>
                                        <td>{{$constancia["fecha_sol"]}}</td>
                                    </tr>

                                    <tr>
                                        <td class="text-bold">FECHA DE VIGENCIA</td>
                                        <td>{{$constancia["fecha_vigencia"]}}</td>
                                    </tr>

                                    <tr>
                                        <td class="text-bold">OFICIO</td>
                                        <td>{{$constancia["oficio"]}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            @endif

                        </div>


                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text-center">
                                <label style="font-size: 20px"><b>DATOS DE PARTICULAR </b></label>
                            </div>
                            <br>
                            <table class="table table-bordered table-hover">
                                <tbody>
                                <tr>
                                    <td class="text-bold">CURP/RFC</td>
                                    <td>{{($tramite->propietario->clave_unica) ?? "-"}}</td>
                                </tr>
                                <tr>
                                    <td class="text-bold">NOMBRE</td>
                                    <td> {{($tramite->propietario->nombre)?? "-"}} {{($tramite->propietario->primer_apellido)??"-"}} {{($tramite->propietario->segundo_apellido)??"-"}}</td>
                                </tr>

                                <tr>
                                    <td class="text-bold">FECHA DE NACIMIENTO</td>
                                    <td>{{($tramite->propietario->fecha_nacimiento) ?? "-"}}</td>
                                </tr>
                                <tr>
                                    <td class="text-bold">NACIONALIDAD</td>
                                    <td>
                                        @if($tramite->propietario)
                                            @if($tramite->propietario->pais_id == 82)
                                                MEXICANA
                                            @else
                                                OTRA
                                            @endif
                                        @else
                                            -
                                        @endif

                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-bold">TELEFONO</td>
                                    <td>{{($tramite->propietario->telefono) ?? "-"}}</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <div class="text-center">
                                <label style="font-size: 20px">
                                    <b>
                                        {{($tramite->holograma)? "HOLOGRAMA IMPRESO": "NO SE IMPRIMIO HOLOGRAMA"}}
                                    </b>
                                </label>
                            </div>


                            <br>
                            <table class="table table-bordered table-hover">
                                <tbody>
                                <tr>
                                    <td class="text-bold">FOLIO HOLOGRAMA</td>
                                    <td>{{($tramite->holograma->folio_holograma)?? "-"}}</td>
                                </tr>
                                <tr>
                                    <td class="text-bold">FECHA EXPEDIDO</td>
                                    <td>{{($tramite->holograma->fecha_expedido)?? "-"}}</td>
                                </tr>
                                <tr>
                                    <td class="text-bold">FECHA VIGENCIA</td>
                                    <td>{{($tramite->holograma->vigencia_documento)?? "-"}}</td>
                                </tr>
                                <tr>
                                    <td class="text-bold">NÚMERO DE IMPRESIONES</td>
                                    <td>{{($tramite->impresion)}}</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <br>

                </div>


            </div>
            <div class="panel-footer">
                <div class="text-center">
                    <a class="btn btn-xlg bg-teal-700" href="{{url("/Tramite")}}">
                        <i class="icon icon-arrow-left12"></i> REGRESAR
                    </a>
                    <button type="button" class="btn btn-xlg bg-danger-700" onclick="cancelar()">
                        <i class="icon icon-cross3"></i> CANCELAR TRÁMITE
                    </button>

                </div>
            </div>
        </div>
        <div id="modal_confirmar" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="modal_form" action="{{url("/Tramite/Cancelar")}}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id_revista" value="{{$tramite->id_revista_vehiculo}}">

                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group">
                                    <div class="text-center">
                                        <span style="font-size: 25px" class="text-bold text-danger-800"> ¿Estas seguro de cancelar el trámite?</span>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div style="font-size: 15px">
                                    <div class="form-group">
                                        <div class="col md-12">

                                            <div class="text-left">
                                                <label style="font-size: 25px">Inserta el folio del oficio</label>
                                            </div>
                                            <br>
                                            <div class="text-center ">
                                                <input name="folio_oficio " type="text" id="folio_oficio"
                                                       class="form-control mayusculas_todo input-xlg"
                                                       placeholder="DCVLP-999-2018">
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-xlg bg-danger-800" data-dismiss="modal">CANCELAR
                                </button>
                                <button type="submit" class="btn btn-xlg bg-success-800" id="btn_confirmar">CONFIRMAR
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $("#btn_confirmar").attr("disabled", true);

            function cancelar() {
                $('#modal_confirmar').modal({backdrop: 'static', keyboard: false});
                $('#modal_confirmar').on('shown.bs.modal', function () {
                    $("#folio_oficio").val("").focus();
                });
            }

            $("#folio_oficio").on('keyup', function (x) {

                if ($(this).val().length < 5)
                    $("#btn_confirmar").attr("disabled", true);
                else
                    $("#btn_confirmar").attr("disabled", false);
            })
        </script>

    @else

        <div class="panel panel-flat">
            <div class="panel-heading text-left">
                <h2>
                    <b>Información de un Trámite </b>
                </h2>
                <label style="font-size: 20px" class="text-pink"><b>Ingresa la cortesia Qr o la placa ocupada en el
                        trámite</b></label>
                <br>
                <label style="font-size: 18px"></label>

            </div>
            <div class="panel-body">
                <form action="{{url('/Tramite')}}" method="POST" id="form_tramite">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="row ">
                        <div class="col-md-12">

                            <div class="col-md-4 col-md-offset-2 text-center">

                                <h2>Inserte cortesía Qr <br>
                                    <small></small>
                                </h2>
                                <input type="text" class="form-control text-center folio_text input-xlg"
                                       maxlength="16"
                                       name="folio_qr" autofocus>

                            </div>
                            <div class="col-md-4 text-center">

                                <h2>Inserte placa <br>
                                    <small></small>
                                </h2>
                                <input type="text" class="form-control text-center placa_text input-xlg"
                                       maxlength="5"
                                       name="placa">

                            </div>

                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="row">
                        <div class="text-center">
                            <button class="btn bg-pink btn-xlg">
                                BUSCAR <i style="margin-left: 5px" class="icon icon-search4 "></i>
                            </button>
                        </div>
                    </div>


                </form>

            </div>


        </div>

    @endif

@endsection
